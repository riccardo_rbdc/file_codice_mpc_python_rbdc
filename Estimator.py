# -*- coding: utf-8 -*-
"""
Created on December 3, 2015

@author: Marco, Mirco, Gabriele

Estimator with linear example
"""

from casadi import *
from casadi.tools import *
from matplotlib import pylab as plt
import math
import scipy.linalg as scla
import numpy as np
from Utilities import*

def defEstimator(Fx,Fy,y_k,u_k, estype,xhat_min, t_k, **kwargsin):
    if estype == 'kal':
        Q = kwargsin['Q']
        R = kwargsin['R']
        P_min = kwargsin['P_min']
        [P_plus, P_corr, xhat_corr] = kalman(Fx,Fy,y_k,u_k,Q,R,P_min,xhat_min,t_k)
        kwargsout = {"P_plus": P_plus, "P_corr": P_corr}
    elif estype == 'ekf':
        Q = kwargsin['Q']
        R = kwargsin['R']
        P_min = kwargsin['P_min']
        ts = kwargsin['ts']
        [P_plus, P_corr, xhat_corr] = ekf(Fx,Fy,y_k,u_k,Q,R,P_min,xhat_min,ts,t_k)
        kwargsout = {"P_plus": P_plus, "P_corr": P_corr}
    elif estype == 'kalss':
        K = kwargsin['K']
        [xhat_corr] = kalss(Fx,Fy,y_k,u_k,K,xhat_min,t_k)
        kwargsout = {}
    elif estype == 'mhe':
        Fobj = kwargsin['Fobj']
        P_k = kwargsin['P_min']
        wmin = kwargsin['wmin']
        wmax = kwargsin['wmax']
        vmin = kwargsin['vmin']
        vmax = kwargsin['vmax']
        ymin = kwargsin['ymin']
        ymax = kwargsin['ymax']
        xmin = kwargsin['xmin']
        xmax = kwargsin['xmax']
        N = kwargsin['N']
        opts = kwargsin['opts']
        ts = kwargsin['ts']
        v_k = kwargsin['vk']
        w_k = kwargsin['wk']
        U = kwargsin['U']
        X = kwargsin['X']
        Y = kwargsin['Y']
        V = kwargsin['V']
        W = kwargsin['W']
        xb = kwargsin['xb']
        up = kwargsin['up']
        Nmhe = kwargsin['Nmhe']
        [P_k, P_corr, xhat_corr, w_k,v_k,U,Y,X,V,W,xb] = mhe(Fx,Fy,y_k,u_k,P_k,xhat_min,N,Fobj,ts,t_k,U,Y,X,V,W,w_k,v_k,xb,wmin,wmax,vmin,vmax,ymin,ymax,xmin,xmax,opts,up,Nmhe)
        kwargsout = {"P_plus": P_k, "P_corr": P_corr, "U_mhe" : U, "X_mhe" : X, "Y_mhe" : Y,"V_mhe" : V, "W_mhe" : W, "wk" : w_k, "vk" : v_k, "xb" : xb}
    return [xhat_corr, kwargsout]

def Kkalss(Fx_model, Fy_model, x, u, k, d, t, h, x_ss, u_ss, ny, nd, nx, Q_kf, R_kf, offree, **dis_mat):
    """
    SUMMARY:
    Discrete-time steady-state Kalman filter gain calculation for the given
    linear system in state space form.
    
    SYNTAX:
    assignment = kalman(Fx_model,Fy_model,ny,nd,nx,Q_kf,R_kf,offree,**dis_mat)
  
    ARGUMENTS:
    + Fx_model - State correlation function  
    + Fy_model - Output correlation function 
    + nx, ny , nd  - State, output and disturbance dimensions
    + Q_kf - Process noise covariance matrix
    + R_kf - Measurements noise covariance matrix
    + offree - Offset free tag
    + dis_mat - Disturbance matrices
       
    OUTPUTS:
    + Kaug  - Steady-state Kalman filter gain 
    """    
    
    # get the system matrices
    Fun_in = SX.get_input(Fx_model)
    Adummy = jacobian(Fx_model.call(Fun_in)[0], Fun_in[0])    
    Fun_in = SX.get_input(Fy_model)
    Cdummy = jacobian(Fy_model.call(Fun_in)[0], Fun_in[0]) 
    
    d_ss = DM.zeros(nd)
    if offree == 'nl':
        xnew = vertcat(x,d)
        x_ss_p = vertcat(x_ss,d_ss)
    else:
        xnew = x
        x_ss_p = x_ss
        
    A_dm = Function('A_dm', [xnew,u,k,t], [Adummy])
    C_dm = Function('C_dm', [x,u,d,t], [Cdummy])
    
    A = A_dm(x_ss_p, u_ss, h, 0)
    C = C_dm(x_ss, u_ss, d_ss, 0.0)
    
    Aaug = DM.eye(nx+nd)
    Caug = DM.zeros(ny, nx+nd)
    
    if offree == 'nl':
        if A.size2() < nx+nd:
            Aaug[0:nx,0:nx] = A
        else:
            Aaug = A
            
        if C.size2() < nx+nd:
            Caug[0:ny,0:nx] = C
        else:
            Caug = C
    else:
        Aaug[0:nx,0:nx] = A
        Caug[0:ny,0:nx] = C
    
    if offree == "lin":
        Bd = dis_mat["Bd"]
        Cd = dis_mat["Cd"]
        
        Aaug[0:nx,nx:nx+nd] = Bd
        Caug[0:ny,nx:nx+nd] = Cd
    
    Ae = np.array(Aaug.T)
    Be = np.array(Caug.T)
    Qe = np.array(Q_kf)
    Re = np.array(R_kf)
    Pe = scla.solve_discrete_are(Ae,Be,Qe,Re)
    MAT = np.dot(Be.transpose(), Pe)
    MAT = np.dot(MAT, Be) + Re
    invMAT = np.linalg.inv(MAT)
    Ke = np.dot(Pe,Be)
    Ke = np.dot(Ke, invMAT)
    Kaug = DM(Ke)

    # Eigenvalue checks
    Aobs = Aaug - mtimes(mtimes(Aaug, Kaug), Caug)
    eigvals, eigvecs = scla.eig(Aobs)
    
    return (Kaug)
    
def kalss(Fx,Fy,y_act,u_k,K,xhat_min,t_k):
    """
    SUMMARY:
    Steady-state Discrete-time Kalman filter for the given linear system 
    in state space form.
    
    SYNTAX:
    assignment = kalman(Fx_model,Fy_model,y_act,u_k,K,xhat_min)
  
    ARGUMENTS:
    + Fx_model - State correlation function  
    + Fy_model - Output correlation function    
    + y_act - Measurements, i.e. y(k)
    + u_k - Input, i.e. u(k)
    + K - Kalman filter gain
    + xhat_min - Predicted mean of the state, i.e. x(k|k-1)
        
    OUTPUTS:
    + xhat_corr - Estimated mean of the state, i.e. x(k|k) 
    """    
    # predicted output: y(k|k-1) 
    yhat = Fy(xhat_min,u_k,t_k) 
    
    # estimation error
    e_k = y_act - yhat
    
    # estimated mean of the state: x(k|k) 
    xhat_corr = DM(xhat_min + mtimes(K, e_k))
    
    return [xhat_corr]
    
def kalman(Fx,Fy,y_act,u_k,Q,R,P_min,xhat_min,t_k):
    """
    SUMMARY:
    Discrete-time Kalman filter for the given linear system in state space form.
    
    SYNTAX:
    assignment = kalman(Fx_model,Fy_model,y_act,u,Q,R,P_min,xhat_min)
  
    ARGUMENTS:
    + Fx_model - State correlation function  
    + Fy_model - Output correlation function    
    + Q - Process noise covariance matrix
    + R - Measurements noise covariance matrix
    + y_act - Measurements, i.e. y(k)
    + u - Input, i.e. u(k)
    + P_min - Predicted covariance of the state, i.e. P(k|k-1)
    + xhat_min - Predicted mean of the state, i.e. x(k|k-1)
        
    OUTPUTS:
    + P_plus - Predicted covariance of the state, i.e. P(k+1|k)
    + P_corr - Estimated covariance of the state, i.e. P(k|k) 
    + xhat_corr - Estimated mean of the state, i.e. x(k|k) 
    """    
    # get the system matrices
    Fun_in = SX.get_input(Fx)
    A_dm = jacobian(Fx.call(Fun_in)[0], Fun_in[0])    
    Fun_in = SX.get_input(Fy)
    C_dm = jacobian(Fy.call(Fun_in)[0], Fun_in[0]) 
    
    # predicted output: y(k|k-1) 
    yhat = mtimes(C_dm,xhat_min) 
    
    # filter gain
    K = (solve((mtimes(mtimes(C_dm,P_min),C_dm.T) + R).T,(mtimes(P_min,C_dm.T)).T)).T
    
    # estimated covariance of the state: P(k|k)
    P_corr = mtimes(DM.eye(A_dm.shape[0]) - mtimes(K,C_dm), P_min)
    
    # estimation error
    e_k = y_act - yhat
    
    # estimated mean of the state: x(k|k) 
    xhat_corr = DM(xhat_min + mtimes(K, e_k))
        
    # predicted covariance of the state: P(k+1|k) 
    P_plus = mtimes(mtimes(A_dm, P_corr),A_dm.T) + Q
    
    return [P_plus, P_corr, xhat_corr]
    
def ekf(Fx,Fy,y_act,u_k,Q,R,P_min,xhat_min,ts,t_k):
    """
    SUMMARY:
    Discrete-time extended Kalman filter for the given nonlinear system.
    
    SYNTAX:
    assignment = ekf(Fx_model,Fy_model,y_act,u,Q,R,P_min,xhat_min,h)
  
    ARGUMENTS:
    + Fx_model - State correlation function  
    + Fy_model - Output correlation function    
    + Q - Process noise covariance matrix
    + R - Measurements noise covariance matrix
    + y_act - Measurements, i.e. y(k)
    + u - Input, i.e. u(k)
    + P_min - Predicted covariance of the state, i.e. P(k|k-1)
    + xhat_min - Predicted mean of the state, i.e. x(k|k-1)
    + h - Time step

        
    OUTPUTS:
    + P_plus - Predicted covariance of the state, i.e. P(k+1|k)
    + P_corr - Estimated covariance of the state, i.e. P(k|k) 
    + xhat_corr - Estimated mean of the state, i.e. x(k|k) 
    """    
    # predicted output: y(k|k-1) 
    yhat = Fy(xhat_min,u_k,t_k) 
    
    # get linearization of measurements
    Fun_in = SX.get_input(Fy)
    C_dm = jacobian(Fy.call(Fun_in)[0], Fun_in[0])
    C = Function('C', [Fun_in[0],Fun_in[1],Fun_in[2]], [C_dm])
    
    # filter gain 
    C_k = C(xhat_min,u_k,t_k)
    Pp_kC_kT = mtimes(P_min,C_k.T)
    inbrackets = mtimes(C_k,Pp_kC_kT) + R
    K_k = mtimes(Pp_kC_kT,inv(inbrackets)) 
    
    # estimated covariance of the state: P(k|k)
    P_corr = mtimes(DM.eye(xhat_min.size1()) - mtimes(K_k,C_k), P_min)
   
   # estimation error
    e_k = y_act - yhat
    
    # estimated mean of the state: x(k|k) 
    xhat_corr = DM(xhat_min + mtimes(K_k, e_k))
        
    # get linearization of states
    Fun_in = SX.get_input(Fx)
    
    X_next = Fx(Fun_in[0],Fun_in[1],ts,t_k)
    jac_Fx = jacobian(X_next,Fun_in[0])
    A = Function('A', [Fun_in[0],Fun_in[1],Fun_in[2],Fun_in[3]], [jac_Fx])
    
    # next predicted covariance of the state: P(k+1|k) 
    A_k = A(xhat_corr,u_k,ts,t_k)
    A_kP_k = mtimes(A_k,P_corr)
    P_plus = mtimes(A_kP_k, A_k.T) + Q
    
    return [P_plus, P_corr, xhat_corr]
    
def mhe(Fx,Fy,y_act,u_k,P_k,xhat_min,N,F_obj,ts,t_k,U,Y,X,V,W,w_k,v_k,x_bar,wmin,wmax,vmin,vmax,ymin,ymax,xmin,xmax,opts,mhe_up,N_mhe):
    """
    SUMMARY:
    Moving horizon estimation method for the given nonlinear system.
    
    SYNTAX:
    assignment = mhe(Fx,Fy,y_act,u_k,Q,R,P_k,xhat_min,N, G = None)
  
    ARGUMENTS:
    + Fx_model - State correlation function  
    + Fy_model - Output correlation function    
    + Q - Process noise covariance matrix
    + R - Measurements noise covariance matrix
    + y_act - Measurements, i.e. y(k)
    + u - Input, i.e. u(k)
    + P_min - Predicted covariance of the state, i.e. P(k|k-1)
    + xhat_min - Predicted mean of the state, i.e. x(k|k-1)
    + h - Time step

        
    OUTPUTS:
    + P_plus - Predicted covariance of the state, i.e. P(k+1|k)
    + P_corr - Estimated covariance of the state, i.e. P(k|k) 
    + xhat_corr - Estimated mean of the state, i.e. x(k|k) 
    """  
    
    ksim = int(round(t_k/ts))
    n = xhat_min.size1()
    m = u_k.size1()
    p = y_act.size1()
    

    # predicted output: y(k|k-1) 
    yhat = Fy(xhat_min,u_k,t_k) 
    
    # get linearization of measurements
    Fun_in = SX.get_input(Fy)
    C_dm = jacobian(Fy.call(Fun_in)[0], Fun_in[0])
    C = Function('C', [Fun_in[0],Fun_in[1],Fun_in[2]], [C_dm])
    
    D_dm = jacobian(Fy.call(Fun_in)[0], Fun_in[1])
    D = Function('D', [Fun_in[0],Fun_in[1],Fun_in[2]], [D_dm])
    
    # get linearization of states
    Fun_in = SX.get_input(Fx)
    A_dm = jacobian(Fx.call(Fun_in)[0],Fun_in[0])
    A = Function('A', [Fun_in[0],Fun_in[1],Fun_in[2],Fun_in[3],Fun_in[4]], [A_dm])
    
    B_dm = jacobian(Fx.call(Fun_in)[0],Fun_in[1])
    B = Function('B', [Fun_in[0],Fun_in[1],Fun_in[2],Fun_in[3],Fun_in[4]], [B_dm])
    
    G_dm = jacobian(Fx.call(Fun_in)[0],Fun_in[4])
    G = Function('G', [Fun_in[0],Fun_in[1],Fun_in[2],Fun_in[3],Fun_in[4]], [G_dm])
    
    n_w = G_dm.size2() #get w dimension
    
    nxv = n+p 
    nxvw = nxv + n_w
    n_opt = N*nxvw + n + p # total # of variables   
    
    # get linearization of objective function
    Fun_in = SX.get_input(F_obj)
    FobjIn = vertcat(Fun_in[0],Fun_in[1])
    [H_dm,_] = hessian(F_obj.call(Fun_in)[0], FobjIn)
    H = Function('H', [Fun_in[0],Fun_in[1]], [H_dm])
    
    ## Stacking data
    if ksim < N_mhe:
        U = vertcat(U,u_k)
        Y = vertcat(Y,y_act)
#        X = vertcat(X,xhat_corr)
#        V = vertcat(V,v_k)
#        W = vertcat(W,w_k)
        
    else:#Smoothing
        U = vertcat(U[m:],u_k)
        Y = vertcat(Y[p:],y_act)
#        X = vertcat(X[n:],xhat_corr)
#        V = vertcat(V[p:],v_k)
#        W = vertcat(W[n_w:],w_k)
        
    if ksim > 0:
#    if (ksim==0):
    ## Initial guess (on the first NLP run)
        w_guess = DM.zeros(n_opt)
        for key in range(N):
            w_guess[key*nxvw:key*nxvw+n] = xhat_min
            w_guess[key*nxvw+n:key*nxvw+nxv] = v_k
            w_guess[key*nxvw+nxv:(key+1)*nxvw] = w_k
        w_guess[N*nxvw:N*nxvw+n] = xhat_min  #x_N
        w_guess[N*nxvw+n:N*nxvw+nxv] = v_k  #v_N
        
        P_k_r = P_k.reshape((n*n,1)) 
    
        ## Defining the optimization solver
        (solver, w_lb, w_ub, g_lb, g_ub) = mhe_opt(n, m, p, n_w, F_obj, Fx, Fy, \
        N, ts, opts, wmin = wmin, wmax = wmax,  vmin = vmin, vmax = vmax, \
        ymin = ymin, ymax = ymax, xmin = xmin, xmax = xmax)
        
        
        ## Set parameter for dynamic optimisation
        par = vertcat(U,Y,x_bar,P_k_r,t_k)
        
        # Optimization problem
        sol = solver(lbx = w_lb,
                     ubx = w_ub,
                     x0  = w_guess,
                     p = par,
                     lbg = g_lb,
                     ubg = g_ub)
    
        w_opt = sol["x"]
#        xhat_corr = w_opt[n_opt-nxwy-n:n_opt-nxwy] 
        xhat_corr = w_opt[-nxv:-p] 
        v_k = w_opt[-p-nxvw:-nxvw] 
        w_k = w_opt[-nxvw:-nxv] 
        
    else:
        xhat_corr = xhat_min
        
        
    ## Stacking data
    if ksim < N_mhe:
#        U = vertcat(U,u_k)
#        Y = vertcat(Y,y_act)
        X = vertcat(X,xhat_corr)
        V = vertcat(V,v_k)
        W = vertcat(W,w_k)
        
    else:#Smoothing
#        U = vertcat(U[m:],u_k)
#        Y = vertcat(Y[p:],y_act)
        X = vertcat(X[n:],xhat_corr)
        V = vertcat(V[p:],v_k)
        W = vertcat(W[n_w:],w_k)
            
        
        #x_bar updating
    if ksim > N_mhe: #permitted only after packing enough information    
        if mhe_up == 'filter': #Filtering
            x_bar = X[0:n]
            v_bar = V[0:p]
            w_bar = W[0:n_w]
        else: # Smoothing: picking the second component of the optimization sequence
            x_bar = w_opt[nxwy:nxwy+n]
            v_bar = w_opt[nxwy+nxw:2*nxwy]
            w_bar = w_opt[nxwy+n:nxwy+nxw]
     
    P_corr = P_k
    # Update prior weight and x_bar
    if ksim > N_mhe:
        if mhe_up == 'filter': #Filtering
            # Calculating linearization of objective function    
            H_k = inv(H(w_bar,v_bar))    
            Q_k = H_k[0:n_w,0:n_w]
            R_k = H_k[-p:,-p:]
            S_k = H_k[0:n_w,-p:]
            
            C_k = C(xhat_min,u_k,t_k)
            Pp_kC_kT = mtimes(P_k,C_k.T)
            inbrackets = mtimes(C_k,Pp_kC_kT) + R_k
            K_k = mtimes(Pp_kC_kT,inv(inbrackets)) 
            P_corr = mtimes(DM.eye(xhat_min.size1()) - mtimes(K_k,C_k), P_k)
       	
            # next predicted covariance of the state: P(k+1|k) 
            A_k = A(xhat_corr,u_k,ts,t_k,w_k)
            G_k = G(xhat_corr,u_k,ts,t_k,w_k)
            A_kP_k = mtimes(A_k,P_corr)
            #The following terms comes from the correlation between v and w (Feng et al. 2013)
            M_k = mtimes(-K_k,S_k.T)
            A_kM_k = mtimes(A_k,M_k)
            G_kM_kT = mtimes(G_k,M_k.T)
            G_kQ_k = mtimes(G_k,Q_k)
            P_k = mtimes(A_kP_k, A_k.T) + mtimes(G_kQ_k,G_k.T) + mtimes(A_kM_k, G_k.T) \
            + mtimes(G_kM_kT, A_k.T)
    
        else: #Smoothing
            #Building matrices Rd,Qd,M,W
            R_k = DM(scla.block_diag(R_k, R))
            Q_k = DM(scla.block_diag(Q_k, Q))
#            M_k = DM(.....) #to complete
#            W = R_k + xQx(M_k,Q_k)
	
	#to complete...
        
    return [P_k, P_corr, xhat_corr, w_k,v_k,U,Y,X,V,W,x_bar]

    
