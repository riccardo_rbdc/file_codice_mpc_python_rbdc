# -*- coding: utf-8 -*-
"""
Created on December 3, 2015

@author: Marco, Mirco, Gabriele

Target calculation
"""

from casadi import *
from casadi.tools import *
from matplotlib import pylab as plt
import math
from math import *
import scipy.linalg as scla
import numpy
import numpy as np
from Utilities import*

# Target calculation: model constraints and objective function
def opt_ss(n, m, p, nd, offree, Fx_model,Fy_model,Fss_obj,QForm_ss,DUssForm,Adaptation,sol_opts, umin = None, umax = None, w_s = None, z_s = None, ymin = None, ymax = None, xmin = None, xmax = None, h = None):
    
    nxu = n+m 
    nxuy = nxu + p
    
    # Define symbolic optimization variables
    wss = MX.sym("wss",nxuy) 
    
    # Get states
    Xs = wss[0:n]
    
    # Get controls
    Us = wss[n:nxu]
    
    # Get output
    Ys = wss[nxu:nxuy]
    
    # Define parameters
    par_ss = MX.sym("par_ss", 2*m+p+nd+p*m+n+1)
    usp = par_ss[0:m]   
    ysp = par_ss[m:m+p]
    xsp = par_ss[m+p:m+p+n]
    d = par_ss[m+p+n:m+p+n+nd]
    Us_prev = par_ss[m+p+n+nd:2*m+p+n+nd]
    lambdaT_r = par_ss[2*m+p+n+nd:2*m+p+n+nd+p*m]
    t = par_ss[2*m+p+nd+n+p*m:2*m+p+nd+n+p*m+1]
    
    lambdaT = lambdaT_r.reshape((p,m)) #shaping lambda_r vector in order to reconstruct the matrix
    
    
    # Defining constraints
    if ymin is None:
        ymin = -DM.inf(p)
    if ymax is None:
        ymax = DM.inf(p)
    if xmin is None:
        xmin = -DM.inf(n)
    if xmax is None:
        xmax = DM.inf(n)
    if umin is None:
        umin = -DM.inf(m)
    if umax is None:
        umax = DM.inf(m) 
    
    if h is None:
        h = .1 #Defining integrating step if not provided from the user
    gss = []
    
    Xs_next = Fx_model( Xs, Us, h, d, t)
        
    gss.append(Xs_next - Xs)
    gss = vertcat(*gss)
    
    Ys_next = Fy_model( Xs, Us, d, t) + mtimes(lambdaT,(Us - Us_prev))
    gss = vertcat(gss , Ys_next- Ys)
    
    # Defining obj_fun
    dy = Ys
    du = Us
    dx = Xs
    
    if QForm_ss is True:   #Checking if the OF is quadratic
        dx = dx - xsp
        dy = dy - ysp
        du = du - usp
        
    if DUssForm is True:
        du = Us - Us_prev #Adding weight on du
        
    fss_obj = Fss_obj( dx, du, dy, xsp, usp, ysp)
#            # Defining soft constraint on y and x
#            eps_s = MX.sym("eps_s",p) 
#            eta_s = MX.sym("eta_s",n)  
#            if w_s == None:
#                w_s = DM.zeros(p,p) 
#            if z_s == None:
#                z_s = DM.zeros(n,n)
#            
#            MAT_eps = mtimes( w_s,eps_s)
#            MAT_eta = mtimes(z_s, eta_s)
#            fss_obj = fss_obj + mtimes(eps_s.T, MAT_eps) + mtimes(eta_s.T, MAT_eta) 
#            
#            # Defining augmented vector
#            wss_aug = vertcat(wss,eta_s,eps_s)
    
    #Defining bound constraint
#    wss_lb = -DM.inf(nxuy+n+p)
#    wss_ub = DM.inf(nxuy+n+p)
    wss_lb = -DM.inf(nxuy)
    wss_ub = DM.inf(nxuy)
    wss_lb[0:n] = xmin
    wss_ub[0:n] = xmax
    wss_lb[n: nxu] = umin
    wss_ub[n: nxu] = umax
    wss_lb[nxu: nxuy] = ymin
    wss_ub[nxu: nxuy] = ymax
    
    ng = gss.size1()
    gss_lb = DM.zeros(ng,1)   # Equalities identification 
    gss_ub = DM.zeros(ng,1)
     
#    wss_lb[nxuy: nxuy+n+p] = DM.zeros(n+p) # Imposing slack variables greater than 0
    
    
#            # Defining augmented bound constraint  
#            gss = vertcat(gss, Xs + eta_s, Xs - eta_s, ys + eps_s, ys - eps_s)
#            gss_lb = -DM.inf(n+p+2*n+2*p)
#            gss_ub = DM.inf(n+p+2*n+2*p)
#            gss_lb[0:n] = 0   # Equalities identification 
#            gss_ub[0:n] = 0
#            gss_lb[n: n+p] = 0
#            gss_ub[n: n+p] = 0
#            gss_lb[n+p: 2*n+p] = xmin
#            gss_ub[2*n+p: 3*n+p] = xmax
#            gss_lb[3*n+p: 3*n+ 2*p] = ymin
#            gss_ub[3*n+ 2*p: 3*n+ 3*p] = ymax
    
    nlp_ss = {'x':wss, 'p':par_ss, 'f':fss_obj, 'g':gss}
    
    solver_ss = nlpsol('solver','ipopt', nlp_ss, sol_opts)
    
    return [solver_ss, wss_lb, wss_ub, gss_lb, gss_ub]
