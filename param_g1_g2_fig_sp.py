#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  4 08:26:18 2017

@author: marcovaccari & RBdC
"""

from casadi import *
from casadi.tools import *
from matplotlib import pylab as plt
import math
import scipy.linalg as scla
import numpy as np

distmod = 'ODM'         # IDM or ODM

import scipy.io as sio #to manage savings of files and loading

# error on gamma_1 and 2 for stiction and NO-stiction case
# error on gamma_1
JCL_g1aware = sio.loadmat('JCL_proc1aware_sp_' + distmod + '.mat')
JCL_g1stic_inv = sio.loadmat('JCL_proc1stic_inv_sp_' + distmod + '.mat')
# JCL_g1aware_FF = sio.loadmat('JCL_proc1_FFaware_sp_' + distmod + '.mat')
# JCL_g1stic_inv_FF = sio.loadmat('JCL_proc1_FFstic_inv_sp_' + distmod + '.mat')

J_CL_aw = JCL_g1aware['JCL']
egm1_aw = JCL_g1aware['egm1']
J_CL_si = JCL_g1stic_inv['JCL']
egm1_si = JCL_g1stic_inv['egm1']
#J_CL_aw_FF = JCL_g1aware_FF['JCL']
#egm1_aw_FF = JCL_g1aware_FF['egm1']
#J_CL_si_FF = JCL_g1stic_inv_FF['JCL']
#egm1_si_FF = JCL_g1stic_inv_FF['egm1']

pf = './results/mimo/sp/param_proc1/'
if not os.path.exists(pf):
    os.makedirs(pf)

# plot figures
plt.figure(figsize=(6,18))

plt.plot(egm1_aw, J_CL_si,'k^--') 
plt.plot(egm1_aw, J_CL_aw,'ro-')
#plt.plot(egm1_aw, J_CL_si_FF,'b*:') 
#plt.plot(egm1_aw, J_CL_aw_FF,'g+-')
plt.yscale('log')
plt.xlabel('$e_{\gamma_1}$', fontsize = 23)
plt.ylabel('$J_{CL}$', fontsize = 23)
plt.xticks(fontsize = 17)
plt.yticks(fontsize = 17)
plt.axis([np.amin(egm1_aw)-0.01, np.amax(egm1_aw)+0.01, 0.8*np.amin(J_CL_aw), 1.2*np.amax(J_CL_aw)])
plt.grid(True)
plt.legend(('st. inversion','st. embedding','LMPC-FF','NMPC-FF'),loc = 'lower left',fontsize = 14)
plt.savefig(pf + 'param_gamma1_' + distmod + '.png', format = 'png', transparent = True, bbox_inches = 'tight')
plt.show()


# error on gamma_2
JCL_g2aware = sio.loadmat('JCL_proc2aware_sp_' + distmod + '.mat')
JCL_g2stic_inv = sio.loadmat('JCL_proc2stic_inv_sp_' + distmod + '.mat')
#JCL_g2aware_FF = sio.loadmat('JCL_proc2_FFaware_sp_' + distmod + '.mat')
#JCL_g2stic_inv_FF = sio.loadmat('JCL_proc2_FFstic_inv_sp_' + distmod + '.mat')

J_CL_aw = JCL_g2aware['JCL']
egm2_aw = JCL_g2aware['egm2']
J_CL_si = JCL_g2stic_inv['JCL']
egm2_si = JCL_g2stic_inv['egm2']
#J_CL_aw_FF = JCL_g2aware_FF['JCL']
#egm2_aw_FF = JCL_g2aware_FF['egm2']
#J_CL_si_FF = JCL_g2stic_inv_FF['JCL']
#egm2_si_FF = JCL_g2stic_inv_FF['egm2']

pf = './results/mimo/sp/param_proc2/'
if not os.path.exists(pf):
    os.makedirs(pf)

# plot figures
plt.figure(figsize=(6,18))

plt.plot(egm2_aw, J_CL_si,'k^--') 
plt.plot(egm2_aw, J_CL_aw,'ro-')
#plt.plot(egm2_aw, J_CL_si_FF,'b*:') 
#plt.plot(egm2_aw, J_CL_aw_FF,'g+-')
plt.yscale('log')
plt.xlabel('$e_{\gamma_2}$', fontsize = 23)
plt.ylabel('$J_{CL}$', fontsize = 23)
plt.xticks(fontsize = 17)
plt.yticks(fontsize = 17)
plt.axis([np.amin(egm2_aw)-0.01, np.amax(egm2_aw)+0.01, 0.8*np.amin(J_CL_aw), 1.2*np.amax([np.amax(J_CL_aw),np.amax(J_CL_si)])])
plt.grid(True)
plt.legend(('st. inversion','st. embedding','LMPC-FF','NMPC-FF'),loc = 'lower left',fontsize = 14)
plt.savefig(pf + 'param_gamma2_' + distmod + '.png', format = 'png', transparent = True, bbox_inches = 'tight')
plt.show()


