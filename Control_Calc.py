# -*- coding: utf-8 -*-
"""
Created on December 3, 2015

@author: Marco, Mirco, Gabriele

MPC control calculation
"""

from casadi import *
from casadi.tools import *
from matplotlib import pylab as plt
import math
import scipy.linalg as scla
import numpy as np
from Utilities import*


def opt_dyn(xSX , uSX, ySX, dSX, tSX, n, m, p, nd, offree, Fx_model, Fy_model, F_obj, Vfin, N, QForm, DUForm, ContForm, TermCons, nw, n_ws, sol_opts, umin = None, umax = None,  W = None, Z = None, ymin = None, ymax = None, xmin = None, xmax = None, Dumin = None, Dumax = None, h = None, fx = None, xstat = None, ustat = None, Qs = DM(0.), S_adj = DM(0.)):

    # Extract dimensions
    nxu = n+m 
    nxuy = nxu + p
    
    # Define symbolic optimization variables
    w = SX.sym("w",nw)  # w = [x[0],u[0], ... ,u[N-1],x[N],d,xs,us]
        
    # Get states
    X = [w[nxu*k : nxu*k+n] for k in range(N+1)]
     
    # Get controls
    U = [w[nxu*k+n : nxu*k + nxu] for k in range(N)]
    
    # Define parameters
#    par = MX.sym("par", nxu+nd+m+1+n*n+n*m)
    par = SX.sym("par", nxu+nd+m+1+n)
    xs = par[0:n]
    us = par[n:nxu]
    d = par[nxu:nxu+nd]
    um1 = par[nxu+nd:nxu+nd+m]
    t = par[nxu+nd+m:nxu+nd+m+1]
    x0 = par[nxu+nd+m+1:]
    
#    lambdaxT_r = par[nxu+nd+m+1:nxu+nd+m+1+n*n]
#    lambdauT_r = par[nxu+nd+m+1+n*n:nxu+nd+m+1+n*n+n*m]
#    
#    lambdaxT = lambdaxT_r.reshape((n,n)) #shaping lambda_r vector in order to reconstruct the matrix
#    lambdauT = lambdauT_r.reshape((n,m)) #shaping lambda_r vector in order to reconstruct the matrix
        
    
    
    # Defining bound constraint 
    if ymin is None and ymax is None:
        yFree = True
    else:
        yFree = False
        if ymin is None:
            ymin = -DM.inf(p)
        if ymax is None:
            ymax = DM.inf(p)
    if xmin is None:
        xmin = -DM.inf(n)
    if xmax is None:
        xmax = DM.inf(n)
    if umin is None:
        umin = -DM.inf(m)
    if umax is None:
        umax = DM.inf(m) 
    if Dumin is None and Dumax is None:
        DuFree = True
    else:
        DuFree = False
        if Dumin is None:
            Dumin = -DM.inf(m)
        if Dumax is None:
            Dumax = DM.inf(m)  
        
    if h is None:
        h = .1 #Defining integrating step if not provided from the user
    
    if ContForm is True:
        xdot = fx(xSX,uSX,dSX,tSX)
        y = Fy_model( xSX, uSX, dSX, tSX) 
        ystat = Fy_model( xstat, ustat, dSX, tSX)
        F_obj1 = F_obj(xSX, uSX, y, xstat, ustat, ystat)
        
        # Create an integrator
        dae = {'x':xSX, 'p':vertcat(uSX,dSX,tSX,xstat,ustat), 'ode':xdot, 'quad':F_obj1}
        opts = {'tf':h, 't0':0.0} # final time
        F = integrator('F', 'idas', dae, opts)
    
    # Initializing constraints vectors and obj fun
    g = []
    g1 = [] # Costraint vector for y bounds
    g2 = [] # Costraint vector for Du bounds
    f_obj = 0.0;
    
    # Defining soft constraint on y and x
#        slack = MX.sym("slack", n*N+p*N)
#        n_p = n+p
#        n_w = nw + n_p*N
#        eps = [slack[n_p*k : n_p*k+p] for k in range(N)]
#        eta = [slack[n_p*k+p: n_p*(k+1)] for k in range(N)]
#        if W == None:
#            W = DM.zeros(p,p)
#        if Z == None:
#            Z = DM.zeros(n,n)
    
    ys = Fy_model( xs, us, d, t) #Calculating steady-state output if necessary
    g.append(x0 - X[0])
    nqs = Qs.size1()
    for k in range(N):
        Y_k = Fy_model( X[k], U[k], d, t)
        
        # Correction for dynamic KKT matching
#        Thetax_k = mtimes(lambdaxT,(X[k] - xs)) + mtimes(lambdauT,(U[k] - us))
        
        if yFree is False:
            g1.append(Y_k) #bound constraint on Y_k
#        g.append(X[k] + eta[k])
#        g.append(X[k] - eta[k])
#        g.append(Y + eps[k])
#        g.append(Y - eps[k])        
#        g.append(DU)
        
        if ContForm is True: 
            Fk = F(x0=X[k], p=vertcat(U[k],d,t, xs, us))
            g.append(X[k+1] - Fk['xf'])# - Thetax_k)

            # Add contribution to the objective
            f_obj += Fk['qf']

        else:
            X_next = Fx_model( X[k], U[k], h, d, t) #- Thetax_k
            if k == 0:
                DU_k = U[k] - um1
            else:
                DU_k = U[k] - U[k-1]
            
            if DuFree is False:
                g2.append(DU_k) #bound constraint on DU_k
                
            g.append(X_next - X[k+1])
            # Defining variable entering the objective function
            dx = X[k] 
            du = U[k]
            dy = Y_k
            if QForm is True:   #Checking if the OF is quadratic
                dx = dx - xs
                du = du - us
                dy = dy - ys
            if DUForm is True:  #Checking if the OF requires DU instead of u
                du = DU_k
            
            dx_valv = X[k+1][:nqs] - X[k][:nqs]
            f_obj_new = F_obj( dx, du, dy, xs, us, ys) + xQx(dx_valv,Qs) + xQx(DU_k,S_adj)
            if k >= n_ws:
                f_obj += f_obj_new
#        Weps = mtimes( W,eps[k])
#        Zeta = mtimes(Z, eta[k])
#        f_obj = f_obj + mtimes(eps[k].T, Weps) + mtimes(eta[k].T, Zeta) 
        
    
    dx = X[N] - xs
#    if QForm is True:   #Checking if the OF is quadratic
#        dx = dx - xs
    if TermCons is True: #Adding the terminal constraint
        g.append(dx) 
        
    g = vertcat(*g)
    g1 = vertcat(*g1) #bound constraint on Y_k
    g2 = vertcat(*g2) #bound constraint on Du_k
    
    vfin = Vfin(dx)
    f_obj += vfin #adding the final weight
    
    #Defining bound constraint
    w_lb = -DM.inf(nw)
    w_ub = DM.inf(nw)
    w_lb[0:n] = xmin
    w_lb[0:n] = xmax
    
    ng = g.size1()
    ng1 = g1.size1()
    ng2 = g2.size1()
    g_lb = DM.zeros(ng+ng1+ng2,1)
    g_ub = DM.zeros(ng+ng1+ng2,1)
    
    
    DUMIN = Dumin 
    DUMAX = Dumax
    for k in range(1,N+1,1):
        w_lb[k*nxu:k*nxu+n] = xmin
        w_ub[k*nxu:k*nxu+n] = xmax
        w_lb[k*nxu-m:k*nxu] = umin
        w_ub[k*nxu-m:k*nxu] = umax
        
        if yFree is False:
            g_lb[ng+(k-1)*p: ng+k*p] = ymin
            g_ub[ng+(k-1)*p: ng+k*p] = ymax
            if DuFree is False:
                if k >= n_ws:
                    DUMIN = -DM.inf(m)
                    DUMAX = DM.inf(m)
                g_lb[ng+ng1+(k-1)*m: ng+ng1+k*m] = DUMIN
                g_ub[ng+ng1+(k-1)*m: ng+ng1+k*m] = DUMAX
        else:
            if DuFree is False:
                if k >= n_ws:
                    DUMIN = -DM.inf(m)
                    DUMAX = DM.inf(m)
                g_lb[ng+(k-1)*m: ng+k*m] = DUMIN
                g_ub[ng+(k-1)*m: ng+k*m] = DUMAX
    
    g = vertcat(g, g1, g2)
    
#    w_lb[nw : n_w] = DM.zeros(n_p*N) # Imposing slack variables greater than 0
    
       # Defining augmented bound constraint 
#        ng = n+2*n_p+m
#        g_lb = -DM.inf(N*ng)
#        g_ub = DM.inf(N*ng)
#        for k in range(N):    
#            g_lb[k*ng:k*ng+n] = 0   # Equalities identification 
#            g_ub[k*ng:k*ng+n] = 0
#            g_lb[k*ng+n: k*ng+2*n] = xmin
#            g_ub[k*ng+2*n: k*ng+3*n] = xmax
#            g_lb[k*ng+3*n: k*ng+3*n+p] = ymin
#            g_ub[k*ng+3*n+p: k*ng+3*n+2*p] = ymax
#            g_lb[k*ng+3*n+2*p: ng*(k+1)] = Dumin   # Delta_u constraint identification 
#            g_ub[k*ng+3*n+2*p: ng*(k+1)] = Dumax  
#            
#        # Defining augmented vector
#        w_aug = vertcat(w,slack)
    
    nlp = {'x':w, 'p':par, 'f':f_obj, 'g':g}

    solver = nlpsol('solver', 'ipopt', nlp, sol_opts)
#    solver = nlpsol('solver', 'qpoases', nlp)
    
    f_obj_guess = Function('f_obj_guess', [w,par], [f_obj])
    
    f_con_guess = Function('f_con_guess', [w,par], [g])
    
    [h,jf] = hessian(f_obj, w)
    H = Function('H',[w,par],[h])
    jac = jacobian(g,w)
    G = Function('G',[w,par],[jac])
    
    GF = Function('GF',[w,par],[jf])


    
    return [solver, w_lb, w_ub, g_lb, g_ub, f_obj_guess, f_con_guess,H,G,GF]
