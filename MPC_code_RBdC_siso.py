# -*- coding: utf-8 -*-
"""
Created on December 3, 2015

@author: RBdC

launch MPC codes
"""

from casadi import *
from casadi.tools import *
from matplotlib import pylab as plt
import math
import scipy.linalg as scla
import scipy.optimize as scopt
from scipy.integrate import odeint
import numpy as np
import time
from Utilities import*
from Target_Calc import *
from Estimator import *
from Control_Calc import *

# Loading example
type_figure = 'check_ws_and_fobj'               # 'nom_compare', 'check_ws_and_fobj', 'noise_levels', 'stic_param_fs', 'stic_param_fd'
type_mpc = 'aware'                              # 'unaware', 'stic_inv', 'aware'
if type_figure == 'nom_compare':
    N_MPC = 1
elif type_figure == 'check_ws_and_fobj':
    N_MPC = 4
    J_CL = np.zeros((N_MPC,1))
elif type_figure == 'noise_levels':
    N_MPC = 10
    r_wn = np.array([0, 1e-10, 1e-7, 1e-6, 1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1e0])
    J_CL = np.zeros((N_MPC,1))
elif type_figure == 'stic_param_fs':
    N_MPC = 9
    F_s = np.array([2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0])
    fs_p = 6.0
    J_CL = np.zeros((N_MPC,1))
    eF_s = np.zeros((N_MPC,1))
elif type_figure == 'stic_param_fd':
    N_MPC = 9
    F_d = np.array([0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0])
    fd_p = 4.0
    J_CL = np.zeros((N_MPC,1))
    eF_d = np.zeros((N_MPC,1))

for load_example in range(N_MPC):

######### con modello attrito di He standard
    OCP = 'yes'                  # OCP: yes do the control problem
    n_ws = 0                        # exclude first n_ws steps from dynamic objective function
    ssjacid = False 
    WS = 'yes'                      # WS: YES warm-start
    if type_figure == 'nom_compare':
        import sys
        sys.path.append('./nominal_compare/siso')
        pf = './nominal_compare/'
        WS = 'no'                      # WS: YES warm-start
        if load_example == 1:
            ## Caso unaware offset-free ("base + Dlin")
            ex_name = __import__('SISO_he_unaware')
        elif load_example == 2:
            ## Caso with stiction model and stiction inversion ("alla Heath")
            ex_name = __import__('SISO_he_stic_inv')        
        else:
            ## Caso aware double stiction model ("robusto")
            ex_name = __import__('SISO_he_aware')
            WS = 'yes'                      # WS: YES warm-start
            
    elif type_figure == 'check_ws_and_fobj':
        import sys
        sys.path.append('./nominal_compare/siso')
        pf = './nominal_compare/'
        ex_name = __import__('SISO_he_aware') 
        if load_example == 0:
            ## Caso "aware" stiction model ("robusto")       
            WS = 'no'                      # WS: YES warm-start
        elif load_example == 1:
            ## Caso "aware" OPC excluded
            OCP = 'no'                  # OCP: yes do the control problem
        elif load_example == 3:
            ## Caso aware double stiction model exlcuding first steps in OPC fobj  
            n_ws = 2                        # exclude first n_ws steps from dynamic objective function 
            
    elif type_figure == 'noise_levels':
        import sys
        sys.path.append('./effect_noise')
        pf = './effect_noise/' + str(r_wn[load_example]) + '/'
        if not os.path.exists(pf):
            os.makedirs(pf)
        
        if type_mpc == 'unaware':
        ## Caso unaware offset-free ("base + Dlin")
            from Ex_stic_he_miss_offset_free_MPC_noise import *
        elif type_mpc == 'aware':
        ## Caso aware double stiction model ("robusto")
            from Ex_stic_he_double_MPC_noise import *
        elif type_mpc == 'stic_inv':
        ## Caso with stiction model and stiction inversion ("alla Heath")
            from Ex_stic_he_stic_inv_MPC_noise import *
        # reset different noise levels
        R_wn = r_wn[load_example]*np.array([[1.0]])
    
    elif type_figure == 'stic_param_fs':   
        # reset stiction parameters
        # fs = F_s[load_example]
        import sys 
        pf = './param_fs/' + str(load_example + 1) + '/'
        if not os.path.exists(pf):
            os.makedirs(pf)
        if type_mpc == 'aware':
            sys.path.append('./param_fs/set_aware')
        ## Caso aware double stiction model ("robusto")
            if load_example == 0:
                from Ex_stic_he_double_MPC_1 import *
            elif load_example == 1:
                from Ex_stic_he_double_MPC_2 import *
            elif load_example == 2:
                from Ex_stic_he_double_MPC_3 import *
            elif load_example == 3:
                from Ex_stic_he_double_MPC_4 import *
            elif load_example == 4:
                from Ex_stic_he_double_MPC_5 import *
            elif load_example == 5:
                from Ex_stic_he_double_MPC_6 import *
            elif load_example == 6:
                from Ex_stic_he_double_MPC_7 import *
            elif load_example == 7:
                from Ex_stic_he_double_MPC_8 import *
            elif load_example == 8:
                from Ex_stic_he_double_MPC_9 import *
        elif type_mpc == 'stic_inv':
            sys.path.append('./param_fs/set_stic_inv')
            ## Caso with stiction model and stiction inversion ("alla Heath")
            if load_example == 0:    
                from Ex_stic_he_stic_inv_MPC_1 import *
            elif load_example == 1:    
                from Ex_stic_he_stic_inv_MPC_2 import *
            elif load_example == 2:    
                from Ex_stic_he_stic_inv_MPC_3 import *            
            elif load_example == 3:    
                from Ex_stic_he_stic_inv_MPC_4 import *      
            elif load_example == 4:    
                from Ex_stic_he_stic_inv_MPC_5 import *            
            elif load_example == 5:    
                from Ex_stic_he_stic_inv_MPC_6 import *            
            elif load_example == 6:    
                from Ex_stic_he_stic_inv_MPC_7 import *           
            elif load_example == 7:    
                from Ex_stic_he_stic_inv_MPC_8 import *            
            elif load_example == 8:    
                from Ex_stic_he_stic_inv_MPC_9 import *
       
    elif type_figure == 'stic_param_fd':   
        # reset stiction parameters
        # fs = F_s[load_example]
        ## Caso aware double stiction model ("robusto")
        import sys
        pf = './param_fd/' + str(load_example + 1) + '/'
        if not os.path.exists(pf):
            os.makedirs(pf)
        if type_mpc == 'aware':
            sys.path.append('./param_fd/set_aware')
            if load_example == 0:
                from Ex_stic_he_double_MPC_fd_1 import *
            if load_example == 1:
                from Ex_stic_he_double_MPC_fd_2 import *
            if load_example == 2:
                from Ex_stic_he_double_MPC_fd_3 import *
            if load_example == 3:
                from Ex_stic_he_double_MPC_fd_4 import *
            if load_example == 4:
                from Ex_stic_he_double_MPC_fd_5 import *
            if load_example == 5:
                from Ex_stic_he_double_MPC_fd_6 import *
            if load_example == 6:
                from Ex_stic_he_double_MPC_fd_7 import *
            if load_example == 7:
                from Ex_stic_he_double_MPC_fd_8 import *
            if load_example == 8:
                from Ex_stic_he_double_MPC_fd_9 import *
        elif type_mpc == 'stic_inv':
            sys.path.append('./param_fd/set_stic_inv')
            ## Caso with stiction model and stiction inversion ("alla Heath")
            if load_example == 0:    
                from Ex_stic_he_stic_inv_MPC_fd_1 import *
            elif load_example == 1:    
                from Ex_stic_he_stic_inv_MPC_fd_2 import *
            elif load_example == 2:    
                from Ex_stic_he_stic_inv_MPC_fd_3 import *            
            elif load_example == 3:    
                from Ex_stic_he_stic_inv_MPC_fd_4 import *      
            elif load_example == 4:    
                from Ex_stic_he_stic_inv_MPC_fd_5 import *            
            elif load_example == 5:    
                from Ex_stic_he_stic_inv_MPC_fd_6 import *            
            elif load_example == 6:    
                from Ex_stic_he_stic_inv_MPC_fd_7 import *           
            elif load_example == 7:    
                from Ex_stic_he_stic_inv_MPC_fd_8 import *            
            elif load_example == 8:    
                from Ex_stic_he_stic_inv_MPC_fd_9 import *
                    
    sys.modules['ex_name'] = ex_name
    from ex_name import * #Loading example
    
    ########## Dimensions #############################
    nx = x.size1()  # model state size                #
    nxp = xp.size1()# process state size              #
    nu = u.size1()  # control size                    #
    ny = y.size1()  # measured output/disturbance size#
    nd = d.size1()  # disturbance size                #
                                                      #
    nxu = nx + nu # state+control                     #
    nxuy = nx + nu + ny # state+control               #
    nw = nx*(N+1) + nu*N # total # of variables       #
    ###################################################
    
    ########## Fixed symbolic variables #########################################
    k = SX.sym("k", 1)  # step integration                                      #
    t = SX.sym("t", 1)  # time                                                  #
    cor = SX.sym('cor',ny) # correction for modifiers-adaption method           #
    dxp = SX.sym("dx", nxp) # discrete time process state disturbance           #  
    dyp = SX.sym("dy", ny)  # discrete time process output disturbance          #
    xs = SX.sym('xs',x.size1()) # stationary state value                        #  
    us = SX.sym('us',u.size1()) # stationary input value                        #
    ys = SX.sym('ys',y.size1()) # stationary output value                       #
    xsp = SX.sym('xsp',x.size1()) # state setpoint value                        #  
    usp = SX.sym('usp',u.size1()) # input setpoint value                        #
    ysp = SX.sym('ysp',y.size1()) # output setpoint value                       #
    #############################################################################
    
    if ssjacid is True:
        from SS_JAC_ID import *
        [A, B, C, D, xlin, ulin, ylin] = ss_p_jac_id(ex_name, nx, nu, ny, nd, k, t)
        # Build linearized model.
        if offree == "lin":
            [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, Bd = Bd, Cd = Cd, A = A, B = B, C = C, xlin = xlin, ulin = ulin, ylin = ylin)
        else:
            [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, A = A, B = B, C = C, xlin = xlin, ulin = ulin, ylin = ylin)
    else:
        #### Model calculation  #####################################################
        if 'User_fxm_Cont' in locals():
            if StateFeedback is True:
                if offree == "lin":
                    [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, Bd = Bd, Cd = Cd, fx = User_fxm_Cont, Mx = Mx, SF = StateFeedback)
                else:
                    [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, fx = User_fxm_Cont, Mx = Mx, SF = StateFeedback)
            else:
                if 'User_fym' in locals():
                    if offree == "lin":
                        [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, Bd = Bd, Cd = Cd, fx = User_fxm_Cont, Mx = Mx, fy = User_fym)
                    else:
                        [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, fx = User_fxm_Cont, Mx = Mx, fy = User_fym)
                else:
                    if offree == "lin":
                        [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, Bd = Bd, Cd = Cd, fx = User_fxm_Cont, Mx = Mx, C = C)
                    else:
                        [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, fx = User_fxm_Cont, Mx = Mx, C = C)
        elif 'User_fxm_Dis' in locals():
            if StateFeedback is True:
                if offree == "lin":
                    [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, Bd = Bd, Cd = Cd, Fx = User_fxm_Dis, SF = StateFeedback)
                else:
                    [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, Fx = User_fxm_Dis, SF = StateFeedback)
            else:
                if 'User_fym' in locals():
                    if offree == "lin":
                        [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, Bd = Bd, Cd = Cd, Fx = User_fxm_Dis, fy = User_fym)
                    else:
                        [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, Fx = User_fxm_Dis, fy = User_fym)
                else:
                    if offree == "lin":
                        [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, Bd = Bd, Cd = Cd, Fx = User_fxm_Dis, C = C)
                    else:
                        [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, Fx = User_fxm_Dis, C = C)
        elif 'A' in locals():
            if StateFeedback is True:
                if offree == "lin":
                    [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, Bd = Bd, Cd = Cd, A = A, B = B, SF = StateFeedback)
                else:
                    [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, A = A, B = B, SF = StateFeedback)
            else:
                if 'User_fym' in locals():
                    if 'xlin' in locals():
                        if offree == "lin":
                            [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, Bd = Bd, Cd = Cd, A = A, B = B, fy = User_fym, xlin = xlin, ulin = ulin)
                        else:
                            [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, A = A, B = B, fy = User_fym, xlin = xlin, ulin = ulin)
                    else:
                        if offree == "lin":
                            [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, Bd = Bd, Cd = Cd, A = A, B = B, fy = User_fym)
                        else:
                            [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, A = A, B = B, fy = User_fym)
                else:
                    if 'ylin' in locals():
                        if 'xlin' in locals():
                            if offree == "lin":
                                [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, Bd = Bd, Cd = Cd, A = A, B = B, C = C, xlin = xlin, ulin = ulin, ylin = ylin)
                            else:
                                [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, A = A, B = B, C = C, xlin = xlin, ulin = ulin, ylin = ylin)
                        else:
                            if offree == "lin":
                                [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, Bd = Bd, Cd = Cd, A = A, B = B, C = C, ylin = ylin)
                            else:        
                                [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, A = A, B = B, C = C, ylin = ylin)
                    elif 'xlin' in locals():
                            if offree == "lin":
                                [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, Bd = Bd, Cd = Cd, A = A, B = B, C = C, xlin = xlin, ulin = ulin)
                            else:
                                [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, A = A, B = B, C = C, xlin = xlin, ulin = ulin)
                    else:
                        if offree == "lin":
                            [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, Bd = Bd, Cd = Cd, A = A, B = B, C = C)
                        else:
                            [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, A = A, B = B, C = C)
        
    #############################################################################
            
    #### Plant equation  ########################################################
    if Fp_nominal is True:
        Fx_p = Fx_model
        Fy_p = Fy_model
    else:
        if 'Ap' in locals():
            if StateFeedback is True: # x = Ax + Bu ; y = x 
                [Fx_p,Fy_p] = defF_p(xp,u,y,k,t,dxp,dyp, Ap = Ap, Bp = Bp, SF = StateFeedback)
            elif 'User_fyp' in locals(): # x = Ax + Bu ; y = h(x,t)  
                [Fx_p,Fy_p] = defF_p(xp,u,y,k,t,dxp,dyp, Ap = Ap, Bp = Bp, fyp = User_fyp)
            else:
                [Fx_p,Fy_p] = defF_p(xp,u,y,k,t,dxp,dyp, Ap = Ap, Bp = Bp, Cp = Cp) 
        elif 'User_fxp_Dis' in locals(): 
            if StateFeedback is True: # x = F(x,t) ; y = x
                [Fx_p,Fy_p] = defF_p(xp,u,y,k,t,dxp,dyp, Fx = User_fxp_Dis, SF = StateFeedback)
            elif 'User_fyp' in locals():   # x = F(x,t) ; y = h(x,t)          
                [Fx_p,Fy_p] = defF_p(xp,u,y,k,t,dxp,dyp, Fx = User_fxp_Dis, fy = User_fyp) 
            elif 'Cp' in locals(): # x = F(x,t)  ; y = Cx 
                [Fx_p,Fy_p] = defF_p(xp,u,y,k,t,dxp,dyp, Fx = User_fxp_Dis, Cp = Cp)   
        elif 'User_fxp_Cont' in locals(): 
            if StateFeedback is True: # x = f(x,t) ; y = x
                [Fx_p,Fy_p] = defF_p(xp,u,y,k,t,dxp,dyp, fx = User_fxp_Cont, Mx = Mx, SF = StateFeedback)
            elif 'User_fyp' in locals(): # x = f(x,t) ; y = h(x,t) 
                [Fx_p,Fy_p] = defF_p(xp,u,y,k,t,dxp,dyp, fx = User_fxp_Cont, Mx = Mx, fy = User_fyp)
            else:  # x = f(x,t)  ; y = Cx 
                [Fx_p,Fy_p] = defF_p(xp,u,y,k,t,dxp,dyp, fx = User_fxp_Cont, Mx = Mx, Cp = Cp)
            
    #############################################################################
            
    #### Objective function calculation  ########################################
    if 'rss_y' in locals():
        if 'A' in locals() and 'C' in locals():
            Sol_Hess_constss = 'yes'        
        if 'rss_u' in locals():
            Fss_obj = defFss_obj(x, u, y, xsp, usp, ysp, r_y = rss_y, r_u = rss_u)
        else:
            Fss_obj = defFss_obj(x, u, y, xsp, usp, ysp, r_y = rss_y, r_Du = rss_Du)
            DUssForm = True    
    elif 'Qss' in locals():
        QForm_ss = True
        if 'A' in locals() and 'C' in locals():
            Sol_Hess_constss = 'yes'
        if 'Rss' in locals():
            Fss_obj = defFss_obj(x, u, y, xsp, usp, ysp, Q = Qss, R = Rss)
        else:
            Fss_obj = defFss_obj(x, u, y, xsp, usp, ysp, Q = Qss, S = Sss) 
            DUssForm = True
    elif 'User_fssobj' in locals():
        Fss_obj = defFss_obj(x, u, y, xsp, usp, ysp, f_obj = User_fssobj)
        
    if 'r_x' in locals():
        QForm = True
        if 'A' and 'C' in locals():
            Sol_Hess_constdyn = 'yes'
        if 'r_u' in locals():
            F_obj = defF_obj(x, u, y, xs, us, ys, r_x = r_x, r_u = r_u)
        else:
            F_obj = defF_obj(x, u, y, xs, us, ys, r_x = r_x, r_Du = r_Du)
            DUForm = True
    elif 'Q' in locals():
        QForm = True
        if 'A' and 'C' in locals():
            Sol_Hess_constdyn = 'yes'
        if 'R' in locals():
            F_obj = defF_obj(x, u, y, xs, us, ys, Q = Q, R = R)
        else:
            F_obj = defF_obj(x, u, y, xs, us, ys, Q = Q, S = S)
            DUForm = True
    elif 'User_fobj_Cont' in locals():
        ContForm = True
        F_obj = defF_obj(x, u, y, xs, us, ys, f_Cont = User_fobj_Cont)
    elif 'User_fobj_Dis' in locals():
        F_obj = defF_obj(x, u, y, xs, us, ys, f_Dis = User_fobj_Dis)
    
    if 'vfin' in locals():
        Vfin = defVfin(x, vfin = vfin)
    elif 'A' in locals():    # Linear system
        if 'Q' in locals():      # QP problem
            #Checking S and R matrix for Riccati Equation
            if 'S' in locals():
                R = S
            Vfin = defVfin(x, A = A, B = B, Q = Q, R = R)
    else:
        Vfin = defVfin(x)
    #############################################################################
    
    ### Solver options ##########################################################
    sol_optss = {'ipopt.max_iter':Sol_itmax, 'ipopt.hessian_constant':Sol_Hess_constss, 'ipopt.tol':1e-10}
    sol_optdyn = {'ipopt.max_iter':Sol_itmax, 'ipopt.hessian_constant':Sol_Hess_constdyn, 'ipopt.tol':1e-10} 
    
    
    #### Modifiers Adaptatation gradient definition  ############################
    if Adaptation is True: 
        (solver_ss1, wssp_lb, wssp_ub, gssp_lb, gssp_ub) = opt_ssp(nxp, nu, ny, nd, Fx_p,Fy_p, Fss_obj, QForm_ss,sol_optss, umin = umin, umax = umax, w_s = None, z_s = None, ymin = ymin, ymax = ymax, xmin = xmin, xmax = xmax, h = h)
        LambdaT = defLambdaT(xp,x,u,y,d,k,t,dxp,dyp, Fx_model, Fx_p, Fy_model, Fy_p)
    #############################################################################
    
    #### Solver definition  #####################################################
    (solver_ss, wss_lb, wss_ub, gss_lb, gss_ub) = opt_ss(nx, nu, ny, nd, offree,  Fx_model,Fy_model, Fss_obj, QForm_ss, DUssForm, Adaptation, sol_optss,umin = umin, umax = umax, w_s = None, z_s = None, ymin = ymin, ymax = ymax, xmin = xmin, xmax = xmax, h = h)
    
    if 'User_fxm_Cont' in locals():
        (solver, w_lb, w_ub, g_lb, g_ub, f_obj_guess, f_con_guess) = opt_dyn(x, u, y, d, t, nx, nu, ny, nd, offree,  Fx_model,Fy_model, F_obj,Vfin, N, QForm, DUForm, ContForm, TermCons, nw, sol_optdyn, umin = umin, umax = umax,  W = None, Z = None, ymin = ymin, ymax = ymax, xmin = xmin, xmax = xmax, Dumin = Dumin, Dumax = Dumax, h = h, fx = User_fxm_Cont, xstat = xs, ustat = us)
    else:
        (solver, w_lb, w_ub, g_lb, g_ub, f_obj_guess, f_con_guess) = opt_dyn(x, u, y, d, t, nx, nu, ny, nd, offree,  Fx_model,Fy_model, F_obj,Vfin, N, QForm, DUForm, ContForm, TermCons, nw, n_ws, sol_optdyn, umin = umin, umax = umax,  W = None, Z = None, ymin = ymin, ymax = ymax, xmin = xmin, xmax = xmax, Dumin = Dumin, Dumax = Dumax, h = h)
    #############################################################################
    
    #### Kalman steady-state gain definition  ###################################
    if kalss is True: 
        if offree == "lin":
            K = Kkalss(Fx_model, Fy_model, x, u, k, d, t, h, x_ss, u_ss, ny, nd, nx, Q_kf, R_kf, offree, Bd = Bd, Cd = Cd)
        else:
            K = Kkalss(Fx_model, Fy_model, x, u, k, d, t, h, x_ss, u_ss, ny, nd, nx, Q_kf, R_kf, offree)
    #############################################################################
    dx_p = DM.zeros(nxp,1) #Dummy variable when disturbance is not present
    dy_p = DM.zeros(ny,1) #Dummy variable when disturbance is not present
    x_k = x0_p
    u_k = u0
    xhat_k = x0_m
    lambdaT_k = DM.zeros(ny,nu)
    cor_k = 0.00*DM.ones(ny,1)
    try:
        P_k = P0
    except NameError:
        P_k = DM.zeros(nx+nd, nx+nd)
        
    dhat_k = DM.zeros(nd)
    Xp = []           # output minimi: Xp,Yp,x_hat,y_hat,U,d_hat,Xs,Us,Ys
    Yp = []           # output accessori: tempo computazionale per iterazione (magari spacchettato),E
    U = []
    XS = []
    YS = []
    US = []
    X_HAT = []
    Y_HAT = []
    D_HAT = []
    COR = []
    TIME_SS = []
    TIME_DYN = []
    Y_sp = []
    U_sp = []
    
    Esim = []
    for ksim in range(Nsim):
        t_k = ksim*h #real time updating
        
        # Updating process disturbances for the linear case
        if 'dkx_p' in locals():
            [dx_p] = defdxp(ksim)
        if 'dky_p' in locals():
            [dy_p] = defdyp(ksim)
        
        ## Store current state
        Xp.append(x_k)
        X_HAT.append(xhat_k)
        
        # Actual output 
        if Fp_nominal is True:
            y_k = Fy_p(x_k, u_k, dhat_k, t_k, cor_k)
        else: #All the other cases
            y_k = Fy_p(x_k,u_k,dy_p,t_k)        
            
            
        # Introducing white noise on output when present
        if 'R_wn' in locals():
            Rv = scla.sqrtm(R_wn)
            v_wn_k = mtimes(Rv,DM(np.random.normal(0,1,ny)))
            y_k = y_k + v_wn_k
            
        yhat_k = Fy_model(xhat_k,u_k, dhat_k, t_k)
        Yp.append(y_k)
        Y_HAT.append(yhat_k)
        
    ################ Estimator calling ############################################
        if offree != "no":
            x_es = vertcat(xhat_k,dhat_k)
            csi = SX.sym("csi", nx+nd)
            x1 = csi[0:nx]
            d1 = csi[nx:nx+nd]        
            Dx = Function('Dx',[d],[d])
            
            dummyFx = vertcat(Fx_model(x1,u,k,d1,t), Dx(d1))
            
            Fx_es = Function('Fx_es', [csi,u,k,t], [dummyFx])
            
            dummyFy = Fy_model(x1,u,d1,t)
            Fy_es = Function('Fy_es', [csi,u,t], [dummyFy])
            
        else:
            if nd != 0.0:
                import sys
                sys.exit("The disturbance dimension is not zero but no disturbance model has been selected")
            x_es = xhat_k
            dummyFx = Fx_model(x,u,k,d,t)
            Fx_es = Function('Fx_es', [x,u,k,t], [dummyFx])
            dummyFy = Fy_model(x,u,d,t)        
            Fy_es = Function('Fy_es', [x,u,t], [dummyFy])
        
        if kalss is True or lue is True: 
            estype = 'kalss'
            [x_es, kwargsout] = defEstimator(Fx_es,Fy_es,y_k,u_k, estype, x_es, t_k, K = K)
        else:
            if kal is True: # only for linear system
                if 'A' not in locals():
                    import sys
                    sys.exit("You cannot use the kalman filter if the model you have chosen is not linear")
                estype = 'kal'
            elif ekf is True: 
                estype = 'ekf'
            [x_es, kwargsout] = defEstimator(Fx_es,Fy_es,y_k,u_k, estype, x_es, t_k, cor_k, P_min = P_k, Q = Q_kf, R = R_kf, ts = h)
            P_k = kwargsout['P_plus']   
       
    #    # x(k|k) 
        if offree != "no":    
            xhat_k = x_es[0:nx]
            dhat_k = x_es[nx:nx+nd]
            
            # dhat_k saturation
            if dmin != None:
                for k_d in range(nd):
                    if dhat_k[k_d] < dmin[k_d]:
                        dhat_k[k_d] = dmin[k_d]
                    elif dhat_k[k_d] > dmax[k_d]:
                        dhat_k[k_d] = dmax[k_d]     
        else:
            xhat_k = x_es
        D_HAT.append(dhat_k)    
    ###############################################################################    
        # Check for feasibile condition
        if np.any(np.isnan(xhat_k.__array__())):
           import sys
           sys.exit("xhat_k has some components that are NaN. This is caused by an error in the integrator. Please check the white-noise or disturbance input: maybe they are too large!")
             
             
        ## Setpoint updating
        if (ksim==0):
            ysp_prev = yhat_k
        else:
            # previous value of set-point
            ysp_prev = ysp_k 
            
        [ysp_k, usp_k, xsp_k] = defSP(t_k)
        
        # U_sp
        if WS == 'yes':
            if ksim > 0:
                # Full warm start
                if WS_sp == 'full':
                    if ysp_k != ysp_prev:
                        k_chsp = ksim
                    if 'k_chsp' in locals() and ksim == k_chsp:
                        usp_k = usp_prev
                    elif 'k_chsp' in locals() and ksim >= k_chsp + 1.0 and ksim <= k_chsp + 2.0:
                        if u_k >= xs_k[0]:
                            usp_k = u_k + alfa*fs
                        else:
                            usp_k = u_k - alfa*fs 
                    elif 'k_chsp' in locals() and ksim > k_chsp + 2.0 and ksim <= k_chsp + 4.0:
                        if u_k >= xs_k[0]:
                            usp_k = xs_k[0] - fd
                        else:
                            usp_k = xs_k[0] + fd
                    elif 'k_chsp' in locals() and ksim > k_chsp + 4.0:
                        usp_k = usp_prev
                # partial warm start      
                elif WS_sp == 'partial':
                    if ysp_k != ysp_prev:
                        k_chsp = ksim
                    if 'k_chsp' in locals() and ksim == k_chsp:
                        usp_k = usp_prev
                    elif 'k_chsp' in locals() and ksim == k_chsp + 1.0:
                        if u_k >= xs_k[0]:
                            usp_k = u_k + alfa*fs
                        else:
                            usp_k = u_k - alfa*fs
                    elif 'k_chsp' in locals() and ksim == k_chsp + 2.0:
                        usp_k = usp_prev     
                    elif 'k_chsp' in locals() and ksim > k_chsp + 2.0:
                        if u_k >= xs_k[0]:
                            usp_k = xs_k[0] - fd
                        else:
                            usp_k = xs_k[0] + fd
                # simple warm start
                elif WS_sp == 'simple':
                    if ysp_k != ysp_prev:
                        k_chsp = ksim
                        usp_k = usp_prev
                    if 'k_chsp' in locals() and ksim == k_chsp + 1.0:    
                        if u_k >= xs_k[0]:
                            usp_k = xs_k[0] - fd
                        else:
                            usp_k = xs_k[0] + fd
                usp_prev = usp_k  

      
        Y_sp.append(ysp_k)
        U_sp.append(usp_k)
        
        if (ksim==0):
            us_k = u_k
            
        us_prev = us_k #just a reminder that this must be us_(k-1)
        
        lambdaT_k_r = lambdaT_k.reshape((nu*ny,1)) #shaping lambda matrix in order to enter par_ss
        
        ## Paramenter for Target optimization
        par_ss = vertcat(usp_k.T,ysp_k.T,xsp_k.T,dhat_k,us_prev,lambdaT_k_r,t_k)    
        
        ## Target calculation
        
        if (ksim==0) or (WS == 'no'):
            wss_guess = DM.zeros(nxuy)
            wss_guess[0:nx] = x0_m
            wss_guess[nx:nxu] = u0
            y0 = Fy_model(x0_m,u0,dhat_k,t_k)
            wss_guess[nxu:nxuy] = y0
        elif WS == 'yes':
            #
            wss_guess[0:nx] = xs_k
            uss_0 = u0
            # full warm start
            if WS_ss == 'full':   
                if ysp_k != ysp_prev:
                    k_chsp = ksim
                    uss_0 = uss_prev
                if 'k_chsp' in locals() and ksim >= k_chsp + 1.0 and ksim <= k_chsp + 2.0:
                    if u_k >= xs_k[0]:
                        uss_0 = u_k + alfa*fs
                    else:
                        uss_0 = u_k - alfa*fs
                elif 'k_chsp' in locals() and ksim > k_chsp + 2.0 and ksim <= k_chsp + 4.0:
                    if u_k >= xs_k[0]:
                        uss_0 = xs_k[0] - fd
                    else:
                        uss_0 = xs_k[0] + fd  
                elif 'k_chsp' in locals() and ksim > k_chsp + 4.0:
                    uss_0 = uss_prev
                else:
                    uss_0 = us_k
            # partial warm start        
            elif WS_ss == 'partial':
                if ysp_k != ysp_prev:
                    k_chsp = ksim
                    uss_0 = uss_prev
                if 'k_chsp' in locals() and ksim == k_chsp + 1.0:
                    if u_k >= xs_k[0]:
                        uss_0 = u_k + alfa*fs
                    else:
                        uss_0 = u_k - alfa*fs
                elif 'k_chsp' in locals() and ksim == k_chsp + 2.0:
                    uss_0 = us_prev
                elif 'k_chsp' in locals() and ksim > k_chsp + 2.0:   
                    if u_k >= xs_k[0]:
                        uss_0 = xs_k[0] - fd
                    else:
                        uss_0 = xs_k[0] + fd
                else:
                    uss_0 = us_k
            # simple warm start
            elif WS_ss == 'simple':
                if ysp_k != ysp_prev:
                    k_chsp = ksim
                    uss_0 = usp_prev
                if 'k_chsp' in locals() and ksim == k_chsp + 1.0: 
                    if u_k >= xs_k[0]:
                        uss_0 = xs_k[0] - fd
                    else:
                        uss_0 = xs_k[0] + fd
                else:
                    uss_0 = us_k
            #       
            wss_guess[nx:nxu] = uss_0
            uss_prev = uss_0
             #   
            wss_guess[nxu:nxuy] = ys_k
            
               
        start_time = time.time()    
        sol_ss = solver_ss(lbx = wss_lb,
                        ubx = wss_ub,
                        x0  = wss_guess,
                        p = par_ss,
                        lbg = gss_lb,
                        ubg = gss_ub)
                        
        etimess = time.time()-start_time
        wss_opt = sol_ss["x"]        
        xs_k = wss_opt[0:nx]
        us_k = wss_opt[nx:nxu]
        
        
        if Adaptation is True:
            # Updating correction for modifiers-adaptation method
            cor_k = mtimes(lambdaT_k,(us_k - us_prev))
        
        COR.append(cor_k)
        XS.append(xs_k)
        US.append(us_k)
        TIME_SS.append(etimess)
        ys_k = Fy_model(xs_k,us_k,dhat_k,t_k)
        YS.append(ys_k)
        
        ## Set current state as initial value
        w_lb[0:nx] = w_ub[0:nx] = xhat_k
        
        ## Set current targets 
        cur_tar = vertcat(xs_k,us_k)
        
        ## Final assignment    
       
        if WS == 'no':
            ## Initial guess (for unware MPC)
            if (ksim==0):
            ## Initial guess (on the first NLP run)
                w_guess = DM.zeros(nw)
                for key in range(N):
                    w_guess[key*nxu:key*nxu+nx] = x0_m
#                    w_guess[key*nxu:key*nxu+nx] = xs_k
                    w_guess[key*nxu+nx:(key+1)*nxu] = u0
#                    w_guess[key*nxu+nx:(key+1)*nxu] = us_k
                w_guess[N*nxu:N*nxu+nx] = x0_m  #x_N
#                w_guess[N*nxu:N*nxu+nx] = xs_k  #x_N
                #u_k = u0
            
        else:
            ## Initial guess (on the first NLP run)
            w_guess = DM.zeros(nw)
            u_guess = DM.zeros(N)
            y_guess = DM.zeros(N)
            mv_ss = xs_k[0]
            
            if u_k >= mv_ss:
                u_guess[0] = u_k + alfa*fs
                u_guess[1] = mv_ss - fd
            else:
                u_guess[0] = u_k - alfa*fs
                u_guess[1] = mv_ss + fd
                
            for key in range(2,N):
                u_guess[key] = u_guess[key-1]
            
            # bypass DYN OPT 
            if OCP == 'no':
                if ksim == 0:
                    U_Guess = DM.zeros(N)
                    i_cont = 0
                else:
                    # check change of set-point
                    if ysp_k != ysp_prev:
                        k_chsp = ksim
                        i_cont = 0
                    if 'k_chsp' in locals() and ksim >= k_chsp and ksim < k_chsp + 3.0:
                        U_Guess = u_guess
                        i_cont = i_cont + 1
                
            else:
                # set u_guess first trajectory from 2-moves compensation
                for key in range(N):
                    if key == 0:
                        w_guess[key*nxu:key*nxu+nx] = xhat_k
                    else:
                        w_guess[key*nxu:key*nxu+nx] = Fx_model(w_guess[(key-1)*nxu:(key-1)*nxu+nx], u_guess[key-1], h, dhat_k, t_k + h*(key-1))  
                    w_guess[key*nxu+nx:(key+1)*nxu] = u_guess[key]
                w_guess[N*nxu:N*nxu+nx] = Fx_model(w_guess[(N-1)*nxu:(N-1)*nxu+nx], u_guess[N-1], h, dhat_k, t_k + h*(N-1))  #x_N
                
                for key in range(N):
                    y_guess[key] = Fy_model(w_guess[(key)*nxu:(key)*nxu+nx],u_guess[key], dhat_k, t_k + h*(key))
           
        ## Solve the OCP
        
        if OCP == 'yes':
            ## Set parameter for dynamic optimisation
            par = vertcat(cur_tar,dhat_k,u_k,t_k,xhat_k)
            f_k = f_obj_guess(w_guess,par)
            g_k = f_con_guess(w_guess,par)
            start_time = time.time()
            if WS == 'yes':
                for i in range(N*nx):            
                    if g_k[i] > 1e-10:
                        import sys
                        sys.exit('w_guess infeasible')               
            
            sol = solver(lbx = w_lb,
                         ubx = w_ub,
                         x0  = w_guess,
                         p = par,
                         lbg = g_lb,
                         ubg = g_ub)
    
            etime = time.time()-start_time
            f_opt = sol["f"]
            w_opt = sol["x"]
            f_opt2 = f_obj_guess(w_opt,par)
            TIME_DYN.append(etime)
            ## Get the optimal input
            u_k = w_opt[nx:nxu]        
        else:
            # Set mpc output as the warm-start (always bypass OPC)
            u_k = U_Guess[i_cont]
             
        U.append(u_k)      
        
        if Fp_nominal is True:
            x_k = Fx_p(x_k, u_k, h, dhat_k)
    #    elif 'User_fxp_Cont' in locals(): # Non-linear continuous definition
    #        x_k = Fx_p(x_k, u_k, dx_p, t_k, h)
        else: # All the other cases
            x_k = Fx_p(x_k, u_k, dx_p, t_k, h)   
            
        
        # Check for feasibile condition
        if np.any(np.isnan(x_k.__array__())):
           import sys
           sys.exit("x_k has some components that are NaN. This is caused by an error in the integrator. Please check the white-noise or disturbance input: maybe they are too large!")
             
        # Introducing white noise on state when present
        if 'G_wn' in locals():
            Qw = scla.sqrtm(Q_wn)
            w_wn_k = mtimes(Qw,DM(np.random.normal(0,1,nxp)))    
            x_k = x_k + mtimes(G_wn,w_wn_k)
        
        
        ## Initial guess (warm start)
        if WS == 'no':
            # update w_guess
            w_guess = vertcat(w_opt[nxu:nw-nxu],us_k,xs_k,us_k,xs_k)
        
        ## Next predicted state (already including the disturbance estimate) 
        ## i.e.x(k|k-1)
        if 'User_fxm_Dis_2' in locals():
            xhat_k = User_fxm_Dis_2(xhat_k, u_k, dhat_k, t_k)       # in the case of 2 models
        else:
            xhat_k = Fx_model(xhat_k, u_k, h, dhat_k, t_k)          # in regular cases 
       
        if Adaptation is True:
            if (ksim==0):
                xs_kp = x_k
             
            par_ssp = vertcat(usp_k.T,ysp_k.T,xsp_k.T,dy_p,t_k)    
        
            ## Target process calculation
            wssp_guess = DM.zeros(nxp+nu+ny)
            wssp_guess[0:nxp] = x0_p
            wssp_guess[nxp:nxp+nu] = u0
            y0 = Fy_p(x0_p,u0,dy_p,t_k)
            wssp_guess[nxp+nu:nxp+nu+ny] = y0
                
            sol_ss = solver_ss1(lbx = wssp_lb,
                            ubx = wssp_ub,
                            x0  = wssp_guess,
                            p = par_ssp,
                            lbg = gssp_lb,
                            ubg = gssp_ub) 
            
            wssp_opt = sol_ss["x"]        
            xs_kp = wssp_opt[0:nx]
    
            ## Evaluate the updated term for the correction
            lambdaT_k = LambdaT(xs_kp,xs_k,us_k,dhat_k,ys_k,h,t_k,dx_p,dy_p, lambdaT_k)
            
    Xp = vertcat(*Xp)        # output minimi: Xp,Yp,x_hat,y_hat,U,d_hat,Xs,Us,Ys
    Yp = vertcat(*Yp)        # output accessori: tempo computazionale per iterazione (magari spacchettato),E
    U = vertcat(*U)
    XS = vertcat(*XS)
    YS = vertcat(*YS)
    US = vertcat(*US)
    X_HAT = vertcat(*X_HAT)
    Y_HAT = vertcat(*Y_HAT)
    D_HAT = vertcat(*D_HAT)
    COR = vertcat(*COR)
    TIME_SS = vertcat(*TIME_SS)
    TIME_DYN = vertcat(*TIME_DYN)
    Y_sp = vertcat(*Y_sp)
    U_sp = vertcat(*U_sp)
    
    tsim = plt.linspace(0, (Nsim-1)*h, Nsim)
    
    ## Objective function
    dU = np.diff(U.T)
    if 'User_fxm_Dis_2' in locals():
        C = Caug
    #
    if dyn_of_type == 'QP':
        if Nsim > 151:
            N_in = 150
        else:
            N_in = 0
        if Tuning == 'with_S':
            J_cl = np.sum(((Yp[N_in:Nsim-1] - Y_sp[N_in:Nsim-1])**2)/(np.max(np.array(C))**2)) + np.sum(S*dU[N_in:Nsim-1]**2)
        else:
            J_cl = np.sum(((Yp - Y_sp)**2)/(np.max(np.array(C))**2)) + np.sum(R*(U - US)**2)
    
    ## global time
    Tot_time = np.sum(TIME_DYN)
    
    plt.close("all")
    
    [X_HAT, XS, _] = makeplot(tsim,X_HAT,'State ',XS,path_figure = pf)
    [Xp, _ , _] = makeplot(tsim,Xp,'Process State ',path_figure = pf)  
    [U, US, U_sp] = makeplot(tsim,U,'Input ',US,U_sp,path_figure = pf)
    [Yp, YS, Y_sp] = makeplot(tsim,Yp,'Output ',YS,Y_sp,path_figure = pf)
    [D_HAT, _ , _] = makeplot(tsim,D_HAT,'Disturbance Estimate ',path_figure = pf)
    err = U - Xp[:,0]
    
            
    ## Figure overlapping   
    if N_MPC == 3 and type_figure == 'nom_compare':    # nominal compare
        
        # choose_MPC_type
        if load_example == 0: 
            Yp1 = Yp
            Yp2 = Yp
            U1 = U
            U2 = U
        elif load_example == 1:
            Yp1 = Yp
            U1 = U  
        # plot figures
        plt.figure(figsize=(6,18))
        #
        plt.subplot(2, 1, 1)
        # plt.plot(tsim, YS,'g:',linewidth=1.5)
        plt.plot(tsim, Y_sp,'g:',linewidth=1.5)
        plt.plot(tsim, Yp2,'r-',linewidth=1.5)
        plt.plot(tsim, Yp1,'b--',linewidth=1.5)
        plt.plot(tsim, Yp,'k-.',linewidth=1.5)
        plt.xlabel('time  [min]',fontsize=14)
        plt.ylabel('Output',fontsize=14)
        plt.axis([0.0, 1000.0, -10, 10])
        # plt.legend(('Target','Unaware','Stiction Inversion','Aware'),loc = 'upper right',fontsize = 10)
        # plt.legend(('Target','LMPC-0','LMPC-1','NMPC'),loc = 'upper right',fontsize = 10)
        plt.legend(('Set-Point','LMPC-0','LMPC-1','NMPC'),loc = 'upper right',fontsize = 10)
        # axes = plt.gca()
        #plt.grid(True)
        #
        plt.subplot(2, 1, 2)
        plt.plot(tsim, U2,'r-',linewidth=1.5,ls = 'steps')
        plt.plot(tsim, U1,'b--',linewidth=1.5,ls = 'steps')
        plt.plot(tsim, U,'k-.',linewidth=1.5,ls = 'steps')
        plt.xlabel('time  [min]',fontsize=14)
        plt.ylabel('Input',fontsize=14)
        plt.axis([0.0, 1000.0, -50, 40])
        # plt.legend(('Unaware','Stiction Inversion','Aware'),loc = 'upper right',fontsize = 10)
        plt.legend(('LMPC-0','LMPC-1','NMPC'),loc = 'lower right',fontsize = 10)
        # axes = plt.gca()
        #plt.grid(True)
        plt.tight_layout() # gives spacing between figures
        # plt.savefig(pf + 'nominal_compare.pdf', format = 'pdf', transparent = True, bbox_inches = 'tight')
        plt.savefig(pf + 'nominal_compare.png', format = 'png', transparent = True, bbox_inches = 'tight')
        #
        plt.show()
        
        # plot figures
        plt.figure(figsize=(12,12))
        #
        plt.subplot(2, 1, 1)
        # plt.plot(tsim, YS,'g:',linewidth=1.5)
        plt.plot(tsim, Y_sp,'g:',linewidth=1.5)
        plt.plot(tsim, Yp2,'r-',linewidth=1.5)
        plt.plot(tsim, Yp1,'b--',linewidth=1.5)
        plt.plot(tsim, Yp,'k-.',linewidth=1.5)
        plt.xlabel('time  [min]',fontsize=13)
        plt.ylabel('Output',fontsize=13)
        axes = plt.gca()
        # plt.legend(('Target','Unaware','Stiction Inversion','Aware'),loc = 'upper right',fontsize = 10)   
        #plt.legend(('Target','LMPC-0','LMPC-1','NMPC'),loc = 'upper right',fontsize = 10)
        plt.legend(('Set-Point','LMPC-0','LMPC-1','NMPC'),loc = 'upper right',fontsize = 10)
        
        #plt.grid(True)
        #
        plt.subplot(2, 1, 2)
        plt.plot(tsim, U2,'r-',linewidth=1.5,ls = 'steps')
        plt.plot(tsim, U1,'b--',linewidth=1.5,ls = 'steps')
        plt.plot(tsim, U,'k-.',linewidth=1.5,ls = 'steps')
        plt.xlabel('time  [min]',fontsize=13)
        plt.ylabel('Input',fontsize=13)
        axes = plt.gca()
        # plt.legend(('Unaware','Stiction Inversion','Aware'),loc = 'upper right',fontsize = 10)
        plt.legend(('LMPC-0','LMPC-1','NMPC'),loc = 'lower right',fontsize = 10)
        #plt.grid(True)
        plt.tight_layout() # gives spacing between figures
        plt.savefig(pf + 'nominal_compare_long.png', format = 'png', transparent = True, bbox_inches = 'tight')
        # plt.savefig(pf + 'nominal_compare_long.pdf', format = 'pdf', transparent = True, bbox_inches = 'tight')
        #
        plt.show()
    
    ## Figure overlapping   
    elif N_MPC == 4 and (type_figure == 'check_ws_and_fobj'):    # check ws and fobj corrected
        
        # choose_MPC_type
        if load_example == 0:
            Yp1 = Yp
            Yp2 = Yp
            Yp3 = Yp
            U1 = U
            U2 = U
            U3 = U
            X_1 = Xp
            X_2 = Xp
            X_3 = Xp     
        elif load_example == 1:
            Yp1 = Yp
            Yp2 = Yp
            U1 = U
            U2 = U
            X_1 = Xp
            X_2 = Xp 
        elif load_example == 2:
            Yp1 = Yp
            U1 = U
            X_1 = Xp

        # plot figures
        plt.figure(figsize=(6,18))
        #
        plt.subplot(3, 1, 1)
        # plt.plot(tsim, YS,'g:',linewidth=1.5)
        plt.plot(tsim, Y_sp,'g:',linewidth=1.5)
        plt.plot(tsim, Yp3,'r-',linewidth=1.5)
        plt.plot(tsim, Yp2,'co',linewidth=1.5)
        plt.plot(tsim, Yp1,'b--',linewidth=1.5)
        plt.plot(tsim, Yp,'k-.',linewidth=1.5)
        plt.xlabel('time  [min]',fontsize=14)
        plt.ylabel('Output',fontsize=14)
        plt.axis([0.0, 300, -1, 10])
        plt.legend(('Set-Point','NMPC','WS - no Dyn Mod','NMPC with ws','revised NMPC with ws'),loc = 'lower right',fontsize = 10)
        # axes = plt.gca()
        #plt.grid(True)
        #
        plt.subplot(3, 1, 2)
        plt.plot(tsim, U3,'r-',linewidth=1.5,ls = 'steps')
        plt.plot(tsim, U2,'co',linewidth=1.5,ls = 'steps')
        plt.plot(tsim, U1,'b--',linewidth=1.5,ls = 'steps')
        plt.plot(tsim, U,'k-.',linewidth=1.5,ls = 'steps')
        plt.xlabel('time  [min]',fontsize=14)
        plt.ylabel('Input',fontsize=14)
        plt.axis([0.0, 300, -1, 12])
        plt.legend(('NMPC','WS - no Dyn Mod','NMPC with ws','revised NMPC with ws'),loc = 'lower right',fontsize = 10)
        # axes = plt.gca()
        #plt.grid(True)
        #
        #
        plt.subplot(3, 1, 3)
#        plt.plot(tsim, X_3[:,0],'r-',linewidth=1.5)
#        plt.plot(tsim, X_2[:,0],'co',linewidth=1.5)
#        plt.plot(tsim, X_1[:,0],'b--',linewidth=1.5)
#        plt.plot(tsim, Xp[:,0],'k-.',linewidth=1.5)
        plt.xlabel('time  [min]',fontsize=14)
        plt.ylabel('Valve position',fontsize=14)
        plt.axis([0.0, 300, -1, 12])
        plt.legend(('NMPC','WS - no Dyn Mod','NMPC with ws','revised NMPC with ws'),loc = 'lower right',fontsize = 10)
        # axes = plt.gca()
        #plt.grid(True)
        #
        plt.tight_layout() # gives spacing between figures
        # plt.savefig(pf + 'nominal_compare.pdf', format = 'pdf', transparent = True, bbox_inches = 'tight')
        plt.savefig(pf + 'check_ws_and_fobj_siso.png', format = 'png', transparent = True, bbox_inches = 'tight')

        
    else:
        J_CL[load_example] = J_cl
        if type_figure == 'stic_param_fs':
            eF_s[load_example] = fs_p - fs
        if type_figure == 'stic_param_fd':
            eF_d[load_example] = fd_p - fd    
            

if type_figure == 'noise_levels':           # noise levels      
    # plot figures
    plt.figure(figsize=(6,18))
    plt.plot(r_wn, J_CL,'ro-')
    plt.xscale('log')
    plt.xlabel('$R_{wn}$', fontsize = 14)
    plt.ylabel('$J_{CL}$', fontsize = 14)
    plt.axis([np.amin(r_wn), 1.1*np.amax(r_wn), 0.98*np.amin(J_CL), 1.02*np.amax(J_CL)])
    plt.grid(True)
    #
    plt.savefig('./effect_noise/' + 'noise_effect.pdf', format = 'pdf', transparent = True, bbox_inches = 'tight')
    plt.show()
    
if type_figure == 'stic_param_fs':             # stiction parameters
    # plot figures
    plt.figure(figsize=(6,18))
    #
    # plt.subplot(2, 1, 1)
    plt.plot(eF_s, J_CL,'ro-')
    plt.xlabel('$e_{S}$', fontsize = 14)
    plt.ylabel('$J_{CL}$', fontsize = 14)
    plt.axis([np.amin(eF_s)-0.2, np.amax(eF_s)+0.2, 0.98*np.amin(J_CL), 1.02*np.amax(J_CL)])
    plt.grid(True)
    plt.savefig('./param_fs/' + 'stic_par_fs.pdf', format = 'pdf', transparent = True, bbox_inches = 'tight')
    plt.show()
    #

if type_figure == 'stic_param_fd':             # stiction parameters
    plt.figure(figsize=(6,18))    
    # plt.subplot(2, 1, 2)
    plt.plot(eF_d, J_CL,'ro-')
    plt.xlabel('$e_{D}$', fontsize = 14)
    plt.ylabel('$J_{CL}$', fontsize = 14)
    plt.axis([np.amin(eF_d)-0.2, np.amax(eF_d)+0.2, 0.98*np.amin(J_CL), 1.02*np.amax(J_CL)])
    plt.grid(True)
    plt.savefig('./param_fd/' + 'stic_par_fd.pdf', format = 'pdf', transparent = True, bbox_inches = 'tight')
    plt.show()
    
        
        
        
  