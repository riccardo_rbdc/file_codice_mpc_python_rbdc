# -*- coding: utf-8 -*-
"""


@author: RBdC

launch MPC codes (extended version, for MIMO systems: Quadruple Tank examples)
"""

from casadi import *
from casadi.tools import *
from matplotlib import pylab as plt
import math
import scipy.linalg as scla
import scipy.optimize as scopt
from scipy.integrate import odeint
import numpy as np
import time
from Utilities import*
from Target_Calc import *
from Estimator import *
from Control_Calc import *
from Default_Values import *
import sys
import scipy.io as sio #to manage savings of files and loading

# Loading example
type_figure = 'stic_param_fd'                 # 'nom_compare', nom_compare_LIN', 'check_ws_and_fobj', 'noise_levels', 'stic_param_fs', 'stic_param_fd'
type_mpc = 'aware'                      # 'unaware', 'stic_inv', 'aware'
u_in_figure = 'yes'                       # u in figure: yes, no
if type_figure == 'nom_compare':
    N_MPC = 1
elif type_figure == 'nom_compare_LIN':
    N_MPC = 1
elif type_figure == 'check_ws_and_fobj':
    N_MPC = 3 
elif type_figure == 'check_ws_and_fobj_LIN':
    N_MPC = 1 
    J_CL = np.zeros((N_MPC,1))    
elif type_figure == 'noise_levels':
    N_MPC = 10
    r_wn = np.array([0, 1e-9, 1e-6, 1e-3, 1e-2, 5e-2, 7.5e-2, 1e-1, 2.5e-1, 5e-1])
    J_CL = np.zeros((N_MPC,1))
elif type_figure == 'stic_param_fs':
#    F_s = np.array([2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0])
    F_s = np.array([3., 4., 4.5, 5.0, 5.5, 6.0, 6.5, 7.0, 7.5, 8.0, 9.0])
    N_MPC = F_s.shape[0]
    J_CL = np.zeros((N_MPC,1))
    eF_s = np.zeros((N_MPC,1))
    BYpYsp = np.zeros((N_MPC,1))
    BYpYsp2 = np.zeros((N_MPC,1))
    BXpXsp = np.zeros((N_MPC,1))
    BdXdX = np.zeros((N_MPC,1))
    
elif type_figure == 'stic_param_fd':
#    F_d = np.array([0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0])
    F_d = np.array([1.0, 1.2, 1.4, 1.6, 1.8, 2.0, 2.2, 2.4, 2.6, 2.8, 3.0])
    N_MPC = F_d.shape[0]
    J_CL = np.zeros((N_MPC,1))
    eF_d = np.zeros((N_MPC,1))
    BYpYsp = np.zeros((N_MPC,1))
    BYpYsp2 = np.zeros((N_MPC,1))
    BXpXsp = np.zeros((N_MPC,1))
    BdXdX = np.zeros((N_MPC,1))

for load_example in range(N_MPC):

######### Analisi con modello attrito di He semifisico
    OCP = 'yes'                  # OCP: yes do the control problem
    n_ws = 0                        # exclude first n_ws steps from dynamic objective function
    ssjacid = False 
    WS = 'yes'                      # WS: YES warm-start
    if type_figure == 'nom_compare':            # unaware & aware
        sys.path.append('./results/mimo')
        pf = './results/mimo/standard/nominal_compare/'
        if not os.path.exists(pf):
            os.makedirs(pf)
        if load_example == 1:           
            ## Caso unaware offset-free ("base + Dlin")                
            ex_name = __import__('MIMO_he_unaware')
            WS = 'no'                      # WS: YES warm-start
        else:
            ## Caso aware stiction model ("robusto")
            ex_name = __import__('MIMO_he_aware')  
        
           
    elif type_figure == 'nom_compare_LIN':      # unaware & inversion & aware
        sys.path.append('./results/mimo')
        pf = './results/mimo/standard/nominal_compare_LIN/'
        if not os.path.exists(pf):
            os.makedirs(pf)
        WS = 'no'                      # WS: YES warm-start
        
        if load_example == 2:       
            ## Caso unaware offset-free ("base + Dlin")
            ssjacid = True                 # True: compute ss initial values & Jacobian functions
            ex_name = __import__('MIMO_he_unaware')
            
        elif load_example == 1:
            ## Caso with stiction model and stiction inversion ("alla Heath")
            ssjacid = True                 # True: compute ss initial values & Jacobian functions
            ex_name = __import__('MIMO_he_stic_inv')
        
        else:
            ## Caso aware stiction model ("robusto")
            ex_name = __import__('MIMO_he_aware_LIN')
            WS = 'yes'                      # WS: YES warm-start
            
    
    elif type_figure == 'check_ws_and_fobj_LIN' or type_figure == 'check_ws_and_fobj':
        sys.path.append('./results/mimo')
        if type_figure == 'check_ws_and_fobj_LIN':
            pf = './results/mimo/standard/check_LIN/'
            ex_name = __import__('MIMO_he_aware_LIN') 
        else:
            pf = './results/mimo/standard/check/'
            ex_name = __import__('MIMO_he_aware') 
        # Saving path for results
        if not os.path.exists(pf):
                os.makedirs(pf)
        if load_example == 0:
            ## Caso "aware" stiction model ("robusto")      
            WS = 'no'                      # WS: YES warm-start   
        elif load_example == 1:
#            ## Caso aware OPC excluded ("robusto") + warm start
            OCP = 'no'                  # OCP: yes do the control problem
        elif load_example == 3:
            ## Caso aware exlcuding first steps in OPC fobj    
            n_ws = 4                        # exclude first n_ws steps from dynamic objective function 
            WS = 'yes'                      # WS: YES warm-start 
            
    elif type_figure == 'noise_levels':
        sys.path.append('./results/mimo')
        pf = './results/mimo/standard/effect_noise/' + type_mpc + '/' + str(r_wn[load_example]) + '/'
        if not os.path.exists(pf):
            os.makedirs(pf)
            
        if type_mpc == 'unaware':
            ## Caso unaware offset-free ("base + Dlin")
            ssjacid = True                 # True: compute ss initial values & Jacobian functions
            ex_name = __import__('MIMO_he_unaware')
            WS = 'no'                      # WS: YES warm-start
        elif type_mpc == 'stic_inv':
            ## Caso with stiction model and stiction inversion ("alla Heath")
            ssjacid = True                 # True: compute ss initial values & Jacobian functions
            ex_name = __import__('MIMO_he_stic_inv')
            WS = 'no'                      # WS: YES warm-start  
        elif type_mpc == 'aware':
            ## Caso aware stiction model ("robusto")
            #ex_name = __import__('MIMO_sp_he_aware_LIN')
            ex_name = __import__('MIMO_he_aware')
        # reset different noise levels                        
            
        R_wn = r_wn[load_example]*np.array([[1.0]])
    
    elif type_figure == 'stic_param_fs' or  type_figure == 'stic_param_fd':   
        # reset stiction parameters
        sys.path.append('./results/mimo')
        if type_figure == 'stic_param_fs':
            pf = './results/mimo/standard/param_fs/'+ type_mpc + '/' + str(load_example + 1) + '/'
        else:
            pf = './results/mimo/standard/param_fd/'+ type_mpc + '/' + str(load_example + 1) + '/'
        if not os.path.exists(pf):
            os.makedirs(pf)
            #
        if type_mpc == 'aware':
        ## Caso aware stiction model ("robusto")
            ex_name = __import__('MIMO_he_aware')
        elif type_mpc == 'stic_inv':
            ## Caso with stiction model and stiction inversion ("alla Heath")
            ex_name = __import__('MIMO_he_stic_inv')
            ssjacid = True                 # True: compute ss initial values & Jacobian functions
            WS = 'no'                      # WS: YES warm-start  

    sys.modules['ex_name'] = ex_name
    from ex_name import * #Loading example
#    Nsim = 200

    if type_figure == 'stic_param_fs' or type_figure == 'stic_param_fd': 
        import functools
        if type_figure == 'stic_param_fs':
            fs_1_m = F_s[load_example]
            fs_1 = 6.0
            User_fxp_Dis = functools.partial(User_fxp_Dis, fs_1_m = fs_1_m)
            User_fxm_Dis = functools.partial(User_fxm_Dis, fs_1_m = fs_1_m)
        else:
            fd_1_m = F_d[load_example]
            fd_1 = 2.0
#            fs_1 = fs_1_m = 8.0
            User_fxp_Dis = functools.partial(User_fxp_Dis, fd_1_m = fd_1_m)
            User_fxm_Dis = functools.partial(User_fxm_Dis, fd_1_m = fd_1_m)
        Dumin = vertcat(-alfa*fs_1_m, -DM.inf(u.size1()-1))
        Dumax = vertcat(alfa*fs_1_m, DM.inf(u.size1()-1))


    ########## Dimensions #############################
    nx = x.size1()  # model state size                #
    nxp = xp.size1()# process state size              #
    nu = u.size1()  # control size                    #
    ny = y.size1()  # measured output/disturbance size#
    nd = d.size1()  # disturbance size                #
                                                      #
    nxu = nx + nu # state+control                     #
    nxuy = nx + nu + ny # state+control               #
    nw = nx*(N+1) + nu*N # total # of variables       #
    ###################################################
    
    ########## Fixed symbolic variables #########################################
    k = SX.sym("k", 1)  # step integration                                      #
    t = SX.sym("t", 1)  # time                                                  #
#    cor = SX.sym('cor',ny) # correction for modifiers-adaption method           #
    dxp = SX.sym("dx", nxp) # discrete time process state disturbance           #  
    dyp = SX.sym("dy", ny)  # discrete time process output disturbance          #
    xs = SX.sym('xs',x.size1()) # stationary state value                        #  
    us = SX.sym('us',u.size1()) # stationary input value                        #
    ys = SX.sym('ys',y.size1()) # stationary output value                       #
    xsp = SX.sym('xsp',x.size1()) # state setpoint value                        #  
    usp = SX.sym('usp',u.size1()) # input setpoint value                        #
    ysp = SX.sym('ysp',y.size1()) # output setpoint value                       #
    #############################################################################


    if ssjacid is True:
        from SS_JAC_ID import *
        offree == "lin" # used for unaware when linearized
        [A, B, C, D, xlin, ulin, ylin] = ss_p_jac_id(ex_name, nx, nu, ny, nd, k, t)
        # Build linearized model.
        if offree == "lin":
            [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, Bd = Bd, Cd = Cd, A = A, B = B, C = C, xlin = xlin, ulin = ulin, ylin = ylin)
        else:
            [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, A = A, B = B, C = C, xlin = xlin, ulin = ulin, ylin = ylin)
    else:
        #### Model calculation  #####################################################
        if 'User_fxm_Cont' in locals():
            if StateFeedback is True:
                if offree == "lin":
                    [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, Bd = Bd, Cd = Cd, fx = User_fxm_Cont, Mx = Mx, SF = StateFeedback)
                else:
                    [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, fx = User_fxm_Cont, Mx = Mx, SF = StateFeedback)
            else:
                if 'User_fym' in locals():
                    if offree == "lin":
                        [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, Bd = Bd, Cd = Cd, fx = User_fxm_Cont, Mx = Mx, fy = User_fym)
                    else:
                        [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, fx = User_fxm_Cont, Mx = Mx, fy = User_fym)
                else:
                    if offree == "lin":
                        [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, Bd = Bd, Cd = Cd, fx = User_fxm_Cont, Mx = Mx, C = C)
                    else:
                        [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, fx = User_fxm_Cont, Mx = Mx, C = C)
        elif 'User_fxm_Dis' in locals():
            if StateFeedback is True:
                if offree == "lin":
                    [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, Bd = Bd, Cd = Cd, Fx = User_fxm_Dis, SF = StateFeedback)
                else:
                    [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, Fx = User_fxm_Dis, SF = StateFeedback)
            else:
                if 'User_fym' in locals():
                    if offree == "lin":
                        [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, Bd = Bd, Cd = Cd, Fx = User_fxm_Dis, fy = User_fym)
                    else:
                        [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, Fx = User_fxm_Dis, fy = User_fym)
                else:
                    if offree == "lin":
                        [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, Bd = Bd, Cd = Cd, Fx = User_fxm_Dis, C = C)
                    else:
                        [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, Fx = User_fxm_Dis, C = C)
        elif 'A' in locals():
            if StateFeedback is True:
                if offree == "lin":
                    [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, Bd = Bd, Cd = Cd, A = A, B = B, SF = StateFeedback)
                else:
                    [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, A = A, B = B, SF = StateFeedback)
            else:
                if 'User_fym' in locals():
                    if 'xlin' in locals():
                        if offree == "lin":
                            [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, Bd = Bd, Cd = Cd, A = A, B = B, fy = User_fym, xlin = xlin, ulin = ulin)
                        else:
                            [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, A = A, B = B, fy = User_fym, xlin = xlin, ulin = ulin)
                    else:
                        if offree == "lin":
                            [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, Bd = Bd, Cd = Cd, A = A, B = B, fy = User_fym)
                        else:
                            [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, A = A, B = B, fy = User_fym)
                else:
                    if 'ylin' in locals():
                        if 'xlin' in locals():
                            if offree == "lin":
                                [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, Bd = Bd, Cd = Cd, A = A, B = B, C = C, xlin = xlin, ulin = ulin, ylin = ylin)
                            else:
                                [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, A = A, B = B, C = C, xlin = xlin, ulin = ulin, ylin = ylin)
                        else:
                            if offree == "lin":
                                [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, Bd = Bd, Cd = Cd, A = A, B = B, C = C, ylin = ylin)
                            else:        
                                [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, A = A, B = B, C = C, ylin = ylin)
                    elif 'xlin' in locals():
                            if offree == "lin":
                                [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, Bd = Bd, Cd = Cd, A = A, B = B, C = C, xlin = xlin, ulin = ulin)
                            else:
                                [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, A = A, B = B, C = C, xlin = xlin, ulin = ulin)
                    else:
                        if offree == "lin":
                            [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, Bd = Bd, Cd = Cd, A = A, B = B, C = C)
                        else:
                            [Fx_model,Fy_model] = defF_model(x,u,y,d,k,t,offree, A = A, B = B, C = C)
    #############################################################################
            
    #### Plant equation  ########################################################
    if Fp_nominal is True:
        Fx_p = Fx_model
        Fy_p = Fy_model
    else:
        if 'Ap' in locals():
            if StateFeedback is True: # x = Ax + Bu ; y = x 
                [Fx_p,Fy_p] = defF_p(xp,u,y,k,t,dxp,dyp, Ap = Ap, Bp = Bp, SF = StateFeedback)
            elif 'User_fyp' in locals(): # x = Ax + Bu ; y = h(x,t)  
                [Fx_p,Fy_p] = defF_p(xp,u,y,k,t,dxp,dyp, Ap = Ap, Bp = Bp, fyp = User_fyp)
            else:
                [Fx_p,Fy_p] = defF_p(xp,u,y,k,t,dxp,dyp, Ap = Ap, Bp = Bp, Cp = Cp) 
        elif 'User_fxp_Dis' in locals(): 
            if StateFeedback is True: # x = F(x,t) ; y = x
                [Fx_p,Fy_p] = defF_p(xp,u,y,k,t,dxp,dyp, Fx = User_fxp_Dis, SF = StateFeedback)
            elif 'User_fyp' in locals():   # x = F(x,t) ; y = h(x,t)          
                [Fx_p,Fy_p] = defF_p(xp,u,y,k,t,dxp,dyp, Fx = User_fxp_Dis, fy = User_fyp) 
            elif 'Cp' in locals(): # x = F(x,t)  ; y = Cx 
                [Fx_p,Fy_p] = defF_p(xp,u,y,k,t,dxp,dyp, Fx = User_fxp_Dis, Cp = Cp)   
        elif 'User_fxp_Cont' in locals(): 
            if StateFeedback is True: # x = f(x,t) ; y = x
                [Fx_p,Fy_p] = defF_p(xp,u,y,k,t,dxp,dyp, fx = User_fxp_Cont, Mx = Mx, SF = StateFeedback)
            elif 'User_fyp' in locals(): # x = f(x,t) ; y = h(x,t) 
                [Fx_p,Fy_p] = defF_p(xp,u,y,k,t,dxp,dyp, fx = User_fxp_Cont, Mx = Mx, fy = User_fyp)
            else:  # x = f(x,t)  ; y = Cx 
                [Fx_p,Fy_p] = defF_p(xp,u,y,k,t,dxp,dyp, fx = User_fxp_Cont, Mx = Mx, Cp = Cp)
            
    #############################################################################
            
    #### Objective function calculation  ########################################
    if 'rss_y' in locals():
        if 'A' in locals() and 'C' in locals():
            Sol_Hess_constss = 'yes'        
        if 'rss_u' in locals():
            Fss_obj = defFss_obj(x, u, y, xsp, usp, ysp, r_y = rss_y, r_u = rss_u)
        else:
            Fss_obj = defFss_obj(x, u, y, xsp, usp, ysp, r_y = rss_y, r_Du = rss_Du)
            DUssForm = True    
    elif 'Qss' in locals():
        QForm_ss = True
        if 'A' in locals() and 'C' in locals():
            Sol_Hess_constss = 'yes'
        if 'Rss' in locals():
            Fss_obj = defFss_obj(x, u, y, xsp, usp, ysp, Q = Qss, R = Rss)
        else:
            Fss_obj = defFss_obj(x, u, y, xsp, usp, ysp, Q = Qss, S = Sss) 
            DUssForm = True
    elif 'User_fssobj' in locals():
        Fss_obj = defFss_obj(x, u, y, xsp, usp, ysp, f_obj = User_fssobj)
        
    if 'r_x' in locals():
        QForm = True
        if 'A' and 'C' in locals():
            Sol_Hess_constdyn = 'yes'
        if 'r_u' in locals():
            F_obj = defF_obj(x, u, y, xs, us, ys, r_x = r_x, r_u = r_u)
        else:
            F_obj = defF_obj(x, u, y, xs, us, ys, r_x = r_x, r_Du = r_Du)
            DUForm = True
    elif 'Q' in locals():
        QForm = True
        if 'A' and 'C' in locals():
            Sol_Hess_constdyn = 'yes'
        if 'R' in locals():
            F_obj = defF_obj(x, u, y, xs, us, ys, Q = Q, R = R)
        else:
            F_obj = defF_obj(x, u, y, xs, us, ys, Q = Q, S = S)
            DUForm = True
    elif 'User_fobj_Cont' in locals():
        ContForm = True
        F_obj = defF_obj(x, u, y, xs, us, ys, f_Cont = User_fobj_Cont)
    elif 'User_fobj_Dis' in locals():
        F_obj = defF_obj(x, u, y, xs, us, ys, f_Dis = User_fobj_Dis)
    
    if 'vfin' in locals():
        Vfin = defVfin(x, vfin = vfin)
    elif 'A' in locals():    # Linear system
        if 'Q' in locals():      # QP problem
            #Checking S and R matrix for Riccati Equation
            if 'S' in locals():
                R = S
            Vfin = defVfin(x, A = A, B = B, Q = Q, R = R)
    else:
        Vfin = defVfin(x)
    #############################################################################
    
    ### Solver options ##########################################################
    sol_optss = {'ipopt.max_iter':Sol_itmax, 'ipopt.hessian_constant':Sol_Hess_constss}
    sol_optdyn = {'ipopt.max_iter':Sol_itmax, 'ipopt.hessian_constant':Sol_Hess_constdyn} 
    
    
    #### Modifiers Adaptatation gradient definition  ############################
    if Adaptation is True: 
        (solver_ss1, wssp_lb, wssp_ub, gssp_lb, gssp_ub) = opt_ssp(nxp, nu, ny, nd, Fx_p,Fy_p, Fss_obj, QForm_ss,sol_optss, umin = umin, umax = umax, w_s = None, z_s = None, ymin = ymin, ymax = ymax, xmin = xmin, xmax = xmax, h = h)
        LambdaT = defLambdaT(xp,x,u,y,d,k,t,dxp,dyp, Fx_model, Fx_p, Fy_model, Fy_p)
    #############################################################################
    
    #### Solver definition  #####################################################
    (solver_ss, wss_lb, wss_ub, gss_lb, gss_ub) = opt_ss(nx, nu, ny, nd, offree,  Fx_model,Fy_model, Fss_obj, QForm_ss, DUssForm, Adaptation, sol_optss,umin = umin, umax = umax, w_s = None, z_s = None, ymin = ymin, ymax = ymax, xmin = xmin, xmax = xmax, h = h)
    
    Qvalv = Qs if 'Qs' in locals() else DM(0.)
    S_adj = S_adj if 'S_adj' in locals() else DM(0.)
    if 'User_fxm_Cont' in locals():
        (solver, w_lb, w_ub, g_lb, g_ub, f_obj_guess, f_con_guess,H,G,GF) = opt_dyn(x, u, y, d, t, nx, nu, ny, nd, offree,  Fx_model,Fy_model, F_obj,Vfin, N, QForm, DUForm, ContForm, TermCons, nw, n_ws, sol_optdyn, umin = umin, umax = umax,  W = None, Z = None, ymin = ymin, ymax = ymax, xmin = xmin, xmax = xmax, Dumin = Dumin, Dumax = Dumax, h = h, fx = User_fxm_Cont, xstat = xs, ustat = us, Qs = Qvalv, S_adj = S_adj)
    else:                                                                                                                                                
        (solver, w_lb, w_ub, g_lb, g_ub, f_obj_guess, f_con_guess,H,G,GF) = opt_dyn(x, u, y, d, t, nx, nu, ny, nd, offree,  Fx_model,Fy_model, F_obj,Vfin, N, QForm, DUForm, ContForm, TermCons, nw, n_ws, sol_optdyn, umin = umin, umax = umax,  W = None, Z = None, ymin = ymin, ymax = ymax, xmin = xmin, xmax = xmax, Dumin = Dumin, Dumax = Dumax, h = h, Qs = Qvalv, S_adj = S_adj)
    #############################################################################
    
    #### Kalman steady-state gain definition  ###################################
    if kalss is True: 
        if offree == "lin":
            K = Kkalss(Fx_model, Fy_model, x, u, k, d, t, h, x_ss, u_ss, ny, nd, nx, Q_kf, R_kf, offree, Bd = Bd, Cd = Cd)
        else:
            K = Kkalss(Fx_model, Fy_model, x, u, k, d, t, h, x_ss, u_ss, ny, nd, nx, Q_kf, R_kf, offree)
    #############################################################################
    dx_p = DM.zeros(nxp,1) #Dummy variable when disturbance is not present
    dy_p = DM.zeros(ny,1) #Dummy variable when disturbance is not present
    x_k = x0_p
    u_k = u0
    xhat_k = x0_m
    lambdaT_k = DM.zeros(ny,nu)
    cor_k = 0.00*DM.ones(ny,1)
    try:
        P_k = P0
    except NameError:
        P_k = DM.zeros(nx+nd, nx+nd)
        
    dhat_k = DM.zeros(nd)
    Xp = []           # output minimi: Xp,Yp,x_hat,y_hat,U,d_hat,Xs,Us,Ys
    Yp = []           # output accessori: tempo computazionale per iterazione (magari spacchettato),E
    U = []
    XS = []
    YS = []
    US = []
    X_HAT = []
    Y_HAT = []
    D_HAT = []
    COR = []
    TIME_SS = []
    TIME_DYN = []
    Y_sp = []
    U_sp = []
    
    Esim = []
    for ksim in range(Nsim):
        t_k = ksim*h #real time updating
        
        # Updating process disturbances for the linear case
        if 'dkx_p' in locals():
            [dx_p] = defdxp(ksim)
        if 'dky_p' in locals():
            [dy_p] = defdyp(ksim)
        
        ## Store current state
        Xp.append(x_k)
        X_HAT.append(xhat_k)
        
        # Actual output 
        if Fp_nominal is True:
            y_k = Fy_p(x_k, u_k, dhat_k, t_k)
        else: #All the other cases
            y_k = Fy_p(x_k,u_k,dy_p,t_k)        
            
            
        # Introducing white noise on output when present
        if 'R_wn' in locals():
            Rv = scla.sqrtm(R_wn)
            v_wn_k = mtimes(Rv,DM(np.random.normal(0,1,ny)))
            y_k = y_k + v_wn_k
            
        yhat_k = Fy_model(xhat_k,u_k, dhat_k, t_k)
        Yp.append(y_k)
        Y_HAT.append(yhat_k)
        
    ################ Estimator calling ############################################
        if offree != "no":
            x_es = vertcat(xhat_k,dhat_k)
            csi = SX.sym("csi", nx+nd)
            x1 = csi[0:nx]
            d1 = csi[nx:nx+nd]        
            Dx = Function('Dx',[d],[d])
            
            dummyFx = vertcat(Fx_model(x1,u,k,d1,t), Dx(d1))
            
            Fx_es = Function('Fx_es', [csi,u,k,t], [dummyFx])
            
            dummyFy = Fy_model(x1,u,d1,t)
            Fy_es = Function('Fy_es', [csi,u,t], [dummyFy])
            
        else:
            if nd != 0.0:
                import sys
                sys.exit("The disturbance dimension is not zero but no disturbance model has been selected")
            x_es = xhat_k
            dummyFx = Fx_model(x,u,k,d,t)
            Fx_es = Function('Fx_es', [x,u,k,t], [dummyFx])
            dummyFy = Fy_model(x,u,d,t)        
            Fy_es = Function('Fy_es', [x,u,t], [dummyFy])
        
        if kalss is True or lue is True: 
            estype = 'kalss'
            [x_es, kwargsout] = defEstimator(Fx_es,Fy_es,y_k,u_k, estype, x_es, t_k, K = K)
        else:
            if kal is True: # only for linear system
                if 'A' not in locals():
                    import sys
                    sys.exit("You cannot use the kalman filter if the model you have chosen is not linear")
                estype = 'kal'
            elif ekf is True: 
                estype = 'ekf'
            [x_es, kwargsout] = defEstimator(Fx_es,Fy_es,y_k,u_k, estype, x_es, t_k, P_min = P_k, Q = Q_kf, R = R_kf, ts = h)
            P_k = kwargsout['P_plus']   
       
    #    # x(k|k) 
        if offree != "no":    
            xhat_k = x_es[0:nx]
            dhat_k = x_es[nx:nx+nd]
            
            # dhat_k saturation
            if dmin != None:
                for k_d in range(nd):
                    if dhat_k[k_d] < dmin[k_d]:
                        dhat_k[k_d] = dmin[k_d]
                    elif dhat_k[k_d] > dmax[k_d]:
                        dhat_k[k_d] = dmax[k_d]     
        else:
            xhat_k = x_es
        D_HAT.append(dhat_k)    
    ###############################################################################    
        # Check for feasibile condition
        if np.any(np.isnan(xhat_k.__array__())):
           import sys
           sys.exit("xhat_k has some components that are NaN. This is caused by an error in the integrator. Please check the white-noise or disturbance input: maybe they are too large!")
             
             
        ## Setpoint updating
        if (ksim==0):
            ysp_prev = yhat_k
        else:
            # previous value of set-point
            ysp_prev = ysp_k
            
        [ysp_k, usp_k, xsp_k] = defSP(t_k)
        
        
        # U_sp
        if WS == 'yes':
            if ksim > 0:  
                # for jj in range(ny): 
                for i in range(nu):     # (nu = ny)
                    # Full warm start
                    if WS_sp == 'full':
                        # if ysp_k != ysp_prev:
                        if DM(ysp_k)[i] != DM(ysp_prev)[i]:
                            k_chsp = ksim                        
                        if 'k_chsp' in locals() and ksim == k_chsp:
                            usp_k[:,i] = usp_prev[:,i]
                        elif 'k_chsp' in locals() and ksim >= k_chsp + 1.0 and ksim <= k_chsp + 2.0:
                            if u_k[i] >= xs_k[i]:
                                usp_k[:,i] = u_k[i] + alfa*fs[i]
                            else:
                                usp_k[:,i] = u_k[i] - alfa*fs[i]
                        elif 'k_chsp' in locals() and ksim > k_chsp + 2.0 and ksim <= k_chsp + 4.0:
                            if u_k[i] >= xs_k[i]:
                                usp_k[:,i] = xs_k[i] - fd[i]
                            else:
                                usp_k[:,i] = xs_k[i] + fd[i]
                        elif 'k_chsp' in locals() and ksim > k_chsp + 4.0:
                            usp_k[:,i] = usp_prev[:,i]
                        # partial warm start      
                    elif WS_sp == 'partial':
                        # if ysp_k != ysp_prev:
                        if DM(ysp_k)[i] != DM(ysp_prev)[i]:
                            k_chsp = ksim
                        if 'k_chsp' in locals() and ksim == k_chsp:
                            usp_k[:,i] = usp_prev[:,i]
                        elif 'k_chsp' in locals() and ksim == k_chsp + 1.0:
                            if u_k[i] >= xs_k[i]:
                                usp_k[:,i] = u_k[i] + alfa*fs[i]
                            else:
                                usp_k[:,i] = u_k[i] - alfa*fs[i]
                        elif 'k_chsp' in locals() and ksim == k_chsp + 2.0:
                            usp_k[:,i] = usp_prev[:,i]   
                        elif 'k_chsp' in locals() and ksim > k_chsp + 2.0:
                            if u_k[i] >= xs_k[i]:
                                usp_k[:,i] = xs_k[i] - fd[i]
                            else:
                                usp_k[:,i] = xs_k[i] + fd[i]
                    # simple warm start
                    elif WS_sp == 'simple':
                         # if ysp_k != ysp_prev:
                        if DM(ysp_k)[i] != DM(ysp_prev)[i]:
                            k_chsp = ksim
                            usp_k[:,i] = usp_prev[:,i]
                        if 'k_chsp' in locals() and ksim == k_chsp + 1.0:    
                            if u_k[i] >= xs_k[i]:
                                usp_k[:,i] = xs_k[i] - fd
                            else:
                                usp_k[:,i] = xs_k[i] + fd
                    usp_prev = usp_k
                
                
        Y_sp.append(ysp_k.T)
        U_sp.append(usp_k.T)
        
        if (ksim==0):
            us_k = u_k
            
        us_prev = us_k #just a reminder that this must be us_(k-1)
        
        lambdaT_k_r = lambdaT_k.reshape((nu*ny,1)) #shaping lambda matrix in order to enter par_ss
        
        ## Paramenter for Target optimization
        par_ss = vertcat(usp_k.T,ysp_k.T,xsp_k.T,dhat_k,us_prev,lambdaT_k_r,t_k)    
        
        ## Target calculation: --> to be revised
        
        if (ksim==0) or (WS == 'no'):
            wss_guess = DM.zeros(nxuy)
            wss_guess[0:nx] = x0_m
            wss_guess[nx:nxu] = u0
            y0 = Fy_model(x0_m,u0,dhat_k,t_k)
            wss_guess[nxu:nxuy] = y0
        elif WS == 'yes':
            #
            wss_guess[0:nx] = xs_k
            uss_0 = u0
            for i in range(nu):     # (nu = ny)  
                # full warm start
                if WS_ss == 'full':
                    if DM(ysp_k)[i] != DM(ysp_prev)[i]:
                        k_chsp = ksim
                        uss_0[i] = uss_prev[i]
                    if 'k_chsp' in locals() and ksim >= k_chsp + 1.0 and ksim <= k_chsp + 2.0:
                        if u_k[i] >= xs_k[i]:
                            uss_0[i] = u_k[i] + alfa*fs[i]
                        else:
                            uss_0[i] = u_k[i] - alfa*fs[i]
                    elif 'k_chsp' in locals() and ksim > k_chsp + 2.0 and ksim <= k_chsp + 4.0:
                        if u_k[i] >= xs_k[i]:
                            uss_0[i] = xs_k[i] - fd[i]
                        else:
                            uss_0[i] = xs_k[i] + fd[i] 
                    elif 'k_chsp' in locals() and ksim > k_chsp + 4.0:
                        uss_0[i] = uss_prev[i]
                    else:
                        uss_0[i] = us_k[i]
                elif WS_ss == 'partial':
                    if DM(ysp_k)[i] != DM(ysp_prev)[i]:   
                        k_chsp = ksim
                        uss_0[i] = uss_prev[i]
                    if 'k_chsp' in locals() and ksim == k_chsp + 1.0:
                        if u_k[i] >= xs_k[i]:
                            uss_0[i] = u_k[i] + alfa*fs[i]
                        else:
                            uss_0[i] = u_k[i] - alfa*fs[i]
                    elif 'k_chsp' in locals() and ksim == k_chsp + 2.0:
                        uss_0[i] = uss_prev[i]
                    elif 'k_chsp' in locals() and ksim > k_chsp + 2.0:   
                        if u_k[i] >= xs_k[i]:
                            uss_0[i] = xs_k[i] - fd[i]
                        else:
                            uss_0[i] = xs_k[i] + fd[i]
                    else:
                        uss_0[i] = us_k[i]
                # partial warm start
                elif WS_ss == 'simple':
                    if DM(ysp_k)[i] != DM(ysp_prev)[i]:  
                        k_chsp = ksim
                        uss_0[i] = uss_prev[i]
                    if 'k_chsp' in locals() and ksim == k_chsp + 1.0: 
                        if u_k[i] >= xs_k[i]:
                            uss_0[i] = xs_k[i] - fd[i]
                        else:
                            uss_0[i] = xs_k[i] + fd[i]
                    else:
                        uss_0[i] = us_k[i]
                #ut_guess[i,:] = u_guess          
                #       
                wss_guess[nx:nxu] = uss_0
                uss_prev = uss_0
                 #   
                wss_guess[nxu:nxuy] = ys_k
            
        #       
        start_time = time.time()    
        sol_ss = solver_ss(lbx = wss_lb,
                        ubx = wss_ub,
                        x0  = wss_guess,
                        p = par_ss,
                        lbg = gss_lb,
                        ubg = gss_ub)
                        
        etimess = time.time()-start_time
        wss_opt = sol_ss["x"]        
        xs_k = wss_opt[0:nx]
        us_k = wss_opt[nx:nxu]
        
     
        if Adaptation is True:
            # Updating correction for modifiers-adaptation method
            cor_k = mtimes(lambdaT_k,(us_k - us_prev))
        
        COR.append(cor_k)
        XS.append(xs_k)
        US.append(us_k)
        TIME_SS.append(etimess)
        ys_k = Fy_model(xs_k,us_k,dhat_k,t_k)
        YS.append(ys_k)
        
        ## Set current state as initial value
        w_lb[0:nx] = w_ub[0:nx] = xhat_k
        
        ## Set current targets 
        cur_tar = vertcat(xs_k,us_k)
        
        ## Final assignment    
       
        if WS == 'no':
            # --> for stiction (unaware MPC)
            if (ksim==0):
            ## Initial guess (on the first NLP run)
                w_guess = DM.zeros(nw)
                for key in range(N):
                    w_guess[key*nxu:key*nxu+nx] = x0_m
                    w_guess[key*nxu+nx:(key+1)*nxu] = u0
                w_guess[N*nxu:N*nxu+nx] = x0_m  # x_N
                # u_k = u0
            
        else:
            # --> for stiction (aware MPC)
            # Initial guess (on the first NLP run)
            w_guess = DM.zeros(nw)
            u_guess = DM.zeros(nu*N)
            y_guess = DM.zeros(ny,N)
            mv_ss = xs_k[0:nu]
            fs = [fs_1_m, fs_2_m]
            fd = [fd_1_m, fd_2_m]
            
            
#            if u_k >= mv_ss:
#                u_guess[0] = u_k + alfa*fs
#                u_guess[1] = mv_ss - fd
#            else:
#                u_guess[0] = u_k - alfa*fs
#                u_guess[1] = mv_ss + fd
#                
#            for key in range(2,N):
#                u_guess[key] = u_guess[key-1]
            
            
            #
            #
            ## w_guess[0:1*nx]              # (X - 1st prediction)
            ## w_guess[nxu:nxu+nx]          # (X - 2rd prediction)
            ## w_guess[2*nxu:2*nxu+nx]      # (X - 3rd prediction)
            
            # warm start as stiction compensation --> 4-moves to be rivised with MV           
            
            for i in range(nu):
                if u_k[i] >= mv_ss[i]:
                    u_guess[i] = u_k[i] + alfa*fs[i]
                    u_guess[1*nu+i] = mv_ss[i] - fd[i]
                else:
                    u_guess[i] = u_k[i] - alfa*fs[i]
                    u_guess[1*nu+i] = mv_ss[i] + fd[i]
            #
            w_guess[nxu:nxu+nx] = Fx_model(xhat_k, u_guess[:nu], h, dhat_k, t_k)    
            #
#            for i in range(nu):
#                if u_guess[i] >= w_guess[nxu:nxu+nx][i]:
#                    u_guess[1*nu+i] = w_guess[nxu:nxu+nx][i] + fd[i]
#                else:
#                    u_guess[1*nu+i] = w_guess[nxu:nxu+nx][i] - fd[i]
#            #
#            w_guess[2*nxu:2*nxu+nx] = Fx_model(w_guess[nxu:nxu+nx], u_guess[nu:2*nu], h, dhat_k, t_k + 1*h)
#            #
#            
#            
#            
#            for i in range(nu):
#                if u_guess[1*nu+i] >= mv_ss[i]:                  
#                    u_guess[2*nu+i] = (1.0/M[i])*(mv_ss[i] - M[i]*fd[i] - (1.0 - M[i])*w_guess[2*nxu:2*nxu+nx][i])
#                else:
#                    u_guess[2*nu+i] = (1.0/M[i])*(mv_ss[i] + M[i]*fd[i] - (1.0 - M[i])*w_guess[2*nxu:2*nxu+nx][i]) 
#            #
#            w_guess[3*nxu:3*nxu+nx] = Fx_model(w_guess[2*nxu:2*nxu+nx], u_guess[2*nu:3*nu], h, dhat_k, t_k + 2*h)  
#            #
#            for i in range(nu):
#                if u_guess[2*nu+i] > w_guess[3*nxu:3*nxu+nx][i]:
#                    u_guess[3*nu+i] = w_guess[3*nxu:3*nxu+nx][i] + fd[i]
#                else:
#                    u_guess[3*nu+i] = w_guess[3*nxu:3*nxu+nx][i] - fd[i]
            #    
                    
            for key in range(2,N):
                u_guess[key*nu:(key+1)*nu] = u_guess[(key-1)*nu:key*nu]
                w_guess[key*nxu:key*nxu+nx] = Fx_model(w_guess[(key-1)*nxu:(key-1)*nxu+nx], u_guess[(key-1)*nu:key*nu], h, dhat_k, t_k + h*(key-1))
            #                               
            
            # bypass DYN OPT 
            if OCP == 'no':
                if ksim == 0:
                    U_Guess = DM.zeros(N*nu)
                    i_cont = 0
                else:
                    # check change of set-point          
                    for jj in range(ny):   
                        if DM(ysp_k)[jj] != DM(ysp_prev)[jj]:
                            k_chsp = ksim
                            i_cont = 0
                            j_chsp = jj
                        if 'k_chsp' in locals() and ksim >= k_chsp and ksim < k_chsp + 2.0 and jj == j_chsp:
                                U_Guess = u_guess
                                i_cont = i_cont + 1
                
            else:
                # set u_guess first trajectory from 2-moves compensation
                for key in range(N):
                    if key == 0:
                        w_guess[key*nxu:key*nxu+nx] = xhat_k
                    w_guess[key*nxu+nx:(key+1)*nxu] = u_guess[(key)*nu:(key+1)*nu]
                w_guess[N*nxu:N*nxu+nx] = Fx_model(w_guess[(N-1)*nxu:(N-1)*nxu+nx], u_guess[(N-1)*nu:N*nu], h, dhat_k, t_k + h*(N-1))  #x_N
                
#                for key in range(N):
#                    y_guess[:,key] = Fy_model(w_guess[(key)*nxu:(key)*nxu+nx],u_guess[(key)*nu:(key+1)*nu], dhat_k, t_k + h*(key))
#                if ksim == 0:    
#                    plt.figure(figsize=(6,18))    
#                    plt.plot(w_guess[0::nx+nu])
           
        ## Solve the OCP
        
        if OCP == 'yes':
            ## Set parameter for dynamic optimisation
            par = vertcat(cur_tar,dhat_k,u_k,t_k,xhat_k)
            f_k = f_obj_guess(w_guess,par)
            g_k = f_con_guess(w_guess,par)
            start_time = time.time()
            if WS == 'yes':
                for i in range(N*nx):            
                    if g_k[i] > 1e-10:
                        import sys
                        sys.exit('w_guess infeasible')
                        
            sol = solver(lbx = w_lb,
                         ubx = w_ub,
                         x0  = w_guess,
                         p = par,
                         lbg = g_lb,
                         ubg = g_ub)
                         
            etime = time.time()-start_time
            f_opt = sol["f"]
            w_opt = sol["x"]
            TIME_DYN.append(etime)
            ## Get the optimal input
            u_k = w_opt[nx:nxu]
        else:
            # Set mpc output as the warm-start (always bypass OPC)
            if ksim == 0:
                u_k = u0
            if i_cont != 0: 
                u_k = U_Guess[nu*(i_cont):nu*(i_cont+1)]
             
        U.append(u_k)
        
        if Fp_nominal is True:
            x_k = Fx_p(x_k, u_k, h, dhat_k)
        else: # Linear o non-linear discrete definition
            x_k = Fx_p(x_k, u_k, dx_p, t_k, h)        
            
        
        # Check for feasibile condition
        if np.any(np.isnan(x_k.__array__())):
           import sys
           sys.exit("x_k has some components that are NaN. This is caused by an error in the integrator. Please check the white-noise or disturbance input: maybe they are too large!")
             
        # Introducing white noise on state when present
        if 'G_wn' in locals():
            Qw = scla.sqrtm(Q_wn)
            w_wn_k = mtimes(Qw,DM(np.random.normal(0,1,nxp)))    
            x_k = x_k + mtimes(G_wn,w_wn_k)
        
        
        ## Initial guess (warm start)
        if WS == 'no':
            # update w_guess
            w_guess = vertcat(w_opt[nxu:nw-nxu],us_k,xs_k,us_k,xs_k)
        
        ## Next predicted state (already including the disturbance estimate) 
        ## i.e.x(k|k-1)
        if 'User_fxm_Dis_2' in locals():
            xhat_k = User_fxm_Dis_2(xhat_k, u_k, dhat_k, t_k)       # in the case of 2 models
        else:
            xhat_k = Fx_model(xhat_k, u_k, h, dhat_k, t_k)          # in regular cases 
       
        if Adaptation is True:
            if (ksim==0):
                xs_kp = x_k
             
            par_ssp = vertcat(usp_k.T,ysp_k.T,xsp_k.T,dy_p,t_k)    
        
            ## Target process calculation
            wssp_guess = DM.zeros(nxp+nu+ny)
            wssp_guess[0:nxp] = x0_p
            wssp_guess[nxp:nxp+nu] = u0
            y0 = Fy_p(x0_p,u0,dy_p,t_k)
            wssp_guess[nxp+nu:nxp+nu+ny] = y0
                
            sol_ss = solver_ss1(lbx = wssp_lb,
                            ubx = wssp_ub,
                            x0  = wssp_guess,
                            p = par_ssp,
                            lbg = gssp_lb,
                            ubg = gssp_ub) 
            
            wssp_opt = sol_ss["x"]        
            xs_kp = wssp_opt[0:nx]
    
            ## Evaluate the updated term for the correction
            lambdaT_k = LambdaT(xs_kp,xs_k,us_k,dhat_k,ys_k,h,t_k,dx_p,dy_p, lambdaT_k)
            
    Xp = vertcat(*Xp)        # output minimi: Xp,Yp,x_hat,y_hat,U,d_hat,Xs,Us,Ys
    Yp = vertcat(*Yp)        # output accessori: tempo computazionale per iterazione (magari spacchettato),  
    U = vertcat(*U)
    XS = vertcat(*XS)
    YS = vertcat(*YS)
    US = vertcat(*US)
    X_HAT = vertcat(*X_HAT)
    Y_HAT = vertcat(*Y_HAT)
    D_HAT = vertcat(*D_HAT)
    COR = vertcat(*COR)
    TIME_SS = vertcat(*TIME_SS)
    TIME_DYN = vertcat(*TIME_DYN)
    Y_sp = vertcat(*Y_sp)
    U_sp = vertcat(*U_sp)
    
    tsim = plt.linspace(0, (Nsim-1)*h, Nsim)
    
    ## Closed-loop Objective function
    
    if dyn_of_type == 'QP':
        # difference control action
#        dU = DM.zeros((nu*(Nsim-1),1))
        dX = DM.zeros((nu*(Nsim-1),1))
        X_sp = sio.loadmat('X_sp.mat')
        Xsp = X_sp['Xsp']
        #xpxsp = DM.zeros(Nsim,nu)
        XpXsp = 0.0
        dXdX = 0.0
#        URU = 0.0
#        ZQZ = 0.0
        for key in range(Nsim-1):
#            dU[(key)*nu:(key+1)*nu] = U[(key+1)*nu:(key+2)*nu] - U[(key)*nu:(key+1)*nu]
            if TYPE_mpc == 'SI': # stic inversion
                dX[(key)*nu:(key+1)*nu] = Xp[(key+1)*nxp:(key+2)*nxp][4:6] - Xp[(key)*nxp:(key+1)*nxp][4:6]
                dXdX = xQx(dX[(key)*nu:(key+1)*nu],S_adj) + dXdX
                xpxsp = Xp[key*nxp+4:key*nxp+4+nu] - Xsp[key,:]
                Rjcl = R
                #dXdX = xQx(dU[(key)*nu:(key+1)*nu],S_adj) + dXdX
                #URU = xQx((U - US)[(key+1)*nu:(key+2)*nu],R) + URU
            
            else:
                dX[(key)*nu:(key+1)*nu] = Xp[(key+1)*nxp:(key+2)*nxp][:nu] - Xp[(key)*nxp:(key+1)*nxp][:nu]
                if TYPE_mpc == 'SA':    # stic aware
                    dXdX = xQx(dX[(key)*nu:(key+1)*nu],Qs) + dXdX
                    Rjcl = Q[:nu,:nu]
                else:                   # stic unaware
                    dXdX = xQx(dX[(key)*nu:(key+1)*nu],S_adj) + dXdX
                    Rjcl = R
                xpxsp = Xp[key*nxp:key*nxp+nu] - Xsp[key,:]
            XpXsp = xQx(xpxsp,Rjcl) + XpXsp
#                    dXdX = xQx(dU[(key)*nu:(key+1)*nu],S_adj) + dXdX
#                    URU = xQx((U - US)[(key+1)*nu:(key+2)*nu],R) + URU
#            ZQZ = xQx((X_HAT - XS)[(key+1)*nx:(key+2)*nx],Q) + ZQZ
#        J_cl = ZQZ + dXdX + URU
        YpYsp = np.trace(mtimes((Yp - Y_sp).T,(Yp - Y_sp)))
        YpYsp2 = np.sum(np.abs((Yp-Y_sp).full()))           
        # CL Obj fun 
        J_cl = YpYsp + XpXsp + dXdX
    
    ## global time
    Tot_time = np.sum(TIME_DYN)
    
    plt.close("all")
    
    if load_example == N_MPC-1:        
        [X_HAT, XS, _] = makeplot(tsim,X_HAT,'State ',XS,path_figure = pf)
        [Xp, _ , _] = makeplot(tsim,Xp,'Process State ',path_figure = pf)  
    [U, US, U_sp] = makeplot(tsim,U,'Input ',US,U_sp,path_figure = pf, pltopt = 'steps')
    [Yp, YS, Y_sp] = makeplot(tsim,Yp,'Output ',YS,Y_sp,path_figure = pf)
    [D_HAT, _ , _] = makeplot(tsim,D_HAT,'Disturbance Estimate ',path_figure = pf)
    
    # saving x_sp from mpc aware (nominal case)    
#    sio.savemat('X_sp.mat', {'Xsp':XS[:,:nu]})
#    sio.savemat('X_sp.mat', {'Xsp':Xp[:Nsim*nu]})
        
    if Dis_mod == 'Input':
        dis = 'IDM'
    else:
        dis = 'ODM'
        
    ## Figure overlapping   
    if type_figure == 'nom_compare':    # nominal compare
        
        # choose_MPC_type
        if load_example == 0:
            Yp1 = Yp
            U1 = U
            X1 = Xp
        else:   
            # plot figures
            plt.figure(figsize=(18,18))
            #
            if u_in_figure == 'yes':
                plt.subplot(3, 2, 1)
            else:
                plt.subplot(2, 2, 1)
            plt.plot(tsim, Y_sp[:,0],'g:',linewidth=1.5)
            plt.plot(tsim, Yp1[:,0],'r-',linewidth=1.5)
            plt.plot(tsim, Yp[:,0],'k-.',linewidth=1.5)
            plt.xlabel('Time  [min]',fontsize=14)
            plt.ylabel('Output 1  [cm]',fontsize=14)
            plt.axis([0.0, (Nsim-1)*h, 5, 14])
            plt.legend(('Set-Point 1','Unaware','Stiction Embedding'),loc = 'upper right',fontsize = 10)
            #
            if u_in_figure == 'yes':
                plt.subplot(3, 2, 2)
            else:
                plt.subplot(2, 2, 2)
            plt.plot(tsim, Y_sp[:,1],'g:',linewidth=1.5)
            plt.plot(tsim, Yp1[:,1],'r-',linewidth=1.5)
            plt.plot(tsim, Yp[:,1],'k-.',linewidth=1.5)
            plt.xlabel('Time  [min]',fontsize=14)
            plt.ylabel('Output 2  [cm]',fontsize=14)
            plt.axis([0.0, (Nsim-1)*h, 5, 14])
            plt.legend(('Set-Point 2','Unaware','St. Embedding'),loc = 'upper right',fontsize = 10)
            #
            if u_in_figure == 'yes':
                plt.subplot(3, 2, 3)
                plt.plot(tsim, U1[:,0],'r-',linewidth=1.5,ls = 'steps')
                plt.plot(tsim, U[:,0],'k-.',linewidth=1.5,ls = 'steps')
                plt.xlabel('Time  [min]',fontsize=14)
                plt.ylabel('Input 1  [%]',fontsize=14)
                plt.axis([0.0, (Nsim-1)*h, 10, 60])
                plt.legend(('Unaware','St. Embedding'),loc = 'upper right',fontsize = 10)
                #        
                plt.subplot(3, 2, 4)
                plt.plot(tsim, U1[:,1],'r-',linewidth=1.5,ls = 'steps')
                plt.plot(tsim, U[:,1],'k-.',linewidth=1.5,ls = 'steps')
                plt.xlabel('Time  [min]',fontsize=14)
                plt.ylabel('Input 2  [%]',fontsize=14)
                plt.axis([0.0, (Nsim-1)*h, 10, 60])
                plt.legend(('Unaware','St. Embedding'),loc = 'upper right',fontsize = 10)
                #
            if u_in_figure == 'yes':
                plt.subplot(3, 2, 5)
            else:
                plt.subplot(2, 2, 3)    
            plt.plot(tsim, X1[0::6],'r-',linewidth=1.5)
            plt.plot(tsim, Xp[:,0],'k-.',linewidth=1.5)
    #            if load_example == N_MPC-1: 
    #                plt.plot(tsim, X1[:,0],'k-.',linewidth=1.5)
    #            else:
    #                plt.plot(tsim, X1[0::6],'k-.',linewidth=1.5)          
            plt.xlabel('time  [min]',fontsize=14)
            plt.ylabel('Valve 1 position [%]',fontsize=14)
            plt.axis([0.0, (Nsim-1)*h, 0, 75])
            plt.legend(('Unaware','St. Embedding'),loc = 'lower right',fontsize = 10)
            #

            if u_in_figure == 'yes':
                plt.subplot(3, 2, 6)
            else:
                plt.subplot(2, 2, 4)
            plt.plot(tsim, X1[1::6],'r-',linewidth=1.5)
            plt.plot(tsim, Xp[:,1],'k-.',linewidth=1.5)   
    #            if load_example == N_MPC-1: 
    #                plt.plot(tsim, X1[:,1],'k-.',linewidth=1.5)
    #            else:
    #                plt.plot(tsim, X1[1::6],'k-.',linewidth=1.5)          
            plt.xlabel('time  [min]',fontsize=14)
            plt.ylabel('Valve 2 position [%]',fontsize=14)
            plt.axis([0.0, (Nsim-1)*h, 0, 75])
            plt.legend(('Unaware','St. Embedding'),loc = 'lower right',fontsize = 10)
        
            
            plt.tight_layout() # gives spacing between figures
            plt.savefig(pf + 'nominal_compare.png', format = 'png', transparent = True, bbox_inches = 'tight')
            #
            plt.show()
        
        # plot disturbance
        if load_example == 0:
            D_HAT1 = D_HAT
        else:
        
            # plot figures
            plt.figure(figsize=(18,18))
            #
            plt.subplot(2, 1, 1)
            plt.plot(tsim, D_HAT1[:,0],'r-',linewidth=1.5)
            plt.plot(tsim, D_HAT[:,0],'k-.',linewidth=1.5)
            plt.xlabel('Time  [min]',fontsize=14)
            plt.ylabel('Disturbance 1  [cm]',fontsize=14)
            plt.legend(('Unaware','St. Embedding'),loc = 'upper right',fontsize = 10)
            #
            plt.subplot(2, 1, 2)
            plt.plot(tsim, D_HAT1[:,1],'r-',linewidth=1.5)
            plt.plot(tsim, D_HAT[:,1],'k-.',linewidth=1.5)
            plt.xlabel('Time  [min]',fontsize=14)
            plt.ylabel('Disturbance 2  [cm]',fontsize=14)
            plt.legend(('Unaware','St. Embedding'),loc = 'upper right',fontsize = 10)
            
            plt.tight_layout() # gives spacing between figures
            plt.savefig(pf + 'nominal_compare_dis.png', format = 'png', transparent = True, bbox_inches = 'tight')
            #
            plt.show()
            
            sio.savemat('mimo_nom_compare.mat', {'Ysp':Y_sp,'Yp1':Yp1,'Yp':Yp, 'U1':U1,'U':U, 'X1':X1.full(),'X':Xp,'D1':D_HAT1, 'D': D_HAT })
    
        
            ## Figure overlapping   
    elif type_figure == 'nom_compare_LIN':    # nominal compare
        
        # choose_MPC_type
        if load_example == 0: 
            Yp1 = Yp
            Yp2 = Yp
            U1 = U
            U2 = U
            X1 = Xp
            X2 = Xp
        elif load_example == 1:
            Yp1 = Yp
            U1 = U
            X1 = Xp
        else:
            # plot figures
            plt.figure(figsize=(18,18))
            #
            if u_in_figure == 'yes':
                plt.subplot(3, 2, 1)
            else:
                plt.subplot(2, 2, 1)
            plt.plot(tsim, Y_sp[:,0],'g:',linewidth=1.5)
            plt.plot(tsim, Yp2[:,0],'r-',linewidth=1.5)
            plt.plot(tsim, Yp1[:,0],'b--',linewidth=1.5)
            plt.plot(tsim, Yp[:,0],'k-.',linewidth=1.5)
            plt.xlabel('Time  [min]',fontsize=14)
            plt.ylabel('Output 1  [cm]',fontsize=14)
#            plt.axis([0.0, (Nsim-1)*h, 4, 13])
            plt.legend(('Set-Point 1','Unaware','St. Inversion','St. Embedding'),loc = 'upper right',fontsize = 10)
            #
            if u_in_figure == 'yes':
                plt.subplot(3, 2, 2)
            else:
                plt.subplot(2, 2, 2)
            plt.plot(tsim, Y_sp[:,1],'g:',linewidth=1.5)
            plt.plot(tsim, Yp2[:,1],'r-',linewidth=1.5)
            plt.plot(tsim, Yp1[:,1],'b--',linewidth=1.5)
            plt.plot(tsim, Yp[:,1],'k-.',linewidth=1.5)
            plt.xlabel('Time  [min]',fontsize=14)
            plt.ylabel('Output 2  [cm]',fontsize=14)
#            plt.axis([0.0, (Nsim-1)*h, 4, 13])
            plt.legend(('Set-Point 2','Unaware','St. Inversion','St. Embedding'),loc = 'upper right',fontsize = 10)
            #
            if u_in_figure == 'yes':
                plt.subplot(3, 2, 3)
                plt.plot(tsim, U2[:,0],'r-',linewidth=1.5,ls = 'steps')
                plt.plot(tsim, U1[:,0],'b--',linewidth=1.5,ls = 'steps')
                plt.plot(tsim, U[:,0],'k-.',linewidth=1.5,ls = 'steps')
                plt.xlabel('Time  [min]',fontsize=14)
                plt.ylabel('Input 1  [%]',fontsize=14)
#                plt.axis([0.0, (Nsim-1)*h, 25, 52])
                plt.legend(('Unaware','St. Inversion','St. Embedding'),loc = 'upper right',fontsize = 10)
                #
                plt.subplot(3, 2, 4)
                plt.plot(tsim, U2[:,1],'r-',linewidth=1.5,ls = 'steps')
                plt.plot(tsim, U1[:,1],'b--',linewidth=1.5,ls = 'steps')
                plt.plot(tsim, U[:,1],'k-.',linewidth=1.5,ls = 'steps')
                plt.xlabel('Time  [min]',fontsize=14)
                plt.ylabel('Input 2  [%]',fontsize=14)
#                plt.axis([0.0, 1000.0, 25, 52])
                plt.legend(('Unaware','St. Inversion','St. Embedding'),loc = 'upper right',fontsize = 10)
            #
            if u_in_figure == 'yes':
                plt.subplot(3, 2, 5)
            else:
                plt.subplot(2, 2, 3)
            plt.plot(tsim, X2[0::6],'r-',linewidth=1.5)
            plt.plot(tsim, X1[0::10],'b--',linewidth=1.5)
            plt.plot(tsim, Xp[:,0],'k-.',linewidth=1.5)
#            if load_example == N_MPC-1: 
#                plt.plot(tsim, X1[:,0],'k-.',linewidth=1.5)
#            else:
#                plt.plot(tsim, X1[0::6],'k-.',linewidth=1.5)          
            plt.xlabel('time  [min]',fontsize=14)
            plt.ylabel('Valve 1 position [%]',fontsize=14)
#            plt.axis([0.0, (Nsim-1)*h, 0, 100])
            plt.legend(('Unaware','St. Inversion','St. Embedding'),loc = 'lower right',fontsize = 10)
    
            if u_in_figure == 'yes':
                plt.subplot(3, 2, 6)
            else:
                plt.subplot(2, 2, 4)
            plt.plot(tsim, X2[1::6],'r-',linewidth=1.5)
            plt.plot(tsim, X1[1::10],'b--',linewidth=1.5)
            plt.plot(tsim, Xp[:,1],'k-.',linewidth=1.5)   
#            if load_example == N_MPC-1: 
#                plt.plot(tsim, X1[:,1],'k-.',linewidth=1.5)
#            else:
#                plt.plot(tsim, X1[1::6],'k-.',linewidth=1.5)          
            plt.xlabel('time  [min]',fontsize=14)
            plt.ylabel('Valve 2 position [%]',fontsize=14)
#            plt.axis([0.0, (Nsim-1)*h, 0, 100])
            plt.legend(('Unaware','St. Inversion','St. Embedding'),loc = 'lower right',fontsize = 10)
    
            
            
            plt.tight_layout() # gives spacing between figures
            plt.savefig(pf + 'nominal_compare_LIN.png', format = 'png', transparent = True, bbox_inches = 'tight')
            #
            plt.show()
        
        # plot disturbance          
        if load_example == 0: 
            D_HAT1 = D_HAT
            D_HAT2 = D_HAT
        elif load_example == 1:
            D_HAT1 = D_HAT
        else:
                
            # plot figures
            plt.figure(figsize=(18,18))
            #
            plt.subplot(2, 1, 1)
            plt.plot(tsim, D_HAT2[:,0],'r-',linewidth=1.5)
            plt.plot(tsim, D_HAT1[:,0],'b--',linewidth=1.5)
            plt.plot(tsim, D_HAT[:,0],'k-.',linewidth=1.5)
            plt.xlabel('Time  [min]',fontsize=14)
            plt.ylabel('Disturbance 1  [cm]',fontsize=14)
            plt.legend(('Unaware','St. Inversion','St. Embedding'),loc = 'upper right',fontsize = 10)
            # plt.legend((LMPC-0','LMPC-1','NMPC'),loc = 'upper right',fontsize = 10)
            #
            plt.subplot(2, 1, 2)
            plt.plot(tsim, D_HAT2[:,1],'r-',linewidth=1.5)
            plt.plot(tsim, D_HAT1[:,1],'b--',linewidth=1.5)
            plt.plot(tsim, D_HAT[:,1],'k-.',linewidth=1.5)
            plt.xlabel('Time  [min]',fontsize=14)
            plt.ylabel('Disturbance 2  [cm]',fontsize=14)
            plt.legend(('Unaware','St. Inversion','St. Embedding'),loc = 'upper right',fontsize = 10)
            # plt.legend(('Unaware','St. Inversion','St. Embedding'),loc = 'upper right',fontsize = 10)
            
            plt.tight_layout() # gives spacing between figures
            plt.savefig(pf + 'nominal_compare_LIN_dis.png', format = 'png', transparent = True, bbox_inches = 'tight')
            #
            plt.show()
        
            sio.savemat('mimo_compare_lin.mat', {'Ysp':Y_sp,'Yp':Yp,'Yp2':Yp2,'Yp1':Yp1, 'U':U,'U2':U2,'U1':U1,'X2':X2.full(),'X1':X1.full(),'Xp':Xp,'D2':D_HAT2, 'D1':D_HAT1, 'D': D_HAT })
    
    elif type_figure == 'check_ws_and_fobj':    # check ws and fobj corrected
        
        # choose_MPC_type
        if load_example == 0: 
            Yp1 = Yp
            Yp2 = Yp
            Yp3 = Yp
            U1 = U
            U2 = U
            U3 = U
            X1 = Xp
            X2 = Xp
            X3 = Xp
        elif load_example == 1: 
            Yp1 = Yp
            Yp2 = Yp
            U1 = U
            U2 = U
            X1 = Xp
            X2 = Xp
        elif load_example == 2:
            Yp1 = Yp
            U1 = U
            X1 = Xp
    
            # plot figures
            plt.figure(figsize=(18,18))
            #
            if u_in_figure == 'yes':
                plt.subplot(3, 2, 1)
            else:
                plt.subplot(2, 2, 1)
            
            plt.plot(tsim, Y_sp[:,0],'g:',linewidth=1.5)
            plt.plot(tsim, Yp3[:,0],'r-',linewidth=1.5)
            plt.plot(tsim, Yp2[:,0],'co',linewidth=1.5)
            plt.plot(tsim, Yp1[:,0],'b--',linewidth=1.5)
#            plt.plot(tsim, Yp[:,0],'k-.',linewidth=1.5)
            plt.xlabel('Time  [min]',fontsize=14)
            plt.ylabel('Output 1  [cm]',fontsize=14)
#            plt.axis([0.0, (Nsim-1)*h, 4, 13])
            plt.legend(('Set-Point 1','NMPC','WS - no Dyn Mod','NMPC with ws','revised NMPC with ws'),loc = 'lower right',fontsize = 10)
            #
            
            if u_in_figure == 'yes':
                plt.subplot(3, 2, 2)
            else:
                plt.subplot(3, 2, 2)            
            plt.plot(tsim, Y_sp[:,1],'g:',linewidth=1.5)
            plt.plot(tsim, Yp3[:,1],'r-',linewidth=1.5)
            plt.plot(tsim, Yp2[:,1],'co',linewidth=1.5)
            plt.plot(tsim, Yp1[:,1],'b--',linewidth=1.5)
#            plt.plot(tsim, Yp[:,1],'k-.',linewidth=1.5)
            plt.xlabel('Time  [min]',fontsize=14)
            plt.ylabel('Output 2  [cm]',fontsize=14)
#            plt.axis([0.0, (Nsim-1)*h, 4, 13])
            plt.legend(('Set-Point 2','NMPC','WS - no Dyn Mod','NMPC with ws','revised NMPC with ws'),loc = 'lower right',fontsize = 10)
            
            #
            if u_in_figure == 'yes':
                plt.subplot(3, 2, 3)
                plt.plot(tsim, U3[:,0],'r-',linewidth=1.5,ls = 'steps')
                plt.plot(tsim, U2[:,0],'co',linewidth=1.5,ls = 'steps')
                plt.plot(tsim, U1[:,0],'b--',linewidth=1.5,ls = 'steps')
        #            plt.plot(tsim, U[:,0],'k-.',linewidth=1.5,ls = 'steps')
                plt.xlabel('Time  [min]',fontsize=14)
                plt.ylabel('Input 1  [%]',fontsize=14)
        #            plt.axis([0.0, (Nsim-1)*h, 25, 52])
                plt.legend(('NMPC','WS - no Dyn Mod','NMPC with ws','revised NMPC with ws'),loc = 'lower right',fontsize = 10)
                #
                plt.subplot(3, 2, 4)
                plt.plot(tsim, U3[:,1],'r-',linewidth=1.5,ls = 'steps')
                plt.plot(tsim, U2[:,1],'co',linewidth=1.5,ls = 'steps')
                plt.plot(tsim, U1[:,1],'b--',linewidth=1.5,ls = 'steps')
#                plt.plot(tsim, U[:,1],'k-.',linewidth=1.5,ls = 'steps')
                plt.xlabel('Time  [min]',fontsize=14)
                plt.ylabel('Input 2  [%]',fontsize=14)
        #            plt.axis([0.0, (Nsim-1)*h, 25, 52])
                plt.legend(('NMPC','WS - no Dyn Mod','NMPC with ws','revised NMPC with ws'),loc = 'lower right',fontsize = 10)
            
            #
            if u_in_figure == 'yes':
                plt.subplot(3, 2, 5)
            else:
                plt.subplot(2, 2, 3)
            plt.plot(tsim, X3[0::6],'r-',linewidth=1.5)
            plt.plot(tsim, X2[0::6],'co',linewidth=1.5)
    #        plt.plot(tsim, X1[0::6],'b--',linewidth=1.5)
            if load_example == N_MPC-1: 
                plt.plot(tsim, X1[:,0],'b--',linewidth=1.5)
            else:
                plt.plot(tsim, X1[0::6],'b--',linewidth=1.5)          
            plt.xlabel('time  [min]',fontsize=14)
            plt.ylabel('Valve 1 position [%]',fontsize=14)
#            plt.axis([0.0, (Nsim-1)*h, 0, 100])
            plt.legend(('NMPC','WS - no Dyn Mod','NMPC with ws','revised NMPC with ws'),loc = 'lower right',fontsize = 10)
    
    
            if u_in_figure == 'yes':
                plt.subplot(3, 2, 6)
            else:
                plt.subplot(2, 2, 4)
            plt.plot(tsim, X3[1::6],'r-',linewidth=1.5)
            plt.plot(tsim, X2[1::6],'co',linewidth=1.5)
    #        plt.plot(tsim, X1[1::6],'b--',linewidth=1.5)
            if load_example == N_MPC-1: 
                plt.plot(tsim, X1[:,1],'b--',linewidth=1.5)
            else:
                plt.plot(tsim, X1[1::6],'b--',linewidth=1.5)          
            plt.xlabel('time  [min]',fontsize=14)
            plt.ylabel('Valve 2 position [%]',fontsize=14)
#            plt.axis([0.0, (Nsim-1)*h, 0, 100])
            plt.legend(('NMPC','WS - no Dyn Mod','NMPC with ws','revised NMPC with ws'),loc = 'lower right',fontsize = 10)
    
            # axes = plt.gca()
            #plt.grid(True)
            plt.tight_layout() # gives spacing between figures
            plt.savefig(pf + 'check_ws_and_fobj_mimo.png', format = 'png', transparent = True, bbox_inches = 'tight')
            
            sio.savemat('mimo_check.mat', {'Ysp':Y_sp,'Yp3':Yp3,'Yp2':Yp2,'Yp1':Yp1, 'U3':U3,'U2':U2,'U1':U1,'X3':X3.full(),'X2':X2.full(),'X1':X1 })
 
    elif type_figure == 'check_ws_and_fobj_LIN':    # check ws and fobj corrected
        
        # choose_MPC_type
        if load_example == 0: 
            Yp1 = Yp
            Yp2 = Yp
            Yp3 = Yp
            U1 = U
            U2 = U
            U3 = U
            X1 = Xp
            X2 = Xp
            X3 = Xp
        elif load_example == 1: 
            Yp1 = Yp
            Yp2 = Yp
            U1 = U
            U2 = U
            X1 = Xp
            X2 = Xp
        elif load_example == 2:
            Yp1 = Yp
            U1 = U
            X1 = Xp
    
            # plot figures
            plt.figure(figsize=(18,18))
            #
            if u_in_figure == 'yes':
                plt.subplot(3, 2, 1)
            else:
                plt.subplot(2, 2, 1)
            
            plt.plot(tsim, Y_sp[:,0],'g:',linewidth=1.5)
            plt.plot(tsim, Yp3[:,0],'r-',linewidth=1.5)
            plt.plot(tsim, Yp2[:,0],'co',linewidth=1.5)
            plt.plot(tsim, Yp1[:,0],'b--',linewidth=1.5)
#            plt.plot(tsim, Yp[:,0],'k-.',linewidth=1.5)
            plt.xlabel('Time  [min]',fontsize=14)
            plt.ylabel('Output 1  [cm]',fontsize=14)
#            plt.axis([0.0, (Nsim-1)*h, 4, 13])
            plt.legend(('Set-Point 1','NMPC','WS - no Dyn Mod','NMPC with ws','revised NMPC with ws'),loc = 'lower right',fontsize = 10)
            #
            
            if u_in_figure == 'yes':
                plt.subplot(3, 2, 2)
            else:
                plt.subplot(3, 2, 2)            
            plt.plot(tsim, Y_sp[:,1],'g:',linewidth=1.5)
            plt.plot(tsim, Yp3[:,1],'r-',linewidth=1.5)
            plt.plot(tsim, Yp2[:,1],'co',linewidth=1.5)
            plt.plot(tsim, Yp1[:,1],'b--',linewidth=1.5)
#            plt.plot(tsim, Yp[:,1],'k-.',linewidth=1.5)
            plt.xlabel('Time  [min]',fontsize=14)
            plt.ylabel('Output 2  [cm]',fontsize=14)
#            plt.axis([0.0, (Nsim-1)*h, 4, 13])
            plt.legend(('Set-Point 2','NMPC','WS - no Dyn Mod','NMPC with ws','revised NMPC with ws'),loc = 'lower right',fontsize = 10)
            
            #
            if u_in_figure == 'yes':
                plt.subplot(3, 2, 3)
                plt.plot(tsim, U3[:,0],'r-',linewidth=1.5,ls = 'steps')
                plt.plot(tsim, U2[:,0],'co',linewidth=1.5,ls = 'steps')
                plt.plot(tsim, U1[:,0],'b--',linewidth=1.5,ls = 'steps')
        #            plt.plot(tsim, U[:,0],'k-.',linewidth=1.5,ls = 'steps')
                plt.xlabel('Time  [min]',fontsize=14)
                plt.ylabel('Input 1  [%]',fontsize=14)
        #            plt.axis([0.0, (Nsim-1)*h, 25, 52])
                plt.legend(('NMPC','WS - no Dyn Mod','NMPC with ws','revised NMPC with ws'),loc = 'lower right',fontsize = 10)
                #
                plt.subplot(3, 2, 4)
                plt.plot(tsim, U3[:,1],'r-',linewidth=1.5,ls = 'steps')
                plt.plot(tsim, U2[:,1],'co',linewidth=1.5,ls = 'steps')
                plt.plot(tsim, U1[:,1],'b--',linewidth=1.5,ls = 'steps')
#                plt.plot(tsim, U[:,1],'k-.',linewidth=1.5,ls = 'steps')
                plt.xlabel('Time  [min]',fontsize=14)
                plt.ylabel('Input 2  [%]',fontsize=14)
        #            plt.axis([0.0, (Nsim-1)*h, 25, 52])
                plt.legend(('NMPC','WS - no Dyn Mod','NMPC with ws','revised NMPC with ws'),loc = 'lower right',fontsize = 10)
            
            #
            if u_in_figure == 'yes':
                plt.subplot(3, 2, 5)
            else:
                plt.subplot(2, 2, 3)
            plt.plot(tsim, X3[0::6],'r-',linewidth=1.5)
            plt.plot(tsim, X2[0::6],'co',linewidth=1.5)
    #        plt.plot(tsim, X1[0::6],'b--',linewidth=1.5)
            if load_example == N_MPC-1: 
                plt.plot(tsim, X1[:,0],'b--',linewidth=1.5)
            else:
                plt.plot(tsim, X1[0::6],'b--',linewidth=1.5)          
            plt.xlabel('time  [min]',fontsize=14)
            plt.ylabel('Valve 1 position [%]',fontsize=14)
#            plt.axis([0.0, (Nsim-1)*h, 0, 100])
            plt.legend(('NMPC','WS - no Dyn Mod','NMPC with ws','revised NMPC with ws'),loc = 'lower right',fontsize = 10)
    
    
            if u_in_figure == 'yes':
                plt.subplot(3, 2, 6)
            else:
                plt.subplot(2, 2, 4)
            plt.plot(tsim, X3[1::6],'r-',linewidth=1.5)
            plt.plot(tsim, X2[1::6],'co',linewidth=1.5)
    #        plt.plot(tsim, X1[1::6],'b--',linewidth=1.5)
            if load_example == N_MPC-1: 
                plt.plot(tsim, X1[:,1],'b--',linewidth=1.5)
            else:
                plt.plot(tsim, X1[1::6],'b--',linewidth=1.5)          
            plt.xlabel('time  [min]',fontsize=14)
            plt.ylabel('Valve 2 position [%]',fontsize=14)
#            plt.axis([0.0, (Nsim-1)*h, 0, 100])
            plt.legend(('NMPC','WS - no Dyn Mod','NMPC with ws','revised NMPC with ws'),loc = 'lower right',fontsize = 10)
    
            # axes = plt.gca()
            #plt.grid(True)
            plt.tight_layout() # gives spacing between figures
            plt.savefig(pf + 'check_ws_and_fobj_LIN_mimo.png', format = 'png', transparent = True, bbox_inches = 'tight')
            
            sio.savemat('mimo_check_LIN.mat', {'Ysp':Y_sp,'Yp3':Yp3,'Yp2':Yp2,'Yp1':Yp1, 'U3':U3,'U2':U2,'U1':U1,'X3':X3.full(),'X2':X2.full(),'X1':X1 })

    else:
        J_CL[load_example] = J_cl
        if type_figure == 'stic_param_fs':
            eF_s[load_example] = fs_1 - fs_1_m
        if type_figure == 'stic_param_fd':
            eF_d[load_example] = fd_1 - fd_1_m
        BYpYsp[load_example] = YpYsp
        BYpYsp2[load_example] = YpYsp2
        BXpXsp[load_example] = XpXsp
        BdXdX[load_example] = dXdX

if type_figure == 'noise_levels':           # noise levels      
    # plot figures
    plt.figure(figsize=(6,18))
    plt.plot(r_wn, J_CL,'ro-')
    plt.xscale('log')
    plt.xlabel('$R_{wn}$', fontsize = 14)
    plt.ylabel('$J_{CL}$', fontsize = 14)
    plt.axis([np.amin(r_wn), 1.1*np.amax(r_wn), 0.98*np.amin(J_CL), 1.02*np.amax(J_CL)])
    plt.grid(True)
    #
    plt.savefig('./results/mimo/standard/effect_noise/'+ type_mpc +'/effect_noise.png', format = 'png', transparent = True, bbox_inches = 'tight')
    plt.show()
    
    sio.savemat('JCL_noise_' + type_mpc + '_standard_'+ dis + '.mat', {'JCL':J_CL, 'r_wn':r_wn})
      
if type_figure == 'stic_param_fs':             # stiction parameters
    # plot figures
    plt.figure(figsize=(6,18))
    #
    # plt.subplot(2, 1, 1)
    plt.plot(eF_s, J_CL,'ro-')
    plt.xlabel('$e_{S}$', fontsize = 14)
    plt.ylabel('$J_{CL}$', fontsize = 14)
    plt.axis([np.amin(eF_s)-0.2, np.amax(eF_s)+0.2, 0.98*np.amin(J_CL), 1.02*np.amax(J_CL)])
    plt.grid(True)
    plt.savefig('./results/mimo/standard/param_fs/'+ type_mpc +'/stic_par_fs.png', format = 'png', transparent = True, bbox_inches = 'tight')
    plt.show()
    #
    sio.savemat('JCL_fs' + type_mpc + '_standard_'+ dis + '.mat', {'JCL':J_CL, 'eF_s':eF_s,     'BYpYsp':BYpYsp, 'BYpYsp2':BYpYsp2, 'BXpXsp':BXpXsp, 'BdXdX':BdXdX})

if type_figure == 'stic_param_fd':             # stiction parameters
    plt.figure(figsize=(6,18))    
    # plt.subplot(2, 1, 2)
    plt.plot(eF_d, J_CL,'ro-')
    plt.xlabel('$e_{D}$', fontsize = 14)
    plt.ylabel('$J_{CL}$', fontsize = 14)
    plt.axis([np.amin(eF_d)-0.2, np.amax(eF_d)+0.2, 0.98*np.amin(J_CL), 1.02*np.amax(J_CL)])
    plt.grid(True)
    plt.savefig('./results/mimo/standard/param_fd/'+ type_mpc +'stic_par_fd.png', format = 'png', transparent = True, bbox_inches = 'tight')
    plt.show()
    
    sio.savemat('JCL_fd' + type_mpc + '_standard_'+ dis + '.mat', {'JCL':J_CL, 'eF_d':eF_d,     'BYpYsp':BYpYsp, 'BYpYsp2':BYpYsp2, 'BXpXsp':BXpXsp, 'BdXdX':BdXdX})

        
        
        
  