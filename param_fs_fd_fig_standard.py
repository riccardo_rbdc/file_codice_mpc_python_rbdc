#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  4 08:26:18 2017

@author: marcovaccari
"""

from casadi import *
from casadi.tools import *
from matplotlib import pylab as plt
import math
import scipy.linalg as scla
import numpy as np

distmod = 'IDM'

import scipy.io as sio #to manage savings of files and loading
JCL_fsaware = sio.loadmat('JCL_fsaware_standard_' + distmod + '.mat')
JCL_fsstic_inv = sio.loadmat('JCL_fsstic_inv_standard_' + distmod + '.mat')

J_CL_aw = JCL_fsaware['JCL']
eF_s_aw = JCL_fsaware['eF_s']
J_CL_si = JCL_fsstic_inv['JCL']
YpYsp_aw = JCL_fsaware['BYpYsp']
YpYsp_si = JCL_fsstic_inv['BYpYsp']
YpYsp2_aw = JCL_fsaware['BYpYsp2']
YpYsp2_si = JCL_fsstic_inv['BYpYsp2']
XpXsp_aw = JCL_fsaware['BXpXsp']
XpXsp_si = JCL_fsstic_inv['BXpXsp']
dXdX_aw = JCL_fsaware['BdXdX']
dXdX_si = JCL_fsstic_inv['BdXdX']


pf = './results/mimo/standard/param_fs/'
if not os.path.exists(pf):
    os.makedirs(pf)

# plot figures
plt.figure(figsize=(6,18))

plt.plot(eF_s_aw, J_CL_si,'k^--') 
plt.plot(eF_s_aw, J_CL_aw,'ro-')
plt.yscale('log')
plt.xlabel('$e_{S}$', fontsize = 14)
plt.ylabel('$J_{CL}$', fontsize = 14)
plt.axis([np.amin(eF_s_aw)-0.2, np.amax(eF_s_aw)+0.2, 0.95*np.amin(J_CL_aw), 1.405*np.amax(J_CL_aw)])
plt.grid(True)
plt.legend(('st. inversion','st. embedding'),loc = 'best',fontsize = 10)
plt.savefig(pf + 'stic_par_fs' + distmod + '_1.png', format = 'png', transparent = True, bbox_inches = 'tight')
plt.show()

plt.figure(figsize=(6,18))

plt.plot(eF_s_aw, YpYsp2_si,'k^--') 
plt.plot(eF_s_aw, YpYsp2_aw,'ro-')
plt.yscale('log')
plt.xlabel('$e_{S}$', fontsize = 14)
plt.ylabel('$YpYsp2$', fontsize = 14)
plt.axis([np.amin(eF_s_aw)-0.2, np.amax(eF_s_aw)+0.2, 0.95*np.amin(YpYsp2_aw), 1.05*np.amax(YpYsp2_aw)])
plt.grid(True)
plt.legend(('st. inversion','st. embedding'),loc = 'best',fontsize = 10)
plt.savefig(pf + 'stic_par_fs' + distmod + '_2.png', format = 'png', transparent = True, bbox_inches = 'tight')
plt.show()

JCL_fdaware = sio.loadmat('JCL_fdaware_standard_' + distmod + '.mat')
JCL_fdstic_inv = sio.loadmat('JCL_fdstic_inv_standard_' + distmod + '.mat')

J_CL_aw = JCL_fdaware['JCL']
eF_d_aw = JCL_fdaware['eF_d']
J_CL_si = JCL_fdstic_inv['JCL']
YpYsp_aw = JCL_fdaware['BYpYsp']
YpYsp_si = JCL_fdstic_inv['BYpYsp']
YpYsp2_aw = JCL_fdaware['BYpYsp2']
YpYsp2_si = JCL_fdstic_inv['BYpYsp2']
XpXsp_aw = JCL_fdaware['BXpXsp']
XpXsp_si = JCL_fdstic_inv['BXpXsp']
dXdX_aw = JCL_fdaware['BdXdX']
dXdX_si = JCL_fdstic_inv['BdXdX']

pf = './results/mimo/standard/param_fd/'
if not os.path.exists(pf):
    os.makedirs(pf)
# plot figures
plt.figure(figsize=(6,18))

plt.plot(eF_d_aw, J_CL_si,'k^--') 
plt.plot(eF_d_aw, J_CL_aw,'ro-')
plt.yscale('log')
plt.xlabel('$e_{D}$', fontsize = 14)
plt.ylabel('$J_{CL}$', fontsize = 14)
plt.axis([np.amin(eF_d_aw)-0.2, np.amax(eF_d_aw)+0.2, 0.95*np.amin(J_CL_aw), 1.05*np.amax(J_CL_aw)])
plt.grid(True)
plt.legend(('st. inversion','st. embedding'),loc = 'best',fontsize = 10)
plt.savefig(pf + 'stic_par_fd' + distmod + '_1.png', format = 'png', transparent = True, bbox_inches = 'tight')
plt.show()

plt.figure(figsize=(6,18))

plt.plot(eF_s_aw, YpYsp2_si,'k^--') 
plt.plot(eF_s_aw, YpYsp2_aw,'ro-')
plt.yscale('log')
plt.xlabel('$e_{D}$', fontsize = 14)
plt.ylabel('$YpYsp2$', fontsize = 14)
plt.axis([np.amin(eF_s_aw)-0.2, np.amax(eF_d_aw)+0.2, 0.95*np.amin(YpYsp2_aw), 1.05*np.amax(YpYsp2_aw)])
plt.grid(True)
plt.legend(('st. inversion','st. embedding'),loc = 'best',fontsize = 10)
plt.savefig(pf + 'stic_par_fd' + distmod + '_2.png', format = 'png', transparent = True, bbox_inches = 'tight')
plt.show()

