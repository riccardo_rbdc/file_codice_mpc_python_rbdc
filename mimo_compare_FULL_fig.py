#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  4 08:26:18 2017

@author: marcovaccari & RBdC
"""

from casadi import *
from casadi.tools import *
from matplotlib import pylab as plt
import math
import scipy.linalg as scla
import numpy as np


# comparing NL cases
case = 'no_SI-FF'      # 'yes_SI-FF', 'no_SI-FF'
 
import scipy.io as sio #to manage savings of files and loading
mimo_nom_compare_FULL = sio.loadmat('mimo_nom_compare_FULL.mat')
# sio.savemat('mimo_nom_compare_FULL.mat', {'Ysp':Y_sp,'Yp4':Yp4,'Yp3':Yp3,'Yp2':Yp2,'Yp1':Yp1,'Yp':Yp, ...
# 'U4':U4,'U3':U3,'U2':U2,'U1':U1,'U':U,'X4':X4.full(),'X3':X3.full(),'X2':X2.full(),'X1':X1.full(),'X':Xp})
        
Y_sp = mimo_nom_compare_FULL['Ysp']
Yp = mimo_nom_compare_FULL['Yp']
Yp1 = mimo_nom_compare_FULL['Yp1']
Yp2 = mimo_nom_compare_FULL['Yp2']
Yp3 = mimo_nom_compare_FULL['Yp3']
Yp4 = mimo_nom_compare_FULL['Yp4']
U = mimo_nom_compare_FULL['U']
U1 = mimo_nom_compare_FULL['U1']
U2 = mimo_nom_compare_FULL['U2']
U3 = mimo_nom_compare_FULL['U3']
U4 = mimo_nom_compare_FULL['U4']
Xp = mimo_nom_compare_FULL['X']
X1 = mimo_nom_compare_FULL['X1']
X2 = mimo_nom_compare_FULL['X2']
X3 = mimo_nom_compare_FULL['X3']
X4 = mimo_nom_compare_FULL['X4']


pf = './results/mimo/sp/nominal_compare_FULL/'
if not os.path.exists(pf):
    os.makedirs(pf)

u_in_figure = 'yes'

Nsim = Y_sp.shape[0] 
h = 5
tsim = plt.linspace(0, (Nsim-1)*h, Nsim)

 # plot figures
fig=plt.figure(figsize=(18,18))

#
if u_in_figure == 'yes':
    plt.subplot(3, 2, 1)
else:
    plt.subplot(2, 2, 1)
l1, = plt.plot(tsim, Y_sp[:,0],'g:',linewidth=1.5)
l2, = plt.plot(tsim, Yp4[:,0],'c-',linewidth=1.5)
if case == 'yes_SI-FF':
    l3, = plt.plot(tsim, Yp3[:,0],'m-',linewidth=1.5)
l4, = plt.plot(tsim, Yp2[:,0],'r-',linewidth=1.5)
l5, = plt.plot(tsim, Yp1[:,0],'b-.',linewidth=1.5)
l6, = plt.plot(tsim, Yp[:,0],'k--',linewidth=1.5)
# plt.xlabel('Time  [min]',fontsize=14)
plt.ylabel('Output 1  [cm]',fontsize=14)
plt.axis([0.0, (Nsim-1)*h, 5, 14])
#plt.legend(('Set-Point 1','Unaware','Stiction Embedding'),loc = 'upper right',fontsize = 10)
#
if u_in_figure == 'yes':
    plt.subplot(3, 2, 2)
else:
    plt.subplot(2, 2, 2)
plt.plot(tsim, Y_sp[:,1],'g:',linewidth=1.5)
plt.plot(tsim, Yp4[:,1],'c-',linewidth=1.5)
if case == 'yes_SI-FF':
    plt.plot(tsim, Yp3[:,1],'m-',linewidth=1.5)
plt.plot(tsim, Yp2[:,1],'r-',linewidth=1.5)
plt.plot(tsim, Yp1[:,1],'b-.',linewidth=1.5)
plt.plot(tsim, Yp[:,1],'k--',linewidth=1.5)
#plt.xlabel('Time  [min]',fontsize=14)
plt.ylabel('Output 2  [cm]',fontsize=14)
plt.axis([0.0, (Nsim-1)*h, 5, 14])
#plt.legend(('Set-Point 2','Unaware','St. Embedding'),loc = 'upper right',fontsize = 10)
#
if u_in_figure == 'yes':
    plt.subplot(3, 2, 3)
    plt.plot(tsim, U4[:,0],'c-',linewidth=1.5) # ls = 'steps')
    if case == 'yes_SI-FF':
        plt.plot(tsim, U3[:,0],'m-',linewidth=1.5) # ls = 'steps')
    plt.plot(tsim, U2[:,0],'r-',linewidth=1.5) # ls = 'steps')
    plt.plot(tsim, U1[:,0],'b-.',linewidth=1.5) # ls = 'steps')
    plt.plot(tsim, U[:,0],'k--',linewidth=1.5) #ls = 'steps')
#    plt.xlabel('Time  [min]',fontsize=14)
    plt.ylabel('Input 1  [%]',fontsize=14)
    plt.axis([0.0, (Nsim-1)*h, 10, 60])
#    plt.legend(('Unaware','St. Embedding'),loc = 'upper right',fontsize = 10)
    #        
    plt.subplot(3, 2, 4)
    plt.plot(tsim, U4[:,1],'c-',linewidth=1.5) # ls = 'steps')
    if case == 'yes_SI-FF':
        plt.plot(tsim, U3[:,1],'m-',linewidth=1.5) # ls = 'steps')
    plt.plot(tsim, U2[:,1],'r-',linewidth=1.5) # ls = 'steps')
    plt.plot(tsim, U1[:,1],'b-.',linewidth=1.5) # ls = 'steps')
    plt.plot(tsim, U[:,1],'k--',linewidth=1.5) # ls = 'steps')
#    plt.xlabel('Time  [min]',fontsize=14)
    plt.ylabel('Input 2  [%]',fontsize=14)
    plt.axis([0.0, (Nsim-1)*h, 10, 60])
#    plt.legend(('Unaware','St. Embedding'),loc = 'upper right',fontsize = 10)
    #
if u_in_figure == 'yes':
    plt.subplot(3, 2, 5)
else:
    plt.subplot(2, 2, 3)
plt.plot(tsim, X4[0::6],'c-',linewidth=1.5)
if case == 'yes_SI-FF':
    plt.plot(tsim, X3[4::10],'m-',linewidth=1.5)
plt.plot(tsim, X2[0::6],'r-',linewidth=1.5)    
plt.plot(tsim, X1[4::10],'b-.',linewidth=1.5)
plt.plot(tsim, Xp[:,0],'k--',linewidth=1.5)
#            if load_example == N_MPC-1: 
#                plt.plot(tsim, X1[:,0],'k-.',linewidth=1.5)
#            else:
#                plt.plot(tsim, X1[0::6],'k-.',linewidth=1.5)          
plt.xlabel('Time  [min]',fontsize=14)
plt.ylabel('Valve 1 position [%]',fontsize=14)
#plt.axis([0.0, (Nsim-1)*h, 0, 75])
#plt.legend(('Unaware','St. Embedding'),loc = 'lower right',fontsize = 10)
#

if u_in_figure == 'yes':
    plt.subplot(3, 2, 6)
else:
    plt.subplot(2, 2, 4)
plt.plot(tsim, X4[1::6],'c-',linewidth=1.5)
if case == 'yes_SI-FF':
    plt.plot(tsim, X3[5::10],'m-',linewidth=1.5)
plt.plot(tsim, X2[1::6],'r-',linewidth=1.5)    
plt.plot(tsim, X1[5::10],'b-.',linewidth=1.5)
plt.plot(tsim, Xp[:,1],'k--',linewidth=1.5)  
#            if load_example == N_MPC-1: 
#                plt.plot(tsim, X1[:,1],'k-.',linewidth=1.5)
#            else:
#                plt.plot(tsim, X1[1::6],'k-.',linewidth=1.5)          
plt.xlabel('Time  [min]',fontsize=14)
plt.ylabel('Valve 2 position [%]',fontsize=14)
#plt.axis([0.0, (Nsim-1)*h, 0, 75])


       
art = []
if case == 'yes_SI-FF':
    lgd = plt.legend((l1,l2,l3,l4,l5,l6), ('Set-Point','NMPC-SF','LMPC-SF','Unaware','St. Inversion','St. Embedding'), loc = 'lower center',  bbox_to_anchor = (0,-0.04,1,1), ncol=6,
            bbox_transform = plt.gcf().transFigure)
else:
    lgd = plt.legend((l1,l2,l4,l5,l6), ('Set-Point','NMPC-SF','Unaware','St. Inversion','St. Embedding'), loc = 'lower center',  bbox_to_anchor = (0,-0.04,1,1), ncol=6,
            bbox_transform = plt.gcf().transFigure)
art.append(lgd)
plt.tight_layout() # gives spacing between figures
fig.subplots_adjust(wspace=0.12, hspace=0.15)
if case == 'yes_SI-FF':
    plt.savefig(pf + 'Very_FULL_compare.png', format = 'png', transparent = True, additional_artists=art, bbox_inches = 'tight')
else:
    plt.savefig(pf + 'FULL_compare.png', format = 'png', transparent = True, additional_artists=art, bbox_inches = 'tight')


## plot disturbance
## plot figures
#plt.figure(figsize=(18,18))
##
#plt.subplot(2, 1, 1)
#plt.plot(tsim, D_HAT1[:,0],'r-',linewidth=1.5)
#plt.plot(tsim, D_HAT[:,0],'k-.',linewidth=1.5)
#plt.xlabel('Time  [min]',fontsize=14)
#plt.ylabel('Disturbance 1  [cm]',fontsize=14)
#plt.legend(('Unaware','St. Embedding'),loc = 'upper right',fontsize = 10)
##
#plt.subplot(2, 1, 2)
#plt.plot(tsim, D_HAT1[:,1],'r-',linewidth=1.5)
#plt.plot(tsim, D_HAT[:,1],'k-.',linewidth=1.5)
#plt.xlabel('Time  [min]',fontsize=14)
#plt.ylabel('Disturbance 2  [cm]',fontsize=14)
#   
# #
##plt.legend(('Set-Point 2','Unaware','St. Inversion','St. Embedding'),loc=9, bbox_to_anchor=(0.5, -0.1), ncol=4)
#
#plt.show()




