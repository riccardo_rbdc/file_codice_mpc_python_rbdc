#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  4 08:26:18 2017

@author: marcovaccari
"""

from casadi import *
from casadi.tools import *
from matplotlib import pylab as plt
import math
import scipy.linalg as scla
import numpy as np



import scipy.io as sio #to manage savings of files and loading
mimo_check = sio.loadmat('mimo_check.mat')
#sio.savemat('mimo_nom_compare.mat', {'Ysp':Y_sp,'Yp1':Yp1,'Yp':Yp, 'U1':U1,'U':U, 'X1':X1.full(),'X':Xp,'D1':D_HAT1, 'D': D_HAT })
        
Y_sp = mimo_check['Ysp']
Yp3 = mimo_check['Yp3']
Yp1 = mimo_check['Yp1']
Yp2 = mimo_check['Yp2']
U3 = mimo_check['U3']
U1 = mimo_check['U1']
U2 = mimo_check['U2']

X3 = mimo_check['X3']
X2 = mimo_check['X2']
Xp = mimo_check['Xp']

pf = './results/mimo/check/'

u_in_figure = 'yes'

Nsim = Y_sp.shape[0] 
h = 5
tsim = plt.linspace(0, (Nsim-1)*h, Nsim)


fig = plt.figure(figsize=(18,18))
#
if u_in_figure == 'yes':
    plt.subplot(3, 2, 1)
else:
    plt.subplot(2, 2, 1)

l1, = plt.plot(tsim, Y_sp[:,0],'g:',linewidth=1.5)
l2, = plt.plot(tsim, Yp3[:,0],'r-',linewidth=1.5)
l3, = plt.plot(tsim, Yp2[:,0],'co',linewidth=1.5)
l4, = plt.plot(tsim, Yp1[:,0],'b--',linewidth=1.5)
#            plt.plot(tsim, Yp[:,0],'k-.',linewidth=1.5)
#plt.xlabel('Time  [min]',fontsize=14)
plt.ylabel('Output 1  [cm]',fontsize=14)
#            plt.axis([0.0, (Nsim-1)*h, 4, 13])
#plt.legend(('Set-Point 1','NMPC','WS - no Dyn Mod','NMPC with ws','revised NMPC with ws'),loc = 'lower right',fontsize = 10)
#

if u_in_figure == 'yes':
    plt.subplot(3, 2, 2)
else:
    plt.subplot(3, 2, 2)            
plt.plot(tsim, Y_sp[:,1],'g:',linewidth=1.5)
plt.plot(tsim, Yp3[:,1],'r-',linewidth=1.5)
plt.plot(tsim, Yp2[:,1],'co',linewidth=1.5)
plt.plot(tsim, Yp1[:,1],'b--',linewidth=1.5)
#            plt.plot(tsim, Yp[:,1],'k-.',linewidth=1.5)
#plt.xlabel('Time  [min]',fontsize=14)
plt.ylabel('Output 2  [cm]',fontsize=14)
#            plt.axis([0.0, (Nsim-1)*h, 4, 13])
#plt.legend(('Set-Point 2','NMPC','WS - no Dyn Mod','NMPC with ws','revised NMPC with ws'),loc = 'lower right',fontsize = 10)

#
if u_in_figure == 'yes':
    plt.subplot(3, 2, 3)
    plt.plot(tsim, U3[:,0],'r-',linewidth=1.5,ls = 'steps')
    plt.plot(tsim, U2[:,0],'co',linewidth=1.5,ls = 'steps')
    plt.plot(tsim, U1[:,0],'b--',linewidth=1.5,ls = 'steps')
#                plt.plot(tsim, U[:,0],'k-.',linewidth=1.5,ls = 'steps')
#    plt.xlabel('Time  [min]',fontsize=14)
    plt.ylabel('Input 1  [%]',fontsize=14)
#            plt.axis([0.0, (Nsim-1)*h, 25, 52])
#    plt.legend(('NMPC','WS - no Dyn Mod','NMPC with ws','revised NMPC with ws'),loc = 'lower right',fontsize = 10)
    #
    plt.subplot(3, 2, 4)
    plt.plot(tsim, U3[:,1],'r-',linewidth=1.5,ls = 'steps')
    plt.plot(tsim, U2[:,1],'co',linewidth=1.5,ls = 'steps')
    plt.plot(tsim, U1[:,1],'b--',linewidth=1.5,ls = 'steps')
#                plt.plot(tsim, U[:,1],'k-.',linewidth=1.5,ls = 'steps')
#    plt.xlabel('Time  [min]',fontsize=14)
    plt.ylabel('Input 2  [%]',fontsize=14)
#            plt.axis([0.0, (Nsim-1)*h, 25, 52])
#    plt.legend(('NMPC','WS - no Dyn Mod','NMPC with ws','revised NMPC with ws'),loc = 'lower right',fontsize = 10)

#
if u_in_figure == 'yes':
    plt.subplot(3, 2, 5)
else:
    plt.subplot(2, 2, 3)
plt.plot(tsim, X3[0::6],'r-',linewidth=1.5)
plt.plot(tsim, X2[0::6],'co',linewidth=1.5)
#            plt.plot(tsim, X1[0::6],'b--',linewidth=1.5)
plt.plot(tsim, Xp[:,0],'b--',linewidth=1.5)
plt.xlabel('Time  [min]',fontsize=14)
plt.ylabel('Valve 1 position [%]',fontsize=14)
#            plt.axis([0.0, (Nsim-1)*h, 0, 100])
#plt.legend(('NMPC','WS - no Dyn Mod','NMPC with ws','revised NMPC with ws'),loc = 'lower right',fontsize = 10)


if u_in_figure == 'yes':
    plt.subplot(3, 2, 6)
else:
    plt.subplot(2, 2, 4)
plt.plot(tsim, X3[1::6],'r-',linewidth=1.5)
plt.plot(tsim, X2[1::6],'co',linewidth=1.5)
#            plt.plot(tsim, X1[1::6],'b--',linewidth=1.5)
plt.plot(tsim, Xp[:,1],'b--',linewidth=1.5)
plt.xlabel('Time  [min]',fontsize=14)
plt.ylabel('Valve 2 position [%]',fontsize=14)
#            plt.axis([0.0, (Nsim-1)*h, 0, 100])
#plt.legend(('NMPC','WS - no Dyn Mod','NMPC with ws','revised NMPC with ws'),loc = 'lower right',fontsize = 10)
       
art = []
lgd = plt.legend((l1,l2,l3,l4), ('Set-Point','NMPC','WS - no Dyn Mod','NMPC with ws'), loc = 'lower center',  bbox_to_anchor = (0,-0.04,1,1), ncol=4,
            bbox_transform = plt.gcf().transFigure)
art.append(lgd)
plt.tight_layout() # gives spacing between figures
fig.subplots_adjust(wspace=0.12, hspace=0.15)
plt.savefig(pf + 'check_ws_and_fobj_mimo.png', format = 'png', transparent = True, additional_artists=art, bbox_inches = 'tight')

plt.show()




