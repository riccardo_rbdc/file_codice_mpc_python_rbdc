%% checking MPC Ts for 4tanks system
clc, clearvars
% discrete-time matrix
A = [0.9222         0    0.1958         0;
         0    0.9451         0    0.1479;
         0         0    0.7958         0;
         0         0         0    0.8477];
% sampling period
Ts = 5;
% continuos-time matrix
Ac = logm(A)*(1/Ts);
% eigenvalues
pd = eig(A);
pc = eig(Ac);
% time constants
Tau = -1./pc;
disp('Tau'), disp(Tau)