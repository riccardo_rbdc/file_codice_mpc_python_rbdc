# -*- coding: utf-8 -*-
"""

@author: RBdC + MV

Nonlinear MIMO Process P (Quadruple Tank)

+

Valve Stiction Model:
- He's STANDARD Model
- V1 is sticky
- V2 is healthy

STICTION AWARE Case: 
--> M_mpc === P_real --> MPC ha modello NL esatto della dinamica NON-lineare dei 4-tanks
--> V_mpc === V_real --> MPC conosce attrito (ovviamente è OFFSET-FREE)
"""

from casadi import *
from casadi.tools import *
from matplotlib import pylab as plt
import math
import scipy.linalg as scla
import numpy as np
from Utilities import*

# NOC discretization parameters

Nsim = 1000      # Simulation length (number of samples) (600)

N = 50          # Horizon

# Time interval value
h = 5.           # [s] if you are in discrete time just put h = 1.0

########## Symbolic variables #####################

xp = SX.sym("xp", 6)    # process state vector        # 6 --> 4 tanks + 2 valves output
x = SX.sym("x", 6)      # model state vector          # 6 --> 4 tanks + 2 valves output
#
u = SX.sym("u", 2)      # control vector              # 2 --> 2 valves input
y = SX.sym("y", 2)      # measured output vector      # 2 --> 2 lower tanks
k = SX.sym("k", 1)      # discrete time               # ???
d = SX.sym("d", 2)      # disturbance                 # 2 --> 2 lower tanks
###################################################


##### PARAMETERS
# stiction parameters (plant)
#fs_1 = 5.0
#fd_1 = 2.0

fs_2 = 0.0
fd_2 = 0.0

#
# stiction parameters (model)
fs_1_m = 6.0
fd_1_m = 2.0
fs_2_m = .0
fd_2_m = .0
#

Tau = 1e2
alfa = 2.1
#
WS_sp = 'none'                  # 'full', 'partial', 'simple', 'none'
WS_ss = 'none'                  # 'full', 'partial', 'simple', 'none'
Tuning_ss = 'with_S'            # with Rss or with Sss
Tuning_dyn = 'with_S'           # with R or with S
stiction_type = 'if_else'       # if_else/smoothed (type of stiction model): Inside process
stiction_type_mpc = 'if_else'   # if_else/smoothed (type of stiction model): INSIDE MPC
dyn_of_type = 'QP'              # LP/QP dynamic optimization objective function
Dis_mod = 'Input'               # Disturbance Model: Input/Output
TYPE_mpc = 'SA'

#  He's model (casadi if statements)
def Valve_din_He(u,x,fs,fd):
    if fs == 0:
        x_1 = u
    else:
        x_1 = if_else(u - x > fs, u - fd, \
        if_else(u - x < -fs, u + fd,\
        x))
    
    x_1 = if_else(x_1 < 0, 0.0, x_1)
    x_1 = if_else(x_1 > 100, 100.0, x_1)

    return x_1

# smoothed He's model      
def Valve_din_He_smoothed(u,x,fs,fd,Tau):
    e = u - x
    # hyperbolic functions
    eta1 = 0.5*tanh(Tau*(e + fs)) + 0.5*tanh(Tau*(-e + fs))    
    eta2 = 0.5*tanh(-Tau*(e + fs)) + 0.5*tanh(Tau*(-e + fs))  
    #
    x_1 = eta1*x + (1.0 - eta1)*u + eta2*fd
    
    return x_1 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
## PLANT (NON-LINEAR)

def Fdyn_p(x,u):
    ## Time CONTINUOUS dynamics of 4 tanks
    
    # TANKS parameters
    # hi = fx_p[0:4] water level
    # g acceleration of gravity [cm/s^2]
    g = 981.0
    # ai cross-section of the outlet hole [cm^2]
    a1 = 0.071
    a2 = 0.057
    a3 = 0.071
    a4 = 0.057
    # Ai cross-section of Tank [cm^2]
    A1 = 28.0
    A2 = 32.0
    A3 = 28.0
    A4 = 32.0
    # gmi flow splitting factor in (0,1)
    # Two operating points
    # P- minimum-phase characteristics      --> easy to control
        # gm1, gm2 > 0.5
    # P+ nonminimum-phase characteristics   --> more difficult to control
        # NM: may include delays, inverse response
    #
    gm1 = 0.7
    gm2 = 0.6
    # Ki prop. constant valve position MVi [x(0),x(1))] - qi flowrate (linear valve characteristics)
    # qi_max: max flowrate [cm^3/s]
    # hi_max: max tank level [cm]
    h1_max = 20.0
    h2_max = 20.0
    # q1_max = 2.0*a1*(2.0*g*h1_max)**0.5
    # q2_max = 2.0*a2*(2.0*g*h2_max)**0.5
    # portata massima: somma portate di scarico dai rispettivi serbatoi
    q1_max = (a1+a4)*(2.0*g*h1_max)**0.5
    q2_max = (a2+a3)*(2.0*g*h2_max)**0.5
    K1 = q1_max/100.0
    K2 = q2_max/100.0

    fx = SX(4,1)
    
    # TC system of equations:
    for i in range(x.shape[0]):
        x[i] = if_else(x[i]<0, 0., x[i])
        x[i] = if_else(x[i]>20, 20., x[i])

    ## tank #1 x[2]:fx_p[2] (left lower)
    fx[0] = -(a1/A1)*(2.0*g*x[0])**0.5 + (a3/A1)*(2.0*g*x[2])**0.5 + (gm1/A1)*K1*u[0]
    
    ## tank #2 x[3]:fx_p[3] (right lower)
    fx[1] = -(a2/A2)*(2.0*g*x[1])**0.5 + (a4/A2)*(2.0*g*x[3])**0.5 + (gm2/A2)*K2*u[1]
    
    ## tank #3 x[4]:fx_p[4] (left upper)
    fx[2] = -(a3/A3)*(2.0*g*x[2])**0.5 + ((1.0 - gm2)/A3)*K2*u[1]
    
    ## tank #4 x[5]:fx_p[5] (right upper)
    fx[3] = -(a4/A4)*(2.0*g*x[3])**0.5 + ((1.0 - gm1)/A4)*K1*u[0]
    
    return fx

# To set if the plant is non-linear
def User_fxp_Dis(x,t,u, fs_1 = 6., fd_1 = 2.,fs_1_m = 6., fd_1_m = 2.):
    ## Corresponding time DISCRETE dynamics of 4 tanks (integration by hand)
    """
    SUMMARY:
    It constructs the function User_fxp_Dis for the non-linear case
    
    SYNTAX:
    assignment = User_fxp_Dis(t)
  
    ARGUMENTS:
    + t             - Variable that indicate the current iteration
    
    OUTPUTS:
    + fx_p          - Non-linear plant function     
    """ 
              
    # initialize variables
    fx_p = SX(x.size1(),1)
    x_k_1 = SX(2,1)
    
    # process parameters
    #### He's semi-physical model + process (state equation)
    
    # VALVES        
    
    ### Valve 1 --> stiction
    if stiction_type == 'if_else':
    #### He's semi-physical model (casadi if statements)
        x_k_1[0] = Valve_din_He(u[0],x[0],fs_1,fd_1)        
    
    else:    
    #### smoothed He's semi-physical model
        x_k_1[0] = Valve_din_He_smoothed(u[0],x[0],fs_1,fd_1,Tau)
    
    ### Valve 2 --> healthy (NO stiction)
    if stiction_type == 'if_else':
    #### He's semi-physical model (casadi if statements)
        x_k_1[1] = Valve_din_He(u[1],x[1],fs_2,fd_2)        
    
    else:    
        #### smoothed He's semi-physical model
        x_k_1[1] = Valve_din_He_smoothed(u[1],x[1],fs_2,fd_2,Tau)
  
    
    # Explicit Runge-Kutta 4 (TC dynamics integrateed by hand)
    Mx = 5                   # Number of elements in each time step
    dt = h/Mx
    x0 = x[2:6]
    fx_p[0:2] = x_k_1   
    for i in range(Mx):         
        k1 = Fdyn_p(x0, x_k_1)
        k2 = Fdyn_p(x0 + dt/2.0*k1, x_k_1)
        k3 = Fdyn_p(x0 + dt/2.0*k2, x_k_1)
        k4 = Fdyn_p(x0 + dt*k3, x_k_1)
        x0 = x0 + (dt/6.0)*(k1 + 2.0*k2 + 2.0*k3 + k4)
    fx_p[2:6] = x0
    
    return fx_p

def User_fyp(x,t,u):
    """
    SUMMARY:
    It constructs the function User_fyp for the non-linear case
    
    SYNTAX:
    assignment = deffyp(t)
  
    ARGUMENTS:
    + t             - Variable that indicate the current iteration
    
    OUTPUTS:
    + fy_p          - Non-linear plant function     
    """
    # (output equation) 
    fy_p = vertcat\
    (\
    x[2],\
    x[3] \
    )
    
    return fy_p


## NOISE
#G_wn = 1e-3*np.array([[1.0, 1.0, 1.0], [1.0, 1.0, 1.0], [1.0, 1.0, 1.0]])  # State white noise matrix
#Q_wn = 1e-3*np.array([[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]])  # State white noise covariance matrix
#R_wn = 1e-3*np.array([[1.0, 0.0], [0.0, 1.0]])                             # Output white noise covariance matrix
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #



# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
################  MODEL

## PLANT Model (NON-LINEAR) - Pm is NL & MPC is aware of stiction

def Fdyn_m(x,u):
    ## Time CONTINUOUS dynamics of 4 tanks
    
    # TANKS parameters
    # hi = fx_p[2:5] water level
    # g acceleration of gravity [cm/s^2]
    g = 981.0
    # ai cross-section of the outlet hole [cm^2]
    a1 = 0.071
    a2 = 0.057
    a3 = 0.071
    a4 = 0.057
    # Ai cross-section of Tank [cm^2]
    A1 = 28.0
    A2 = 32.0
    A3 = 28.0
    A4 = 32.0
    # gmi flow splitting factor in (0,1)
    # Two operating points
    # P- minimum-phase characteristics      --> easy to control
        # gm1, gm2 > 0.5
    # P+ nonminimum-phase characteristics   --> more difficult to control
        # NM: delays, inverse response
    #
    gm1 = 0.7
    gm2 = 0.6
    # Ki prop. constant valve position MVi [x(0),x(1))] - qi flowrate (linear valve characteristics)
    # qi_max: max flowrate [cm^3/s]
    # hi_max: max tank level [cm]
    h1_max = 20.0
    h2_max = 20.0
    # q1_max = 2.0*a1*(2.0*g*h1_max)**0.5
    # q2_max = 2.0*a2*(2.0*g*h2_max)**0.5
    # portata massima: somma portate di scarico dai rispettivi serbatoi
    q1_max = (a1+a4)*(2.0*g*h1_max)**0.5
    q2_max = (a2+a3)*(2.0*g*h2_max)**0.5
    K1 = q1_max/100.0
    K2 = q2_max/100.0

    fx = SX(4,1)
    
    for i in range(x.shape[0]):
        x[i] = if_else(x[i]<0, 0., x[i])
        x[i] = if_else(x[i]>20, 20., x[i])

    ## tank #1 x[2]:fx_p[2] (left lower)
    fx[0] = -(a1/A1)*(2.0*g*x[0])**0.5 + (a3/A1)*(2.0*g*x[2])**0.5 + (gm1/A1)*K1*u[0]
    
    ## tank #2 x[3]:fx_p[3] (right lower)
    fx[1] = -(a2/A2)*(2.0*g*x[1])**0.5 + (a4/A2)*(2.0*g*x[3])**0.5 + (gm2/A2)*K2*u[1]
    
    ## tank #3 x[4]:fx_p[4] (left upper)
    fx[2] = -(a3/A3)*(2.0*g*x[2])**0.5 + ((1.0 - gm2)/A3)*K2*u[1]
    
    ## tank #4 x[5]:fx_p[5] (right upper)
    fx[3] = -(a4/A4)*(2.0*g*x[3])**0.5 + ((1.0 - gm1)/A4)*K1*u[0]
    
    return fx
    
    
def User_fxm_Dis(x,u,d,t, fs_1_m = 6., fd_1_m = 2.):
    ## Corresponding time DISCRETE dynamics of 4 tanks (integration by hand)
    """
    SUMMARY:
    It constructs the function User_fxm_Dis for the non-linear case
    
    SYNTAX:
    assignment = User_fxm_Dis(t)
  
    ARGUMENTS:
    + t                 - Variable that indicate the current iteration
    
    OUTPUTS:
    + fx_model          - Non-linear MODEL plant function     
    """ 
    
     # initialize variables
    fx_model = SX(x.size1(),1)
    x_k_1 = SX(2,1)
    
    # process parameters
    #### He's semi-physical model + process (state equation)
    
    # VALVES
    
    ### Valve 1 --> stiction
    if stiction_type_mpc == 'if_else':
    #### He's semi-physical model (casadi if statements)
        x_k_1[0] = Valve_din_He(u[0],x[0],fs_1_m,fd_1_m)          
    else:    
    #### smoothed He's semi-physical model
        x_k_1[0] = Valve_din_He_smoothed(u[0],x[0],fs_1_m,fd_1_m,Tau)
    
    ### Valve 2 --> healthy (NO stiction)
    if stiction_type_mpc == 'if_else':
    #### He's semi-physical model (casadi if statements)
        x_k_1[1] = Valve_din_He(u[1],x[1],fs_2_m,fd_2_m)        
    else:    
    #### smoothed He's semi-physical model
        x_k_1[1] = Valve_din_He_smoothed(u[1],x[1],fs_2_m,fd_2_m,Tau)
   
    # Explicit Runge-Kutta 4 (TC dynamics integrateed by hand)       
    Mx = 5                   # Number of elements in each time step
    dt = h/Mx
    x0 = x[2:6]
    tilde_x = x_k_1 
    if Dis_mod == 'Input':
        tilde_x = x_k_1 + d
    fx_model[0:2] = tilde_x   
    for i in range(Mx):         
        k1 = Fdyn_m(x0, tilde_x)
        k2 = Fdyn_m(x0 + dt/2.0*k1, tilde_x)
        k3 = Fdyn_m(x0 + dt/2.0*k2, tilde_x)
        k4 = Fdyn_m(x0 + dt*k3, tilde_x)
        x0 = x0 + (dt/6.0)*(k1 + 2.0*k2 + 2.0*k3 + k4)
    fx_model[2:6] = x0
                               
    return fx_model
            
# model (output equation)
def User_fym(x,u,d,t):
    
    """
    SUMMARY:
    It constructs the function fy_m
    
    SYNTAX:
    assignment = fy_m(t)
  
    ARGUMENTS:
    + t             - Variable that indicate the current iteration
    
    OUTPUTS:
    + fy_model          - Non-linear MODEL plant function
     
    """ 
   
    # (output equation)  
    # --> PV (h1, h2)    
    fy_model = vertcat\
                (\
                x[2],\
                x[3]\
                )
        
    return fy_model

## Disturbance model
offree = "nl"  # set "lin" or "nl" to have a disturbance model linear or non linear. 
                # "no" means no disturbance model will be implemented

if Dis_mod == 'Input':
    B = np.array([[ 0.03044478,  0.00186745],
       [ 0.0009123 ,  0.0231119 ],
       [ 0.        ,  0.01619199],
       [ 0.01095566,  0.        ]])
    Bd = 1.0*vertcat(DM.eye(2), B)
    Cd = 0.0*DM.eye(d.size1())
else:
    Bd = 0.0*DM([[1.0, 1.0], [1.0, 1.0], [1.0, 1.0], [1.0, 1.0], [1.0, 1.0], [1.0, 1.0]])
    Cd = 1.0*DM.eye(d.size1())

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# Initial conditions
x0_p = 1.0*vertcat(39.5794, 38.1492, 11.9996, 12.1883, 1.51364, 1.42194)      # [plant]  
x0_m = 1.0*vertcat(39.5794, 38.1492, 11.9996, 12.1883, 1.51364, 1.42194)      # [model]   
u0 = 1.0*vertcat(39.5794, 38.1492)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
## BOUNDS
## Input bounds
umin = 0.0*vertcat(1.0, 1.0)
umax = 100.0*vertcat(1.0, 1.0)

## State bounds
xmin = vertcat(0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
xmax = 1.0*vertcat(100.0, 100.0, 20.0, 20.0, 20.0, 20.0)

## Output bounds
ymin = 0.0*vertcat(1.0, 1.0)
ymax = 20.0*vertcat(1.0, 1.0)

## Disturbance bounds
Dumin = vertcat(-alfa*fs_1_m, -DM.inf(u.size1()-1))
Dumax = vertcat(alfa*fs_1_m, DM.inf(u.size1()-1))

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
## Setpoints

def defSP(t):
    """
    SUMMARY:
    It constructs the setpoints vectors for the steady-state optimisation 
    
    SYNTAX:
    assignment = defSP(t)
  
    ARGUMENTS:
    + t             - Variable that indicates the current time [min]
    
    OUTPUTS:
    + ysp, usp      - Input and output setpoint values      
    """ 
    if t<= 50:
        ysp = np.array([[11.9996, 12.1883]])                            # Output setpoint
        usp = np.array([[39.5185, 38.1743]])                            # Input setpoints
        xsp = np.array([[50.0, 50.0, 10.0, 10.0, 2.0, 2.0]])            # State setpoints
    elif t>50 and t<=1000:
        ysp = np.array([[11.9996, 6.0]])                               # Output setpoint
        usp = np.array([[39.5185, 38.1743]])                                  # Control setpoints
        xsp = np.array([[60.0, 50.0, 12.0, 8.0, 2.0, 2.0]])             # State setpoints                
    elif t>1000 and t<=2000:
        ysp = np.array([[6.0, 6.0]])                                  # Output setpoint
        usp = np.array([[39.5185, 38.1743]])                                  # Control setpoints
        xsp = np.array([[60.0, 40.0, 12.0, 8.0, 2.0, 2.0]])             # State setpoints
    elif t>2000 and t<=3000:
        #ysp = np.array([[11.9996, 12.1883]])                            # Output setpoint
        ysp = np.array([[12.0, 12.0]])                            # Output setpoint
        usp = np.array([[39.5185, 38.1743]])                                  # Control setpoints
        xsp = np.array([[40.0, 40.0, 8.0, 8.0, 2.0, 2.0]])              # State setpoints
    elif t>3000 and t<=4000:
        ysp = np.array([[8.0, 12.0]])                                   # Output setpoint
        usp = np.array([[39.5185, 38.1743]])                                  # Control setpoints
        xsp = np.array([[40.0, 60.0, 8.0, 12.0, 2.0, 2.0]])             # State setpoints
    elif t>4000 and t<=5000:
        ysp = np.array([[10.0, 10.0]])                                  # Output setpoint
        usp = np.array([[39.5185, 38.1743]])                                  # Control setpoints
        xsp = np.array([[50.0, 50.0, 10.0, 10.0, 2.0, 2.0]])            # State setpoints
    else:
        ysp = np.array([[8.0, 12.0]])                                   # Output setpoint
        usp = np.array([[39.5185, 38.1743]])                                  # Control setpoints
        xsp = np.array([[40.0, 40.0, 8.0, 12.0, 2.0, 2.0]])             #  State setpoints    

#    if t<= 50:
#        ysp = np.array([[11.9996, 12.1883]])                            # Output setpoint
#        usp = np.array([[39.5185, 38.1743]])                            # Input setpoints
#        xsp = np.array([[50.0, 50.0, 10.0, 10.0, 2.0, 2.0]])            # State setpoints
#    elif t>50 and t<=1000:
#        ysp = np.array([[11.9996, 7.0]])                               # Output setpoint
#        usp = np.array([[39.5185, 38.1743]])                                  # Control setpoints
#        xsp = np.array([[60.0, 50.0, 12.0, 8.0, 2.0, 2.0]])             # State setpoints                
#    elif t>1000 and t<=2000:
#        ysp = np.array([[7.0, 7.0]])                                  # Output setpoint
#        usp = np.array([[39.5185, 38.1743]])                                  # Control setpoints
#        xsp = np.array([[60.0, 40.0, 12.0, 8.0, 2.0, 2.0]])             # State setpoints
#    elif t>2000 and t<=3000:
#        ysp = np.array([[7, 10.0]])                            # Output setpoint
#        usp = np.array([[39.5185, 38.1743]])                                  # Control setpoints
#        xsp = np.array([[40.0, 40.0, 8.0, 8.0, 2.0, 2.0]])              # State setpoints
#    elif t>3000 and t<=4000:
#        ysp = np.array([[13.0, 10.0]])                                   # Output setpoint
#        usp = np.array([[39.5185, 38.1743]])                                  # Control setpoints
#        xsp = np.array([[40.0, 60.0, 8.0, 12.0, 2.0, 2.0]])             # State setpoints
#    elif t>4000 and t<=5000:
#        ysp = np.array([[11.5, 11.5]])                                  # Output setpoint
#        usp = np.array([[39.5185, 38.1743]])                                  # Control setpoints
#        xsp = np.array([[50.0, 50.0, 10.0, 10.0, 2.0, 2.0]])            # State setpoints
#    else:
#        ysp = np.array([[11.5, 11.5]])                                   # Output setpoint
#        usp = np.array([[39.5185, 38.1743]])                                  # Control setpoints
#        xsp = np.array([[40.0, 40.0, 8.0, 12.0, 2.0, 2.0]])             #  State setpoints                  
    
    return [ysp, usp, xsp]
    
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
## OBJECTIVE FUNCTION

######## Steady-state optimization 

    # To set if the optimisation problem is QP (Default)                             
Qss = 1e0*DM([[1.0, 0.0], [0.0, 1.0]])                                                       # Output matrix
if Tuning_ss == 'with_R':
    Rss = 0e0*DM([[1.0, 0.0], [0.0, 1.0]])                                                   # Control matrix
else:
    Sss = 0e0*DM([[1.0, 0.0], [0.0, 1.0]])                                                   # Delta Control matrix



######## Dynamic optimization 
if dyn_of_type == 'QP':
# To set if the optimisation problem is QP (Default)
#    Q = 1e0*DM([[1000.0, 10.0, 0.0, 0.0, 0.0, 0.0], [10.0, 10.0, 0.0, 0.0, 0.0, 0.0], \
#                [0.0, 0.0, 10.0, 0.0, 0.0, 0.0], [0.0, 0.0, 0.0, 10.0, 0.0, 0.0], \
#                [0.0, 0.0, 0.0, 0.0, 1.0, 0.0], [0.0, 0.0, 0.0, 0.0, 0.0, 1.0]]) #very good for NMPC
#    Q = 1e0*DM([[100.0, 0.0, 0.0, 0.0, 0.0, 0.0], [0.0, 100.0, 0.0, 0.0, 0.0, 0.0], \
#                [0.0, 0.0, 10.0, 0.0, 0.0, 0.0], [0.0, 0.0, 0.0, 10.0, 0.0, 0.0], \
#                [0.0, 0.0, 0.0, 0.0, 1.0, 0.0], [0.0, 0.0, 0.0, 0.0, 0.0, 1.0]]) # NMPC basically equal to skip the ocp with WS
#    Q = 1e0*DM([[1000.0, 0.0, 0.0, 0.0, 0.0, 0.0], [0.0, 1000.0, 0.0, 0.0, 0.0, 0.0], \
#                [0.0, 0.0, 1000.0, 0.0, 0.0, 0.0], [0.0, 0.0, 0.0, 1000.0, 0.0, 0.0], \
#                [0.0, 0.0, 0.0, 0.0, 1.0, 0.0], [0.0, 0.0, 0.0, 0.0, 0.0, 1.0]])      # best so far
    Q = 1e0*DM([[1e3, 0.0, 0.0, 0.0, 0.0, 0.0], [0.0, 1e3, 0.0, 0.0, 0.0, 0.0], \
                [0.0, 0.0, 1.0, 0.0, 0.0, 0.0], [0.0, 0.0, 0.0, 1.0, 0.0, 0.0], \
                [0.0, 0.0, 0.0, 0.0, 1e-6, 0.0], [0.0, 0.0, 0.0, 0.0, 0.0, 1e-6]])            # with Kx != 0
#    Q = 1e0*DM([[1000.0, 0.0, 0.0, 0.0, 0.0, 0.0], [0.0, 1000.0, 0.0, 0.0, 0.0, 0.0], \
#                [0.0, 0.0, 10.0, 0.0, 0.0, 0.0], [0.0, 0.0, 0.0, 10.0, 0.0, 0.0], \
#                [0.0, 0.0, 0.0, 0.0, .1, 0.0], [0.0, 0.0, 0.0, 0.0, 0.0, .1]])            # final decision
                                                                                            # State matrix
    # Q = 1./(0.0204**2)*mtimes(Ct,C) + 0.01*DM.eye(x.size1())                              # State matrix       
    if Tuning_dyn == 'with_R':
        R = 1e0*DM([[1.0, 0.0], [0.0, 1.0]])           
    else:                                                                             # U matrix
#        S = 1e1*DM([[1., 0.0], [0.0, 10.0]])                                          # DeltaU matrix
        S = 0.0*DM([[1.0, 0.0], [0.0, 1.0]])                                          # DeltaU matrix
# note: S and R cannot be set both at the same time

# matrix for weighting valve movements
    Qs = 1e1*DM([[1.,0.],[0.,10.]])
#    Qs = 1e-6*DM([[1.,0.],[0.,1.]]) 
else:
    # to be changed
# To set if the optimisation problem is LP
    r_x = 1e8*np.array(C + 0.01)
    #r_x = 1e0*np.array(diag(q).T)
    # r_x = 1e0*np.array([[1.0, 1.0, 1.0, 1.0]])           # State vector
    if Tuning_dyn == 'with_R':
        r_u = 1e0*np.array([[1.0]])                     # Control vector
    else:
        r_Du = 1e0*np.array([[1.0]])                    # Control vector for DeltaU


    # Specify the terminal cost if the problem is not QP and 0 value is not wanted
Vn = 100.0
vfin = mtimes(x.T,mtimes(Vn,x)) 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
## Estimator

#############################################################################
if Dis_mod == 'Output':
    #### Luemberger observer tuning params ######################################
    lue = True # Set True if you want the Luemberger observer
    nx = x.size1()
    ny = y.size1()
    nd = d.size1()
    # Kx = DM([[1.0, 0.0], [0.0, 1.0],[1.0, 0.0], [0.0, 1.0],[0.0, 0.0], [0.0, 0.0]]) # zeros(nx,ny)
    Kx = DM.zeros(nx,ny)
    Kd = DM.eye(nd)
    K = vertcat(Kx,Kd)
else:
#############################################################################
    ### Extended Kalman filter tuning params ###################################
    ekf = True # Set True if you want the Kalman filter
    nx = x.size1()
    ny = y.size1()
    nd = d.size1()
    Qx_kf = 1.0e-6*DM.eye(nx)
    Qd_kf = 1.0*DM.eye(nd)
    Q_kf = DM(scla.block_diag(Qx_kf, Qd_kf))
    if 'R_wn' in locals():
        R_kf = R_wn
    else:
        R_kf = 1.0e-5*DM.eye(ny)
#    Q_kf = 1.0e-16*DM.eye(nx+d.size1()) # (it will be nx+nd when d will be include)
#    R_kf = 1.0e-15*DM.eye(ny)
    P0 = Q_kf#1e-2*DM.ones(nx+d.size1(),nx+d.size1())  # (it will be nx+nd,nx+nd when d will be include)
