# -*- coding: utf-8 -*-
"""
Created on Tue Jan 12 12:11:54 2016 

@author: RBdC

Simple linear Process P
Third order

+

Valve Stiction Model:
- He's semi-physical Model

Nominal Case: 
--> Mmpc == P
--> Vmpc == V --> MPC CONOSCE attrito, è OFFSET-FREE, ha un DOPPIO modello di attrito (SEMIFISICO)
eventualmente:
1. SENZA IF per fare ottimizzazione
2. CON IF per fare predizione
"""

from casadi import *
from casadi.tools import *
from matplotlib import pylab as plt
import math
import scipy.linalg as scla
import numpy as np
from Utilities import*
from Target_Calc import *
from Estimator import *
from Control_Calc import *
from Default_Values import *

# NOC discretization parameters

Nsim = 210      # Simulation length (number of samples)

N = 50          # Horizon

# Time interval value
h = 1.0         # [min] if you are in discrete time just put h = 1.0

########## Symbolic variables #####################

# in the case of He's stiction model
xp = SX.sym("xp", 4)    # process state vector        #
x = SX.sym("x", 4)      # model state vector          #
#
u = SX.sym("u", 1)      # control vector              #
y = SX.sym("y", 1)      # measured output vector      #
k = SX.sym("k", 1)      # discrete time               #
d = SX.sym("d", 1)      # disturbance                 #
###################################################


##### PARAMETERS
## stiction parameters (plant)
fs = 5.0
fd = 2.0
## stiction parameters (model)
fs_m = 5.0
fd_m = 2.0
#
M = 1.99
Tau = 1e4
alfa = 2.1
#
#MPC_type                               # aware/unaware
#WS_type                                # no/ yes: Set MPC output sequence as the warm-start
#n_ws                                   # exclude first n_ws steps from dynamic objective function
WS_sp = 'none'                          # 'full', 'partial', 'simple', 'none'
WS_ss = 'none'                          # 'full', 'partial', 'simple', 'none'
Tuning = 'with_S'                       # with_R/ with_S
stiction_type = 'if_else'               # if_else/smoothed (type of stiction model): inside process & model
stiction_type_dyn = 'if_else'           # if_else/smoothed (type of stiction model): inside DYN OPT
dyn_of_type = 'QP'                      # LP/QP dynamic opt objective function
Dis_mod = 'Input'                      # Disturbance Model: Input/Output

# # # # Valve dynamics

#  He's model (casadi if statements)
def Valve_din_He_sp(u,x,fs,fd,M):
    if fs == 0:
        x_1 = u
    else:
        x_1 = if_else(u - x > fs, M*(u - fd) + x*(1.0 - M), \
        if_else(u - x < -fs, M*(u + fd) + x*(1.0 - M),\
            x))
    return x_1

# smoothed semiphysical He's model          
def Valve_din_He_smoothed_sp(u,x,fs,fd,Tau,M):
    e = u - x        
    # sigmoidal functions
#       eta1 = M/(1.0 + exp(-(e + fs)/tau)) + M/(1.0 + exp(-(e + fs)/tau)) - 2.0*M 
#       eta2 = M/(1.0 + exp((e - fs)/tau)) - M/(1.0 + exp(-(e + fs)/tau))  
    # hyperbolic function
    eta1 = M/2.0*tanh(Tau*(e + fs)) + M/2.0*tanh(Tau*(-e + fs)) - M    
    eta2 = M/2.0*tanh(-Tau*(e + fs)) + M/2.0*tanh(Tau*(-e + fs))  
        #
    x_1 = (1.0 + eta1)*x - eta1*u + eta2*fd       
    
    return x_1    

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
## PLANT (NON-LINEAR)

# To set if the plant is non-linear
def User_fxp_Dis(x,t,u):
    
    """
    SUMMARY:
    It constructs the function User_fxp_Dis for the non-linear case
    
    SYNTAX:
    assignment = User_fxp_Dis(t)
  
    ARGUMENTS:
    + t             - Variable that indicate the current iteration
    
    OUTPUTS:
    + fx_p          - Non-linear plant function     
    """ 
          
    # initialize variables
    fx_p = SX(x.size1(),1)
    
    # process parameters
    # Continuous
    # ...
    # Discrete
    Ap = np.array([[2.091447612285384, -0.687441758146000, 0.272531793034013], [2.0, 0.0, 0.0], [0.0, 0.5, 0.0]])   
    Bp = np.array([[0.0625], [0.0], [0.0]])
    
    #### He's model + process (state equation)
        
    if stiction_type == 'if_else':
    #### He's semi-physical model (casadi if statements)
        fx_p[0] = Valve_din_He_sp(u[0],x[0],fs,fd,M)
    
    else:    
    #### smoothed He's semi-physical model
        fx_p[0] = Valve_din_He_smoothed_sp(u[0],x[0],fs,fd,Tau,M)
    #
                            
    fx_p[1:4] = mtimes(Ap,x[1:4]) + Bp*fx_p[0]    
    
    return fx_p

def User_fyp(x,t,u):
    """
    SUMMARY:
    It constructs the function User_fyp for the non-linear case
    
    SYNTAX:
    assignment = deffyp(t)
  
    ARGUMENTS:
    + t             - Variable that indicate the current iteration
    
    OUTPUTS:
    + fy_p          - Non-linear plant function     
    """
    
    # process parameters
    # Continous
    # ...
    # Discrete
    Cp = np.array([[0.039116001982656, 0.057450029451752, 0.020449714675490]])
    
    
    # --> PV process (output equation)
    fy_p = mtimes(Cp,x[1:4]) 
    
    return fy_p

############# Additive State Disturbances ###############################
# dkx_p = DM.zeros(x.size1()) # 
# Comment this line if don't want an additive state disturbance.

def defdxp(k):
    """
    SUMMARY:
    It constructs the additive disturbances for the linear case
    
    SYNTAX:
    assignment = defdp(k)
  
    ARGUMENTS:
    + k             - Variable that indicate the current iteration
    
    OUTPUTS:
    + dxp           - State disturbance value      
    """ 
    
#    dxp = np.array([0.1, 0.0, 0.0]) # State disturbance

    if k <= 20:
        dxp = np.array([[0.1, 0.0, 0.0]]) # State disturbance
    else:
        dxp = np.array([[0.0, 0.0, 0.0]]) # State disturbance

    return [dxp]
##########################################################################

############# Additive Output Disturbances ###############################
# dky_p = DM.zeros(y.size1())
# Comment this line if don't want an additive output disturbance.

def defdyp(k):
    """
    SUMMARY:
    It constructs the additive disturbances for the linear case
    
    SYNTAX:
    assignment = defdp(k)
  
    ARGUMENTS:
    + k             - Variable that indicate the current iteration
    
    OUTPUTS:
    + dyp      - Output disturbance value      
    """ 
    
    dyp = np.array([0.1]) # Output disturbance
    
#    if k <= 20:
#        dyp = np.array([[0.5, 350.1, 0.659]]) # State disturbance
#    else:
#        dyp = np.array([[0.4, 355.1, 0.659]]) # State disturbance
    return [dyp]
##########################################################################

## NOISE
#G_wn = 1e-3*np.array([[1.0, 1.0, 1.0], [1.0, 1.0, 1.0], [1.0, 1.0, 1.0]])  # State white noise matrix
#Q_wn = 1e-3*np.array([[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]])  # State white noise covariance matrix
R_wn = 0e0*np.array([[1.0]])                                              # Output white noise covariance matrix
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #



# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
################  MODEL

## Disturbance model
offree = "lin"  # set "lin" or "nl" to have a disturbance model linear or non linear. 
                # "no" means no disturbance model will be implmented
#Bd = DM.eye(d.size1())
if Dis_mod == 'Input':
    B = np.array([[0.0625], [0.0], [0.0]])
    Bd = 1.0*vertcat(DM.eye(1), B)
    Cd = 0*DM.eye(d.size1())
else:
    Bd = 0.0*DM([[1.0], [1.0], [1.0], [1.0]])
    Cd = 1*DM.eye(d.size1())
#Cd = DM.zeros(y.size1(),d.size1())



##  Model (NON-LINEAR)

# First Model (for OCP)
def User_fxm_Dis(x,u,d,k):
    """
    SUMMARY:
    It constructs the function User_fxm_Dis for the non-linear case
    
    SYNTAX:
    assignment = User_fxm_Dis(t)
  
    ARGUMENTS:
    + t                 - Variable that indicate the current iteration
    
    OUTPUTS:
    + fx_model          - Non-linear MODEL plant function     
    """ 
    
    # initialize
    #fx_model = np.zeros(x.shape)
    fx_model = SX(x.size1(),1)
    
    # process parameters
    # Continuous
    # ...
    # Discrete
    A = np.array([[2.091447612285384, -0.687441758146000, 0.272531793034013], [2.0, 0.0, 0.0], [0.0, 0.5, 0.0]])   
    B = np.array([[0.0625], [0.0], [0.0]])
     
    #### He's semi-physical model + process (state equation)        
    if stiction_type_dyn == 'if_else':
    #### He's semi-physical model (casadi if statements)
        fx_model[0] = Valve_din_He_sp(u[0],x[0],fs_m,fd_m,M)        
    
    else:    
    #### smoothed He's semi-physical model
        fx_model[0] = Valve_din_He_smoothed_sp(u[0],x[0],fs_m,fd_m,Tau,M)    
    #
        
    fx_model[1:4] = mtimes(A,x[1:4]) + B*fx_model[0]

    return fx_model
    
# Second Model (for state update)
def User_fxm_Dis_2(x,u,d,k):
    """
    SUMMARY:
    It constructs the function User_fxm_Dis_2 for the non-linear case
    
    SYNTAX:
    assignment = User_fxm_Dis(t)
  
    ARGUMENTS:
    + t                 - Variable that indicate the current iteration
    
    OUTPUTS:
    + fx_model          - Non-linear MODEL plant function     
    """ 
    
    # initialize
#    fx_model = SX(x.size1(),1)
    fx_model = np.zeros(x.shape)
    
    # process parameters
    # Continuous
    # ...
    # Discrete
    A = np.array([[2.091447612285384, -0.687441758146000, 0.272531793034013], [2.0, 0.0, 0.0], [0.0, 0.5, 0.0]])  
    B = np.array([[0.0625], [0.0], [0.0]])
             
    #### He's semi-physical model + process (state equation)        
    if stiction_type == 'if_else':
    #### He's semi-physical model (casadi if statements)
        fx_model[0] = Valve_din_He_sp(u[0],x[0],fs_m,fd_m,M)        
    
    else:    
    #### smoothed He's semi-physical model
        fx_model[0] = Valve_din_He_smoothed_sp(u[0],x[0],fs_m,fd_m,Tau,M)    
    #     
        
    #
    fx_model[1:4] = mtimes(A,x[1:4]) + B*fx_model[0]

    return fx_model    

# process (output equation)
def User_fym(x,u,d,k):
    
    """
    SUMMARY:
    It constructs the function fy_m
    
    SYNTAX:
    assignment = fy_m(t)
  
    ARGUMENTS:
    + t             - Variable that indicate the current iteration
    
    OUTPUTS:
    + fy_model          - Non-linear MODEL plant function
     
    """ 
        
    # process parameters
    # Continuous
    # ...
    # Discrete
    C = np.array([[0.039116001982656, 0.057450029451752, 0.020449714675490]])
 
    fy_model = mtimes(C,x[1:4])
    
    return fy_model


Mx = 10 # Number of elements in each time step 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# Initial conditions
x0_p = 0.0*vertcat(0.0, 1.0, 1.0, 1.0)      # [plant]
x0_m = 0.0*vertcat(0.0, 1.0, 1.0, 1.0)      # [model]   
u0 = 0.0*vertcat(1.0)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
## BOUNDS
## Input bounds
umin = -50.0*vertcat(1.0)
umax = 50.0*vertcat(1.0)

## Delta Input bounds
Dumin = - fs*alfa*vertcat(1.0)
Dumax =  fs*alfa*vertcat(1.0)

## State bounds
xmin = -1000.0*vertcat(0.05, 1.0, 1.0, 1.0)
xmax = 1000.0*vertcat(0.05, 1.0, 1.0, 1.0)

## Output bounds
ymin = -50.0*vertcat(1.0)
ymax = 50.0*vertcat(1.0)

## Disturbance bounds
#dmin = None
#dmax = None

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
## SOFT CONSTRAINT ON X AND Y BOUNDS
## Slack variables weights (SS)
w_s = None ## Output slack variable weight
z_s = None ## State slack variable weight

## Slack variables weights (OCP)
W = None ## Output slack variable weight
Z = None ## State slack variable weight

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
## Setpoints

def defSP(t):
    """
    SUMMARY:
    It constructs the setpoints vectors for the steady-state optimisation 
    
    SYNTAX:
    assignment = defSP(t)
  
    ARGUMENTS:
    + t             - Variable that indicates the current time [min]
    
    OUTPUTS:
    + ysp, usp      - Input and output setpoint values      
    """
    if t<= 10:
        ysp = np.array([[0.0]])                     # Output setpoint
        usp = np.array([[0.0]])                     # Input setpoints
        xsp = np.array([[0.0, 0.0, 0.0, 0.0]])      # State setpoints                
    elif t>10 and t<=110:
        ysp = np.array([[7.0]])                    # Output setpoint
        usp = np.array([[0.0]])               # Control setpoints
        xsp = np.array([[0.0, 0.0, 0.0, 0.0]])      # State setpoints  
    elif t>110 and t<=550:
        ysp = np.array([[-7.0]])                   # Output setpoint
        usp = np.array([[0.0]])                   # Control setpoints
        xsp = np.array([[0.0, 0.0, 0.0, 0.0]])      # State setpoints
    elif t>550 and t<=750:
        ysp = np.array([[0.0]])                   # Output setpoint
        usp = np.array([[0.0]])                   # Control setpoints
        xsp = np.array([[0.0, 0.0, 0.0, 0.0]])      # State setpoints 
    elif t>750 and t<=1050:
        ysp = np.array([[3.0]])                   # Output setpoint
        usp = np.array([[0.0]])                   # Control setpoints
        xsp = np.array([[0.0, 0.0, 0.0, 0.0]])      # State setpoints 
    else:
        ysp = np.array([[0.0]])                     # Output setpoint
        usp = np.array([[0.0]])                     # Control setpoints
        xsp = np.array([[0.0, 0.0, 0.0, 0.0]])      # State setpoints                     
    
    return [ysp, usp, xsp]
    
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
## OBJECTIVE FUNCTION

######## Steady-state optimization 
                          # Control matrix
Qss = DM([[1.0]])
Rss = DM([[0.0]])


######## Dynamic optimization 
C = DM([[0.039116001982656, 0.057450029451752, 0.020449714675490]])
Caug = DM([[0.0, 0.039116001982656, 0.057450029451752, 0.020449714675490]])
#
q = 1.0/(np.amax(C)**2)*mtimes(Caug.T,Caug) + 1.0e-6*DM.eye(x.size1())
#
if dyn_of_type == 'QP':
# To set if the optimisation problem is QP (Default)
    Q = q                       # State matrix 
    Q[0,0] = 10.
    if Tuning == 'with_R': 
        R = 1e2*DM([[1]])                                # U matrix
    else:
        S = 1e2*DM([[1]])                               # DeltaU matrix
#        S = 0.0*DM([[1]])                               # DeltaU matrix
    # note: S and R cannot be set both at the same time
else:
# To set if the optimisation problem is LP
    r_x = 1e8*np.array(Caug + 0.01)
    #r_x = 1e0*np.array(diag(q).T)
    # r_x = 1e0*np.array([[1.0, 1.0, 1.0, 1.0]])           # State vector
    if Tuning == 'with_R':
        r_u = 1e0*np.array([[1.0]])                     # Control vector
    else:
        r_Du = 1e0*np.array([[1.0]])                    # Control vector for DeltaU

    # Specify the terminal cost if the problem is not QP and 0 value is not wanted
Vn = 1e2
vfin = mtimes(x.T,mtimes(Vn,x)) 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
## Estimator


#############################################################################
#### Luenberger observer tuning params ######################################
lue = False # Set True if you want the Luemberger observer
nx = x.size1()
ny = y.size1()
nd = d.size1()
Kx = DM.zeros(nx,ny)
Kd = DM.eye(ny)
K = vertcat(Kx,Kd)
#K = DM([[0.861347, 0.16315],[-58.941, 69.2334],[0.16315, 0.807968],[-37.2345, 43.8201],[-0.348244, -0.343459]])
#############################################################################
#### Extended Kalman filter tuning params ###################################
ekf = True # Set True if you want the Kalman filter
nx = x.size1()
ny = y.size1()
nd = d.size1()
Qx_kf = 1.0e-1*DM.eye(nx)
Qd_kf = 1.0*DM.eye(nd)
Q_kf = DM(scla.block_diag(Qx_kf, Qd_kf))
if 'R_wn' in locals():
    R_kf = R_wn
else:
    R_kf = 1.0*DM.eye(ny)
#    Q_kf = 1.0e-16*DM.eye(nx+d.size1()) # (it will be nx+nd when d will be include)
#    R_kf = 1.0e-15*DM.eye(ny)
P0 = Q_kf #1e-2*DM.ones(nx+d.size1(),nx+d.size1())  # (it will be nx+nd,nx+nd when d will be include)



