# -*- coding: utf-8 -*-
"""
Created on Tue Jan 12 12:11:54 2016 

@author: RBdC

Simple linear Process P
Third order

+

Valve Stiction Model:
- He's Model

Not Nominal Case: 
--> Mmpc === P
--> Vmpc =\= V --> MPC NON conosce attrito ma è OFFSET-FREE
"""

from casadi import *
from casadi.tools import *
from matplotlib import pylab as plt
import math
import scipy.linalg as scla
import numpy as np
from Utilities import*
from Target_Calc import *
from Estimator import *
from Control_Calc import *
from Default_Values import *

# NOC discretization parameters

Nsim = 1500     # Simulation length (number of samples)

N = 50          # Horizon

# Time interval value
h = 1.0         # [min] if you are in discrete time just put h = 1.0

########## Symbolic variables #####################

# in the case of He's stiction model
xp = SX.sym("xp", 4)    # process state vector        #
x = SX.sym("x", 3)      # model state vector          #
#
u = SX.sym("u", 1)      # control vector              #
y = SX.sym("y", 1)      # measured output vector      #
k = SX.sym("k", 1)      # discrete time               #
d = SX.sym("d", 1)      # disturbance                 #
###################################################


##### PARAMETERS
## stiction parameters (plant)
fs = 5.0
fd = 2.0
#
alfa = 2.1
Tau = 1e4
#
MPC_type = 'unaware'            # MPC aware/unaware of stiction
WS_type = 'no'                  # WS: NO warm-start
Tuning = 'with_R'               # with_R/with_S
stiction_type = 'if_else'      # if_else/smoothed (type of stiction model)
dyn_of_type = 'QP'              # LP/QP dynamic opt objective function
Dis_mod = 'Output'               # Disturbance Model: Input/Output

# # # # Valve dynamics

#  He's model (casadi if statements)
def Valve_din_He(u,x,fs,fd):
#    fx_p[0] = if_else(casadi.logic_and (u - x[0] >= - fs, u - x[0] <= fs), x[0], \
#        if_else(u - x[0] > fs, u - fd, \
#        u + fd))
    x_1 = if_else(u - x > fs, u - fd, \
        if_else(u - x < -fs, u + fd,\
        x))
    return x_1
    
def Valve_din_He_smoothed(u,x,fs,fd,Tau):
    e = u - x
    # sigmoidal functions
#       eta1 = 1.0/(1.0 + exp(-(e + fs)/tau)) + 1.0/(1.0 + exp((e - fs)/tau)) - 1.0
#       eta2 = 1.0/(1.0 + exp((e + fs)/tau)) + 1.0/(1.0 + exp((e - fs)/tau)) - 1.0
    #
    # hyperbolic functions
    eta1 = 0.5*tanh(Tau*(e + fs)) + 0.5*tanh(Tau*(-e + fs))    
    eta2 = 0.5*tanh(-Tau*(e + fs)) + 0.5*tanh(Tau*(-e + fs))  
    #
    x_1 = eta1*x + (1.0 - eta1)*u + eta2*fd
    return x_1 


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
## PLANT (NON-LINEAR)

# To set if the plant is non-linear
def User_fxp_Dis(x,t,u):
    
    """
    SUMMARY:
    It constructs the function User_fxp_Dis for the non-linear case
    
    SYNTAX:
    assignment = User_fxp_Dis(t)
  
    ARGUMENTS:
    + t             - Variable that indicate the current iteration
    
    OUTPUTS:
    + fx_p          - Non-linear plant function     
    """  
    
          
    # initialize variables
    fx_p = SX(x.size1(),1)
    
    # process parameters
    # Continuous
    # ...
    # Discrete
    Ap = np.array([[2.091447612285384, -0.687441758146000, 0.272531793034013], [2.0, 0.0, 0.0], [0.0, 0.5, 0.0]])   
    Bp = np.array([[0.0625], [0.0], [0.0]])
    
    #### He's model + process (state equation)
#    if (u[0] - x[0] >= - fs) and (u[0] - x[0] <= fs):
#        fx_p[0] = x[0]
#    elif u - x[0] > fs:
#        fx_p[0] = u[0] - fd
#    else:
#        fx_p[0] = u[0]  + fd
    #    
    
    if stiction_type == 'if_else':       
    # He's model (casadi if statements)
        fx_p[0] = Valve_din_He(u,x[0],fs,fd)
    
    else:
    # approximate He's model
        fx_p[0] = Valve_din_He_smoothed(u,x[0],fs,fd,Tau)
    #
                    
    fx_p[1:4] = mtimes(Ap,x[1:4]) + Bp*fx_p[0]    
    
    return fx_p

def User_fyp(x,t,u):
    """
    SUMMARY:
    It constructs the function User_fyp for the non-linear case
    
    SYNTAX:
    assignment = deffyp(t)
  
    ARGUMENTS:
    + t             - Variable that indicate the current iteration
    
    OUTPUTS:
    + fy_p          - Non-linear plant function     
    """
    
    # process parameters
    # Continous
    # ...
    # Discrete
    Cp = np.array([[0.039116001982656, 0.057450029451752, 0.020449714675490]])
    
    # --> PV process (output equation)
    fy_p = mtimes(Cp,x[1:4]) 
    
    return fy_p

############# Additive State Disturbances ###############################
# dkx_p = DM.zeros(x.size1()) # 
# Comment this line if don't want an additive state disturbance.

def defdxp(k):
    """
    SUMMARY:
    It constructs the additive disturbances for the linear case
    
    SYNTAX:
    assignment = defdp(k)
  
    ARGUMENTS:
    + k             - Variable that indicate the current iteration
    
    OUTPUTS:
    + dxp           - State disturbance value      
    """ 
    
#    dxp = np.array([0.1, 0.0, 0.0]) # State disturbance

    if k <= 20:
        dxp = np.array([[0.1, 0.0, 0.0]]) # State disturbance
    else:
        dxp = np.array([[0.0, 0.0, 0.0]]) # State disturbance

    return [dxp]
##########################################################################

############# Additive Output Disturbances ###############################
# dky_p = DM.zeros(y.size1())
# Comment this line if don't want an additive output disturbance.

def defdyp(k):
    """
    SUMMARY:
    It constructs the additive disturbances for the linear case
    
    SYNTAX:
    assignment = defdp(k)
  
    ARGUMENTS:
    + k             - Variable that indicate the current iteration
    
    OUTPUTS:
    + dyp      - Output disturbance value      
    """ 
    
    dyp = np.array([0.1]) # Output disturbance
    
#    if k <= 20:
#        dyp = np.array([[0.5, 350.1, 0.659]]) # State disturbance
#    else:
#        dyp = np.array([[0.4, 355.1, 0.659]]) # State disturbance
    return [dyp]
##########################################################################

## NOISE
#G_wn = 1e-3*np.array([[1.0, 1.0, 1.0], [1.0, 1.0, 1.0], [1.0, 1.0, 1.0]])  # State white noise matrix
#Q_wn = 1e-3*np.array([[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]])  # State white noise covariance matrix
R_wn = 0e0*np.array([[1.0]])                                              # Output white noise covariance matrix
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #



# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
################  MODEL

## Disturbance model
offree = "lin"  # set "lin" or "nl" to have a disturbance model linear or non linear. 
                # "no" means no disturbance model will be implmented
#Bd = DM.eye(d.size1())
if Dis_mod == 'Input':
    B = np.array([[0.0625], [0.0], [0.0]])
    Bd = B
    Cd = 0*DM.eye(d.size1())
else:
    Bd = 0.0*DM([[1.0], [1.0], [1.0]])
    Cd = 1*DM.eye(d.size1())
#Cd = DM.zeros(y.size1(),d.size1())

## PLANT Model (LINEAR) --> MPC NOT aware of stiction
def User_fxm_Dis(x,u,d,k):
    """
    SUMMARY:
    It constructs the function User_fxm_Dis for the non-linear case
    
    SYNTAX:
    assignment = User_fxm_Dis(t)
  
    ARGUMENTS:
    + t                 - Variable that indicate the current iteration
    
    OUTPUTS:
    + fx_model          - Non-linear MODEL plant function     
    """ 
        
    
    # initialize
    fx_model = SX(x.size1(),1)
    
    # process parameters
    # Continous
    # ...
    # Discrete
    A = np.array([[2.091447612285384, -0.687441758146000, 0.272531793034013], [2.0, 0.0, 0.0], [0.0, 0.5, 0.0]])      
    B = np.array([[0.0625], [0.0], [0.0]])
     
     
    #### No stiction   
        
    fx_model[0:3] = mtimes(A,x[0:3]) + B*u[0]
        

    return fx_model
        


# process (output equation)
def User_fym(x,u,d,k):
    
    """
    SUMMARY:
    It constructs the function fy_m
    
    SYNTAX:
    assignment = fy_m(t)
  
    ARGUMENTS:
    + t             - Variable that indicate the current iteration
    
    OUTPUTS:
    + fy_model          - Non-linear MODEL plant function
     
    """ 
        
    # process parameters
    # Continous
    # ...
    # Discrete
    C = np.array([[0.039116001982656, 0.057450029451752, 0.020449714675490]])
 
    fy_model = mtimes(C,x[0:3])
    
    return fy_model


Mx = 10 # Number of elements in each time step 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# Initial conditions
x0_p = 0.0*vertcat(0.0, 1.0, 1.0, 1.0)      # [plant]
x0_m = 0.0*vertcat(1.0, 1.0, 1.0)           # [model]   
u0 = 0.0*vertcat(1.0)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
## BOUNDS
## Input bounds
umin = -50.0*vertcat(1.0)
umax = 50.0*vertcat(1.0)

## Delta Input bounds
#Dumin = -100.0*vertcat(1.0)
#Dumax = 100.0*vertcat(1.0)

## State bounds
xmin = -1000.0*vertcat(1.0, 1.0, 1.0)
xmax = 1000.0*vertcat(1.0, 1.0, 1.0)

## Output bounds
ymin = -50.0*vertcat(1.0)
ymax = 50.0*vertcat(1.0)

## Disturbance bounds
#dmin = None
#dmax = None

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
## SOFT CONSTRAINT ON X AND Y BOUNDS
## Slack variables weights (SS)
w_s = None ## Output slack variable weight
z_s = None ## State slack variable weight

## Slack variables weights (OCP)
W = None ## Output slack variable weight
Z = None ## State slack variable weight

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
## Setpoints

def defSP(t):
    """
    SUMMARY:
    It constructs the setpoints vectors for the steady-state optimisation 
    
    SYNTAX:
    assignment = defSP(t)
  
    ARGUMENTS:
    + t             - Variable that indicates the current time [min]
    
    OUTPUTS:
    + ysp, usp      - Input and output setpoint values      
    """ 
    if t<= 50:
        ysp = np.array([[0.0]])                     # Output setpoint
        usp = np.array([[0.0]])                     # Input setpoints
        xsp = np.array([[0.0, 0.0, 0.0]])           # State setpoints                
    elif t>50 and t<=550:
        ysp = np.array([[7.0]])                     # Output setpoint
        usp = np.array([[0.0]])                     # Control setpoints
        xsp = np.array([[0.0, 0.0, 0.0]])           # State setpoints  
    elif t>550 and t<=1050:
        ysp = np.array([[-7.0]])                    # Output setpoint
        usp = np.array([[0.0]])                     # Control setpoints
        xsp = np.array([[0.0, 0.0, 0.0]])           # State setpoints
    elif t>1050 and t<=1550:
        ysp = np.array([[0.0]])                     # Output setpoint
        usp = np.array([[0.0]])                     # Control setpoints
        xsp = np.array([[0.0, 0.0, 0.0]])           # State setpoints 
    elif t>1550 and t<=2050:
        ysp = np.array([[3.0]])                     # Output setpoint
        usp = np.array([[0.0]])                     # Control setpoints
        xsp = np.array([[0.0, 0.0, 0.0]])           # State setpoints 
    else:
        ysp = np.array([[0.0]])                     # Output setpoint
        usp = np.array([[0.0]])                     # Control setpoints
        xsp = np.array([[0.0, 0.0, 0.0]])           # State setpoints                     
    
    return [ysp, usp, xsp]
    
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
## OBJECTIVE FUNCTION

######## Steady-state optimization 

    # To set if the optimisation problem is QP (Default)                             
Qss = DM([[1.0]])                                                       # Output matrix
Rss = DM([[0.0]])                                                       # Control matrix


    # To set if the optimisation problem is LP
#rss_y = np.array([[0.1, 0.0, 0.0]]) # Output vector
#rss_u = np.array([[0, 0]]) # Control vector


######## Dynamic optimization 

C = DM([[0.039116001982656, 0.057450029451752, 0.020449714675490]])
#
q = 1.0/(np.amax(C)**2)*mtimes(C.T,C) + 1.0*DM.eye(x.size1())
#
if dyn_of_type == 'QP':
# To set if the optimisation problem is QP (Default)
    Q = q                       # State matrix  
    if Tuning == 'with_R': 
        R = 1e1*DM([[1]])                                                   # U matrix
    else:
        S = 1e1*DM([[1]])                                                   # DeltaU matrix
        # note: S and R cannot be set both at the same time
else:
# To set if the optimisation problem is LP
    r_x = 1e8*np.array(C + 0.01)
    #r_x = 1e0*np.array(diag(q).T)
    # r_x = 1e0*np.array([[1.0, 1.0, 1.0, 1.0]])           # State vector
    if Tuning == 'with_R':
        r_u = 1e0*np.array([[1.0]])                     # Control vector
    else:
        r_Du = 1e0*np.array([[1.0]])                    # Control vector for DeltaU

    # Specify the terminal cost if the problem is not QP and 0 value is not wanted
Vn = 1000.0
vfin = mtimes(x.T,mtimes(Vn,x)) 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
## Estimator

#### Kalman filter tuning params  #############################################
kal = False # Set True if you want the Kalman filter

#### Steady-state Kalman filter tuning params #################################
kalss = False # Set True if you want the Steady-state Kalman filter
#nx = x.size1()
#ny = y.size1()
#nd = d.size1()
#Qx_kf = 1.0e-5*DM.eye(nx)
#Qd_kf = 1.0*DM.eye(nd)
#Q_kf = DM(scla.block_diag(Qx_kf, Qd_kf))
#R_kf = 1.0e-5*DM.eye(ny)
#x_ss = 0.0*vertcat([1.0, 2.0, 3.0]) 
#u_ss = 0.0*vertcat([1.0])
#P0 = 1.0e-5*DM.eye(nx+nd) 

#############################################################################
#### Luenberger observer tuning params ######################################
lue = True # Set True if you want the Luemberger observer
nx = x.size1()
ny = y.size1()
nd = d.size1()
Kx = DM.zeros(nx,ny)
Kd = DM.eye(ny)
K = vertcat(Kx,Kd)

#############################################################################
#### Extended Kalman filter tuning params ###################################
ekf = False # Set True if you want the Kalman filter
#nx = x.size1()
#ny = y.size1()
#Q_kf = 1.0e-16*DM.eye(nx+d.size1()) # (it will be nx+nd when d will be include)
#R_kf = 1.0e-15*DM.eye(ny)
#P0 = 1*DM.ones(nx+d.size1(),nx+d.size1())  # (it will be nx+nd,nx+nd when d will be include)
