#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  4 08:26:18 2017

@author: marcovaccari
"""

from casadi import *
from casadi.tools import *
from matplotlib import pylab as plt
import matplotlib 
import math
import scipy.linalg as scla
import numpy as np

tag = 'moving'           # fixed or moving             

import scipy.io as sio #to manage savings of files and loading
JCL_noise_aware = sio.loadmat('JCL_noise_aware_sp_IDM_Rkf_' + tag + '.mat')
JCL_noise_stic_inv = sio.loadmat('JCL_noise_stic_inv_sp_IDM_Rkf_' + tag + '.mat')
JCL_noise_unaware = sio.loadmat('JCL_noise_unaware_sp_IDM_Rkf_' + tag + '.mat')

J_CL_aw = JCL_noise_aware['JCL']
r_wn = JCL_noise_aware['r_wn'].T
J_CL_si = JCL_noise_stic_inv['JCL']
J_CL_un = JCL_noise_unaware['JCL']


pf = './results/mimo/sp/effect_noise/'
if not os.path.exists(pf):
    os.makedirs(pf)
    
# plot figures
plt.figure(figsize=(6,18))
plt.plot(r_wn, J_CL_un,'r*-.')
plt.plot(r_wn, J_CL_si,'b^--') 
plt.plot(r_wn, J_CL_aw,'ko-')
plt.xscale('log'),
yfmt = matplotlib.ticker.ScalarFormatter(useOffset=True, useMathText=True, useLocale=None)
yfmt.set_powerlimits((-3,3))
plt.gca().yaxis.set_major_formatter(yfmt)    
plt.xlabel('$R_{wn}$', fontsize = 14)
plt.ylabel('$J_{CL}$', fontsize = 14)
plt.axis([1e-10, 1, 0.9*np.amin(J_CL_aw), 1.02*np.amax(J_CL_aw)])
plt.legend(('Unaware','St. Inversion','St. Embedding'),loc = 'upper left',fontsize = 10)
plt.grid(True)
#
plt.savefig(pf + 'noise_effect_Rkf_' + tag + '_cmp.png', format = 'png', transparent = True, bbox_inches = 'tight')
plt.show()

