#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  4 08:26:18 2017

@author: marcovaccari & RBdC
"""

from casadi import *
from casadi.tools import *
from matplotlib import pylab as plt
import math
import scipy.linalg as scla
import numpy as np


# comparing LIN cases or unique cases
# to be chosen
case = 'nominal_LIN'                # case: nominal, nominal_LIN

import scipy.io as sio # to manage savings of files and loading
#
if case == 'nominal':
    mimo_compare = sio.loadmat('mimo_compare.mat')
else:
    mimo_compare = sio.loadmat('mimo_compare_lin.mat')

#sio.savemat('mimo_compare_lin.mat', {'Ysp':Y_sp,'Yp':Yp,'Yp2':Yp2,'Yp1':Yp1, 'U':U,'U2':U2,'U1':U1,'X2':X2.full(),'X1':X1.full(),'Xp':Xp,'D2':D_HAT2, 'D1':D_HAT1, 'D': D_HAT })
    
Y_sp = mimo_compare['Ysp']
Yp = mimo_compare['Yp']
Yp1 = mimo_compare['Yp1']
Yp2 = mimo_compare['Yp2']
U = mimo_compare['U']
U1 = mimo_compare['U1']
U2 = mimo_compare['U2']

X1 = mimo_compare['X1']
X2 = mimo_compare['X2']
Xp = mimo_compare['Xp']
D_HAT1 = mimo_compare['D1']
D_HAT2 = mimo_compare['D2']
D_HAT = mimo_compare['D']

if case == 'nominal':
    pf = './results/mimo/sp/nominal_compare/'
else:
    pf = './results/mimo/sp/nominal_compare_LIN/'
if not os.path.exists(pf):
    os.makedirs(pf)

u_in_figure = 'yes'

Nsim = Y_sp.shape[0] 
h = 5
tsim = plt.linspace(0, (Nsim-1)*h, Nsim)

 # plot figures
fig=plt.figure(figsize=(18,18))
#
if u_in_figure == 'yes':
    plt.subplot(3, 2, 1)
else:
    plt.subplot(2, 2, 1)
l1, = plt.plot(tsim, Y_sp[:,0],'g:',linewidth=1.5)
l2, = plt.plot(tsim, Yp2[:,0],'r-',linewidth=1.5)
# l3, = plt.plot(tsim, Yp1[:,0],'b-.',linewidth=1.5)
l4, = plt.plot(tsim, Yp[:,0],'k--',linewidth=1.5)
#plt.xlabel('Time  [s]',fontsize=14)
plt.ylabel('Output 1  [cm]',fontsize=14)
#            plt.axis([0.0, (Nsim-1)*h, 4, 13])
#plt.legend(('Set-Point 1','Unaware','St. Inversion','St. Embedding'),loc = 'upper right',fontsize = 10)
#
if u_in_figure == 'yes':
    plt.subplot(3, 2, 2)
else:
    plt.subplot(2, 2, 2)
plt.plot(tsim, Y_sp[:,1],'g:',linewidth=1.5)
plt.plot(tsim, Yp2[:,1],'r-',linewidth=1.5)
# plt.plot(tsim, Yp1[:,1],'b-.',linewidth=1.5)
plt.plot(tsim, Yp[:,1],'k--',linewidth=1.5)
#plt.xlabel('Time  [s]',fontsize=14)
plt.ylabel('Output 2  [cm]',fontsize=14)
#            plt.axis([0.0, (Nsim-1)*h, 4, 13])
#plt.legend(('Set-Point 2','Unaware','St. Inversion','St. Embedding'),loc = 'upper right',fontsize = 10)
#
if u_in_figure == 'yes':
    plt.subplot(3, 2, 3)
    plt.plot(tsim, U2[:,0],'r-',linewidth=1.5)#,ls = 'steps')
    #plt.plot(tsim, U1[:,0],'b-.',linewidth=1.5)#,ls = 'steps')
    plt.plot(tsim, U[:,0],'k--',linewidth=1.5)#,ls = 'steps')
#    plt.xlabel('Time  [s]',fontsize=14)
    plt.ylabel('Input 1  [%]',fontsize=14)
#                plt.axis([0.0, (Nsim-1)*h, 25, 52])
#    plt.legend(('Unaware','St. Inversion','St. Embedding'),loc = 'upper right',fontsize = 10)
    #
    plt.subplot(3, 2, 4)
    plt.plot(tsim, U2[:,1],'r-',linewidth=1.5)#,ls = 'steps')
    #plt.plot(tsim, U1[:,1],'b-.',linewidth=1.5)#,ls = 'steps')
    plt.plot(tsim, U[:,1],'k--',linewidth=1.5)#,ls = 'steps')
#    plt.xlabel('Time  [s]',fontsize=14)
    plt.ylabel('Input 2  [%]',fontsize=14)
#                plt.axis([0.0, 1000.0, 25, 52])
#    plt.legend(('Unaware','St. Inversion','St. Embedding'),loc = 'upper right',fontsize = 10)
#
if u_in_figure == 'yes':
    plt.subplot(3, 2, 5)
else:
    plt.subplot(2, 2, 3)
plt.plot(tsim, X2[0::6],'r-',linewidth=1.5)
# plt.plot(tsim, X1[0::10],'b-.',linewidth=1.5)
plt.plot(tsim, Xp[:,0],'k--',linewidth=1.5)
#            if load_example == N_MPC-1: 
#                plt.plot(tsim, X1[:,0],'k-.',linewidth=1.5)
#            else:
#                plt.plot(tsim, X1[0::6],'k-.',linewidth=1.5)          
plt.xlabel('Time  [s]',fontsize=14)
plt.ylabel('Valve 1 position [%]',fontsize=14)
#            plt.axis([0.0, (Nsim-1)*h, 0, 100])
#plt.legend(('Unaware','St. Inversion','St. Embedding'),loc = 'lower right',fontsize = 10)

if u_in_figure == 'yes':
    plt.subplot(3, 2, 6)
else:
    plt.subplot(2, 2, 4)
plt.plot(tsim, X2[1::6],'r-',linewidth=1.5)
# plt.plot(tsim, X1[1::10],'b-.',linewidth=1.5)
plt.plot(tsim, Xp[:,1],'k--',linewidth=1.5)   
#            if load_example == N_MPC-1: 
#                plt.plot(tsim, X1[:,1],'k-.',linewidth=1.5)
#            else:
#                plt.plot(tsim, X1[1::6],'k-.',linewidth=1.5)          
plt.xlabel('Time  [s]',fontsize=14)
plt.ylabel('Valve 2 position [%]',fontsize=14)
#            plt.axis([0.0, (Nsim-1)*h, 0, 100])
#plt.legend(('Unaware','St. Inversion','St. Embedding'),loc = 'lower right',fontsize = 10)
art = []
lgd = plt.legend((l1,l2,l4), ('Set-Point','LIN-Unaware','PL-St. Embedding'), loc = 'lower center',  bbox_to_anchor = (0,-0.04,1,1), ncol=4,
            bbox_transform = plt.gcf().transFigure)
art.append(lgd)
plt.tight_layout() # gives spacing between figures
fig.subplots_adjust(wspace=0.12, hspace=0.15)
if case == 'nominal':
    plt.savefig(pf + 'compare.png', format = 'png', transparent = True, additional_artists=art, bbox_inches = 'tight')
else:
    plt.savefig(pf + 'LIN_red_compare.png', format = 'png', transparent = True, additional_artists=art, bbox_inches = 'tight')
#
#plt.legend(('Set-Point 2','Unaware','St. Inversion','St. Embedding'),loc=9, bbox_to_anchor=(0.5, -0.1), ncol=4)

plt.show()

# plot disturbance          
plt.figure(figsize=(18,18))
#
plt.subplot(2, 1, 1)
plt.plot(tsim, D_HAT2[:,0],'r-',linewidth=1.5)
plt.plot(tsim, D_HAT1[:,0],'b-.',linewidth=1.5)
plt.plot(tsim, D_HAT[:,0],'k--',linewidth=1.5)
plt.xlabel('Time  [s]',fontsize=14)
plt.ylabel('Disturbance 1  [cm]',fontsize=14)
plt.legend(('LIN-Unaware','PL-St. Embedding'),loc = 'upper right',fontsize = 10)
# plt.legend((LMPC-0','LMPC-1','NMPC'),loc = 'upper right',fontsize = 10)
#
plt.subplot(2, 1, 2)
plt.plot(tsim, D_HAT2[:,1],'r-',linewidth=1.5)
plt.plot(tsim, D_HAT1[:,1],'b-.',linewidth=1.5)
plt.plot(tsim, D_HAT[:,1],'k--',linewidth=1.5)
plt.xlabel('Time  [s]',fontsize=14)
plt.ylabel('Disturbance 2  [cm]',fontsize=14)
plt.legend(('PL-Unaware','PL-St. Embedding'),loc = 'upper right',fontsize = 10)
# plt.legend(('Unaware','St. Inversion','St. Embedding'),loc = 'upper right',fontsize = 10)

plt.tight_layout() # gives spacing between figures
#plt.savefig(pf + 'nominal_compare_LIN_dis.png', format = 'png', transparent = True, bbox_inches = 'tight')
#
plt.show()


