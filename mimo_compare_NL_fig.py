#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  4 08:26:18 2017

@author: marcovaccari
"""

from casadi import *
from casadi.tools import *
from matplotlib import pylab as plt
import math
import scipy.linalg as scla
import numpy as np


# comparing NL cases or STIC INV test
# to be chosen
case = 'SI'                # case: NL, stic_inv
 
import scipy.io as sio #to manage savings of files and loading
if case == 'NL':
    mimo_nom_compare_NL = sio.loadmat('mimo_nom_compare_NL.mat')
else:
    mimo_nom_compare_NL = sio.loadmat('mimo_SI_compare.mat')
#sio.savemat('mimo_nom_compare.mat', {'Ysp':Y_sp,'Yp1':Yp1,'Yp':Yp, 'U1':U1,'U':U, 'X1':X1.full(),'X':Xp,'D1':D_HAT1, 'D': D_HAT })
        
Y_sp = mimo_nom_compare_NL['Ysp']
Yp = mimo_nom_compare_NL['Yp']
Yp1 = mimo_nom_compare_NL['Yp1']
U = mimo_nom_compare_NL['U']
U1 = mimo_nom_compare_NL['U1']
X1 = mimo_nom_compare_NL['X1']
Xp = mimo_nom_compare_NL['X']
D_HAT1 = mimo_nom_compare_NL['D1']
D_HAT = mimo_nom_compare_NL['D']

if case == 'NL':
    pf = './results/mimo/sp/nominal_compare_NL/'
else:
    pf = './results/mimo/sp/nominal_compare_STIC_INV/'
if not os.path.exists(pf):
    os.makedirs(pf)

u_in_figure = 'yes'

Nsim = Y_sp.shape[0] 
h = 5
tsim = plt.linspace(0, (Nsim-1)*h, Nsim)

 # plot figures
fig = plt.figure(figsize=(18,18))

#
if u_in_figure == 'yes':
    plt.subplot(3, 2, 1)
else:
    plt.subplot(2, 2, 1)
l1, = plt.plot(tsim, Y_sp[:,0],'g:',linewidth=1.5)
l2, = plt.plot(tsim, Yp1[:,0],'r-',linewidth=1.5)
l3, = plt.plot(tsim, Yp[:,0],'k-.',linewidth=1.5)
plt.ylabel('Output 1  [cm]',fontsize=14)
plt.axis([0.0, (Nsim-1)*h, 5, 14])
#plt.legend(('Set-Point 1','Unaware','Stiction Embedding'),loc = 'upper right',fontsize = 10)
#
if u_in_figure == 'yes':
    plt.subplot(3, 2, 2)
else:
    plt.subplot(2, 2, 2)
plt.plot(tsim, Y_sp[:,1],'g:',linewidth=1.5)
plt.plot(tsim, Yp1[:,1],'r-',linewidth=1.5)
plt.plot(tsim, Yp[:,1],'k-.',linewidth=1.5)
plt.ylabel('Output 2  [cm]',fontsize=14)
plt.axis([0.0, (Nsim-1)*h, 5, 14])
#plt.legend(('Set-Point 2','Unaware','St. Embedding'),loc = 'upper right',fontsize = 10)
#
if u_in_figure == 'yes':
    plt.subplot(3, 2, 3)
    plt.plot(tsim, U1[:,0],'r-',linewidth=1.5) # ls = 'steps')
    plt.plot(tsim, U[:,0],'k-.',linewidth=1.5) #ls = 'steps')
    plt.ylabel('Input 1  [%]',fontsize=14)
    plt.axis([0.0, (Nsim-1)*h, 10, 60])
#    plt.legend(('Unaware','St. Embedding'),loc = 'upper right',fontsize = 10)
    #        
    plt.subplot(3, 2, 4)
    plt.plot(tsim, U1[:,1],'r-',linewidth=1.5) # ls = 'steps')
    plt.plot(tsim, U[:,1],'k-.',linewidth=1.5) # ls = 'steps')
    plt.ylabel('Input 2  [%]',fontsize=14)
    plt.axis([0.0, (Nsim-1)*h, 10, 60])
#    plt.legend(('Unaware','St. Embedding'),loc = 'upper right',fontsize = 10)
    #
if u_in_figure == 'yes':
    plt.subplot(3, 2, 5)
else:
    plt.subplot(2, 2, 3)
if case == 'NL':
    plt.plot(tsim, X1[0::6],'r-',linewidth=1.5)
    plt.plot(tsim, Xp[:,0],'k-.',linewidth=1.5)
else:
    plt.plot(tsim, X1[4::10],'r-',linewidth=1.5)
    plt.plot(tsim, Xp[:,4],'k-.',linewidth=1.5)    
plt.xlabel('Time  [min]',fontsize=14)
plt.ylabel('Valve 1 position [%]',fontsize=14)
#plt.axis([0.0, (Nsim-1)*h, 0, 75])
#plt.legend(('Unaware','St. Embedding'),loc = 'lower right',fontsize = 10)
#

if u_in_figure == 'yes':
    plt.subplot(3, 2, 6)
else:
    plt.subplot(2, 2, 4)
if case == 'NL':
    plt.plot(tsim, X1[1::6],'r-',linewidth=1.5)
    plt.plot(tsim, Xp[:,1],'k-.',linewidth=1.5)
else:
    plt.plot(tsim, X1[5::10],'r-',linewidth=1.5)
    plt.plot(tsim, Xp[:,5],'k-.',linewidth=1.5)             
plt.xlabel('Time  [min]',fontsize=14)
plt.ylabel('Valve 2 position [%]',fontsize=14)
#plt.axis([0.0, (Nsim-1)*h, 0, 75])


art = []
if case == 'NL':
    lgd = plt.legend((l1,l2,l3), ('Set-Point','Unaware','St. Embedding'), loc = 'lower center',  bbox_to_anchor = (0,-0.04,1,1), ncol=3, 
    bbox_transform = plt.gcf().transFigure)
else:
    lgd = plt.legend((l1,l2,l3), ('Set-Point','SI-LinJ','SI-TanhJ'), loc = 'lower center',  bbox_to_anchor = (0,-0.04,1,1), ncol=3, 
    bbox_transform = plt.gcf().transFigure)            
art.append(lgd)
plt.tight_layout() # gives spacing between figures
fig.subplots_adjust(wspace=0.12, hspace=0.15)
if case == 'NL':
    plt.savefig(pf + 'NL_compare.png', format = 'png', transparent = True, additional_artists=art, bbox_inches = 'tight')
else:
    plt.savefig(pf + 'STICINV_compare.png', format = 'png', transparent = True, additional_artists=art, bbox_inches = 'tight')


# plot disturbance
# plot figures
plt.figure(figsize=(18,18))
#
plt.subplot(2, 1, 1)
plt.plot(tsim, D_HAT1[:,0],'r-',linewidth=1.5)
plt.plot(tsim, D_HAT[:,0],'k-.',linewidth=1.5)
plt.xlabel('Time  [min]',fontsize=14)
plt.ylabel('Disturbance 1  [cm]',fontsize=14)
plt.legend(('Unaware','St. Embedding'),loc = 'upper right',fontsize = 10)
#
plt.subplot(2, 1, 2)
plt.plot(tsim, D_HAT1[:,1],'r-',linewidth=1.5)
plt.plot(tsim, D_HAT[:,1],'k-.',linewidth=1.5)
plt.xlabel('Time  [min]',fontsize=14)
plt.ylabel('Disturbance 2  [cm]',fontsize=14)
   
 #
#plt.legend(('Set-Point 2','Unaware','St. Inversion','St. Embedding'),loc=9, bbox_to_anchor=(0.5, -0.1), ncol=4)

plt.show()




