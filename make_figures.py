# -*- coding: utf-8 -*-
"""
Created on 2016/2017

@author: Riccardo Bacci di Capaci

file to re-plot figures
"""
from matplotlib import pylab as plt
import matplotlib
import numpy as np
import os

type_figure = 'noise_levels'            # noise_levels, stic_param_fs, stic_param_fd

pf = './make_figure'
if not os.path.exists(pf):
    os.makedirs(pf)

######## noise effect 
#r_wn = np.array([  0.00000000e+00,   1.00000000e-10,   1.00000000e-07,
#         1.00000000e-06,   1.00000000e-05,   1.00000000e-04,
#         1.00000000e-03,   1.00000000e-02,   1.00000000e-01,
#         1.00000000e+00])
         
r_wn = np.array([1.00000000e-06,   1.00000000e-05,   1.00000000e-04,
         1.00000000e-03,   1.00000000e-02,   1.00000000e-01,
         1.00000000e+00])

#J_CL_sinv = np.array([[ 1552073.44697352],
#       [ 1631089.47717613],
#       [ 1545762.34035807],
#       [ 1484621.12220284],
#       [ 1493468.72241607],
#       [ 1579627.65719948],
#       [ 1583705.46737418],
#       [ 1418464.50799838],
#       [ 1635339.16806285],
#       [ 2900546.13446852]])
#
#J_CL_su = np.array([[ 1708735.97007045],
#       [ 1620126.22612808],
#       [ 1542122.87686945],
#       [ 1646104.9145897 ],
#       [ 1619572.01074017],
#       [ 1606004.28308551],
#       [ 1663749.23398488],
#       [ 1530603.28201079],
#       [ 1689524.06569851],
#       [ 3267087.63574367]])
#
#J_CL_sa = np.array([[ 1421291.42265323],
#       [ 1418664.90917272],
#       [ 1414563.08270738],
#       [ 1413067.88379996],
#       [ 1413050.67367154],
#       [ 1411822.80829358],
#       [ 1413593.07403749],
#       [ 1419385.52303898],
#       [ 1630086.92183008],
#       [ 2623485.38272896]])

J_CL_sinv = 1e3*np.array([
        [ 12.2],
       [ 41.6],
       [ 64.6],
       [ 59.2],
       [ 44.1],
       [ 19.6],
       [ 140.3]])
         

J_CL_su = 1e3*np.array([
        [67.6],
       [ 69.9],
       [ 71.1],
       [ 70.6 ],
       [ 48.8],
       [ 41.8],
       [ 154.6]])
             
       
J_CL_sa = 1e3*np.array([
        [ 0.063],
       [ 0.041],
       [ 0.034],
       [ 0.418],
       [ 2.20],
       [ 16.8],
       [ 143.0]])
       
              

############ errors on fs
F_s = np.array([2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0])
fs_p = 6.0
eF_s = fs_p - F_s

fs_J_CL_sa = np.array([[  6.32282357e+04],
       [  1.08526315e+05],
       [  7.61907260e+01],
       #[  2.42858123e+01],
       [  6.42858123e+01],      # corretto
       [  5.43041252e+01],
       [  6.65855499e+01],
       [  4.56360783e+03],
       [  4.04507195e+03],
       [  8.35931885e+03]])
       
fs_J_CL_sinv = np.array([[ 29863.72317367],
       [ 63457.1276312 ],
       [ 74379.02266876],
       [ 90723.68439997],
       [ 90781.7566311 ],
       [ 93500.11912951],
       [ 91757.47207909],
       [ 94392.00190682],
       [ 92924.2325825 ]])     

############ errors on fd
F_d = np.array([0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0])
fd_p = 4.0
eF_d = fd_p - F_d

fd_J_CL_sa = np.array([[  2.50348395e+05],
       [  2.34107340e+06],
       [  5.24199014e+04],
       [  1.15769016e+05],
       [  3.75101123e-01],
       [  8.71588043e+04],
       [  6.67614084e+04],
       [  8.36200110e+04],
       [  1.73188969e+04]])

fd_J_CL_sinv = np.array([[  1.66871100e+05],
       [  1.47903127e+05],
       [  1.68597651e+05],
       [  1.20962418e+05],
       [  1.50875313e+05],
       [  1.02514762e+05],
       [  1.24179869e+02],
       [  5.79272298e+04],
       [  2.04924878e+02]])



#################### 

if type_figure == 'noise_levels':           # noise levels      
    # plot figures
    plt.figure(figsize=(6,18))
    plt.plot(r_wn, J_CL_su,'r*-.')
    plt.plot(r_wn, J_CL_sinv,'b^--') 
    plt.plot(r_wn, J_CL_sa,'ko-')
    plt.xscale('log'),
    yfmt = matplotlib.ticker.ScalarFormatter(useOffset=True, useMathText=True, useLocale=None)
    yfmt.set_powerlimits((-3,3))
    plt.gca().yaxis.set_major_formatter(yfmt)    
    plt.xlabel('$R_{wn}$', fontsize = 14)
    plt.ylabel('$J_{CL}$', fontsize = 14)
    plt.axis([1.0e-6, 2*np.amax(r_wn), 0.0001*np.amin(J_CL_sa), 1.02*np.amax(J_CL_su)])
    plt.legend(('LMPC-0','LMPC-1','NMPC'),loc = 'upper left',fontsize = 10)
    plt.grid(True)
    #
    plt.savefig('./make_figure/' + 'noise_effect_cmp.png', format = 'png', transparent = True, bbox_inches = 'tight')
    plt.show()

if type_figure == 'stic_param_fs':             # stiction parameters
    # plot figures
    plt.figure(figsize=(6,18))
    #
    # plt.subplot(2, 1, 1)
    # plt.plot(eF_s, fs_J_CL_su,'b-')
    plt.plot(eF_s, fs_J_CL_sinv,'k^--') 
    plt.plot(eF_s, fs_J_CL_sa,'ro-')
    yfmt = matplotlib.ticker.ScalarFormatter(useOffset=True, useMathText=True, useLocale=None)
    #yfmt.set_powerlimits((-3,3))
    #plt.gca().yaxis.set_major_formatter(yfmt)
    plt.yscale('log')
    plt.xlabel('$e_{S}$', fontsize = 14)
    plt.ylabel('$J_{CL}$', fontsize = 14)
    plt.axis([np.amin(eF_s)-0.2, np.amax(eF_s)+0.2, 0.8*np.amin(fs_J_CL_sa), 1.2*np.amax(fs_J_CL_sa)])
    plt.grid(True)
    plt.legend(('LMPC-1','NMPC'),loc = 'best',fontsize = 10)
    plt.savefig('./make_figure/' + 'stic_par_fs_cmp.pdf', format = 'pdf', transparent = True, bbox_inches = 'tight')
    plt.savefig('./make_figure/' + 'stic_par_fs_cmp.png', format = 'png', transparent = True, bbox_inches = 'tight')
    plt.show()
    #

if type_figure == 'stic_param_fd':             # stiction parameters
    plt.figure(figsize=(6,18))    
    # plt.subplot(2, 1, 2)
    # plt.plot(eF_s, fd_J_CL_su,'b-')
    plt.plot(eF_s, fd_J_CL_sinv,'k^--') 
    plt.plot(eF_s, fd_J_CL_sa,'ro-')
    yfmt = matplotlib.ticker.ScalarFormatter(useOffset=True, useMathText=True, useLocale=None)
    #yfmt.set_powerlimits((-3,3))
    #plt.gca().yaxis.set_major_formatter(yfmt)
    plt.yscale('log')
    plt.xlabel('$e_{D}$', fontsize = 14)
    plt.ylabel('$J_{CL}$', fontsize = 14)
    plt.axis([np.amin(eF_d)-0.2, np.amax(eF_d)+0.2, 0.8*np.amin(fd_J_CL_sa), 1.2*np.amax(fd_J_CL_sa)])
    plt.grid(True)
    plt.legend(('LMPC-1','NMPC'),loc = 'best',fontsize = 10)
    plt.savefig('./make_figure/' + 'stic_par_fd_cmp.pdf', format = 'pdf', transparent = True, bbox_inches = 'tight')
    plt.savefig('./make_figure/' + 'stic_par_fd_cmp.png', format = 'png', transparent = True, bbox_inches = 'tight')
    plt.show()